/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *   Mupen64plus-ui-console - main.c                                       *
 *   Mupen64Plus homepage: http://code.google.com/p/mupen64plus/           *
 *   Copyright (C) 2007-2010 Richard42                                     *
 *   Copyright (C) 2008 Ebenblues Nmn Okaygo Tillin9                       *
 *   Copyright (C) 2002 Hacktarux                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.          *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/* This is the main application entry point for the console-only front-end
 * for Mupen64Plus v2.0. 
 */
 
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <vector>
#include <thread>
#include <condition_variable>
#include <mutex>
#include <iostream>
#include <iomanip>
#include <unordered_set>
#include <fstream>
#include <array>
#include <sstream>
#include <set>
#include <assert.h>
#include <atomic>

#include "SDL_main.h"
#include "cheat.h"
#include "compare_core.h"
#include "core_interface.h"
#include "m64p_types.h"
#include "main.h"
#include "osal_preproc.h"
#include "plugin.h"
#include "version.h"

#include <commctrl.h>

/* Version number for UI-Console config section parameters */
#define CONFIG_PARAM_VERSION     1.00

/** global variables **/
int    g_Verbose = 0;

/** static (local) variables **/
static m64p_handle l_ConfigCore = NULL;
static m64p_handle l_ConfigVideo = NULL;
static m64p_handle l_ConfigUI = NULL;

static const char *l_CoreLibPath = NULL;
static const char *l_ConfigDirPath = NULL;
static const char *l_ROMFilepath = NULL;       // filepath of ROM to load & run at startup
static const char *l_SaveStatePath = NULL;     // save state to load at startup

#if defined(SHAREDIR)
  static const char *l_DataDirPath = SHAREDIR;
#else
  static const char *l_DataDirPath = NULL;
#endif

static int  *l_TestShotList = NULL;      // list of screenshots to take for regression test support
static int   l_TestShotIdx = 0;          // index of next screenshot frame in list
static int   l_SaveOptions = 1;          // save command-line options in configuration file (enabled by default)
static int   l_CoreCompareMode = 0;      // 0 = disable, 1 = send, 2 = receive

static eCheatMode l_CheatMode = CHEAT_DISABLE;
static char      *l_CheatNumList = NULL;

enum Tabs
{
	TRACED = 0,
	LOGGED = 1
};

enum class AnalyzerState
{
	IDLE,
	STARTED,
	WAITING_FOR_VI_CALLBACK,
	STEPPING_AND_LOGGING,
	ANALYZING,
	DEBUGGING,
	SEARCHING,
	CANCELLED,
	TERMINATED
};

enum class AnalyzerResult
{
	NONE,
	USER_CANCELLED,
	ADDRESS_WAS_NEVER_WRITTEN,
	SUCCESS
};

enum class SearchOperator
{
	EQUAL_TO,
	NOT_EQUAL_TO,
	LESS_THAN,
	GREATER_THAN,
	LESS_THAN_OR_EQUAL_TO,
	GREATER_THAN_OR_EQUAL_TO,
	IN_RANGE
};

enum class SearchComparator
{
	PREVIOUS_VALUE,
	SPECIFIC_VALUE
};

struct Operation
{
	Operation() : instr(0), pc(0), address(0) {}
	Operation(uint32_t instr, uint32_t pc, uint32_t address = 0) : instr(instr), pc(pc), address(address) {}

	uint32_t instr;
	uint32_t pc;
	uint32_t address;
};

struct ListItem
{
	ListItem() {}
	ListItem(const std::string& pc, const std::string func, const std::string decoded) : pc(pc), func(func), decoded(decoded) {}

	std::string pc;
	std::string func;
	std::string decoded;
};

struct Watch
{
	Watch() : address(0), size(0), value(0) {}
	Watch(uint32_t address, uint32_t size) : address(address), size(size), value(0) {}

	bool operator<(const Watch& rhs) const { return address < rhs.address; };

	uint32_t address;
	uint32_t size;
	mutable uint64_t value;
};

struct Result
{
	Result() : value(0), size(0) {}

	uint64_t value;
	uint32_t size;
};

struct SearchResult
{
	SearchResult() : address(0), value(0) {}
	SearchResult(uint32_t address, uint32_t value) : address(address), value(value) {}

	uint32_t address;
	uint32_t value;
};

struct IgnoreItem
{
	IgnoreItem() : address(0), size(0) {}
	IgnoreItem(uint32_t address, uint32_t size, const std::string& description) : address(address), size(size), description(description) {}

	bool operator==(const IgnoreItem& rhs) const { return address == rhs.address; }

	uint32_t address;
	uint32_t size;
	std::string description;
};

using Operations = std::vector<Operation>;
static Operations l_operations;
static Operations l_tempOperations;
static Operations l_foundOperations;
static Operations l_tempFoundOperations;

static bool l_lastStep = false;

using Indices = std::set<uint32_t>;
static Indices l_foundIndices;
static Indices l_tempIndices;

using ListItems = std::vector<ListItem>;
static ListItems l_listItems;

using RegisterSet = std::unordered_set<uint32_t>;
using AddressSet = std::unordered_set<uint32_t>;

static RegisterSet l_registers;
static AddressSet l_addresses;

using WatchList = std::set<Watch>;

static WatchList l_watchList;
static std::mutex l_watchListMutex;

static std::atomic_uint l_format;

static AnalyzerState l_analyserState = AnalyzerState::IDLE;
static AnalyzerResult l_analyzerResult = AnalyzerResult::NONE;
static std::mutex l_analyzerStateMutex;
static std::mutex l_analyzerResultMutex;

static std::mutex l_cancelMutex;
static bool l_cancelClicked = false;

static uint32_t l_prevTime = 0;

static AddressSet l_breakpoints;

static int l_currentIndex = 0;
static unsigned int l_expected_pc = 0;
static bool l_isStepping = false;
static bool l_canToggle = true;

using Results = std::vector<Result>;
static Results l_results;
static Results l_tempResults;

using SearchResults = std::vector<SearchResult>;

using IgnoreList = std::vector<IgnoreItem>;

static uint32_t l_searchSize = 4;
static uint32_t l_searchFormat = 0;
static uint32_t l_searchSpecificValue = 0;
static uint32_t l_searchRangeStart = 0;
static uint32_t l_searchRangeEnd = 0;
static SearchOperator l_searchOperator = SearchOperator::EQUAL_TO;
static SearchComparator l_searchComparator = SearchComparator::PREVIOUS_VALUE;
static SearchResults l_searchResults;
static SearchResults l_tempSearchResults;

static IgnoreList l_ignoreList;

#ifdef CREATE_RESULT_FILE
static std::ofstream l_resultFile;
#endif

#ifdef CREATE_VERIFICATION_FILE
static std::ofstream l_verificationFile;
#endif

static uint32_t l_address = 0;
static uint32_t l_size = 0;

#define WM_FINISHED_ANALYSIS WM_USER + 1
#define WM_ABORT WM_USER + 2
#define WM_UPDATE_REALTIME WM_USER + 3
#define WM_BREAKPOINT_HIT WM_USER + 4
#define WM_SEARCH_COMPLETED WM_USER + 5

static HWND l_window = NULL;
static HWND l_edit = NULL;
static HWND l_analyzeButton = NULL;
static HWND l_refreshButton = NULL;
static HWND l_toggleButton = NULL;
static HWND l_stepButton = NULL;
static HWND l_continueButton = NULL;
static HWND l_tabControl = NULL;
static HWND l_tracedListView = NULL;
static HWND l_loggedListView = NULL;
static HWND l_progressWnd = NULL;
static HWND l_progressBar = NULL;
static HWND l_statusMsg = NULL;
static HWND l_cancelBtn = NULL;
static HWND l_watchListView = NULL;
static HWND l_formatCombo = NULL;
static HWND l_sizeCombo = NULL;
static HWND l_statusText = NULL;
static HWND l_searchWnd = NULL;
static HWND l_searchWndButton = NULL;
static HWND l_searchListView = NULL;
static HWND l_searchSizeCombo = NULL;
static HWND l_searchFormatCombo = NULL;
static HWND l_searchEqualToRadio = NULL;
static HWND l_searchNotEqualToRadio = NULL;
static HWND l_searchLessThanRadio = NULL;
static HWND l_searchGreaterThanRadio = NULL;
static HWND l_searchLessThanOrEqualToRadio = NULL;
static HWND l_searchGreaterThanOrEqualToRadio = NULL;
static HWND l_searchRangeRadio = NULL;
static HWND l_searchRangeStartEdit = NULL;
static HWND l_searchRangeEndEdit = NULL;
static HWND l_searchPreviousValueRadio = NULL;
static HWND l_searchSpecificValueRadio = NULL;
static HWND l_searchSpecificValueEdit = NULL;
static HWND l_searchSearchButton = NULL;
static HWND l_searchResetButton = NULL;
static HWND l_searchStatusText = NULL;
static HWND l_ignoreWnd = NULL;
static HWND l_ignoreWndButton = NULL;
static HWND l_ignoreListView = NULL;
static HWND l_ignoreAddressEdit = NULL;
static HWND l_ignoreSizeCombo = NULL;
static HWND l_ignoreAddButton = NULL;

static const std::array<std::string, 32> register_mnemonic = 
{
	"0", "$at", "$v0", "$v1", "$a0", "$a1", "$a2", "$a3",
	"$t0", "$t1", "$t2", "$t3", "$t4", "$t5", "$t6", "$t7",	
	"$s0", "$s1", "$s2", "$s3", "$s4", "$s5", "$s6", "$s7", 
	"$t8", "$t9", "$k0", "$k1", "$gp", "$sp", "$s8", "$ra"	
};

static const std::array<std::string, 32> cop1_mnemonic = 
{
	"$f0", "$f1", "$f2", "$f3", "$f4", "$f5", "$f6", "$f7",
	"$f8", "$f9", "$f10", "$f11", "$f12", "$f13", "$f14", "$f15",
	"$f16", "$f17", "$f18", "$f19", "$f20", "$f21", "$f22", "$f23", 
	"$f24", "$f25", "$f26", "$f27", "$f28", "$f29", "$f30", "$f31"
};

enum RegisterFlags
{
	REG_LO = (1 << 6),
	REG_HI = (1 << 7),
	REG_CC = (1 << 8),
	REG_COP1 = (1 << 9)
};

enum Opcodes
{
	// Prefixes
	OP_SPECIAL = 0x00,
	OP_REGIMM = 0x01,
	OP_COP0 = 0x10,
	OP_COP1 = 0x11,
	OP_BC1 = 0x08,
	OP_COP1_S = 0x10,
	OP_COP1_D = 0x11,
	OP_COP1_W = 0x14,
	OP_COP1_L = 0x15,

	// Special
	OP_SLL = 0x00,
	OP_SRL = 0x02,
	OP_SRA = 0x03,
	OP_SLLV = 0x04,
	OP_SRLV = 0x06,
	OP_SRAV = 0x07,
	OP_JR = 0x08,
	OP_JALR = 0x09,	
	//OP_SYSCALL = 0x0C,
	//OP_BREAK = 0x0D, //Not implemented
	OP_SYNC = 0x0F,
	OP_MFHI = 0x10,
	OP_MTHI = 0x11,
	OP_MFLO = 0x12,
	OP_MTLO = 0x13,
	OP_DSLLV = 0x14,
	OP_DSRLV = 0x16,
	OP_DSRAV = 0x17,
	OP_MULT = 0x18,
	OP_MULTU = 0x19,
	OP_DIV = 0x1A,
	OP_DIVU = 0x1B,
	OP_DMULT = 0x1C,
	OP_DMULTU = 0x1D,
	OP_DDIV = 0x1E,
	OP_DDIVU = 0x1F,
	OP_ADD = 0x20,
	OP_ADDU = 0x21,
	OP_SUB = 0x22,
	OP_SUBU = 0x23,
	OP_AND = 0x24,
	OP_OR = 0x25,
	OP_XOR = 0x26,
	OP_NOR = 0x27,
	OP_SLT = 0x2A,
	OP_SLTU = 0x2B,
	OP_DADD = 0x2C,
	OP_DADDU = 0x2D,
	OP_DSUB = 0x2E,
	OP_DSUBU = 0x2F,	
	//OP_TGE = 0x30,	// Not implemented
	//OP_TGEU = 0x31,	// Not implemented
	//OP_TLT = 0x32,	// Not implemented
	//OP_TLTU = 0x33,	// Not implemented
	OP_TEQ = 0x34,
	//OP_TNE = 0x36,	// Not implemented	
	OP_DSLL = 0x38,
	OP_DSRL = 0x3A,
	OP_DSRA = 0x3B,
	OP_DSLL32 = 0x3C,
	OP_DSRL32 = 0x3E,	
	OP_DSRA32 = 0x3F,

	// Regimm
	OP_BLTZ = 0x00,
	OP_BGEZ = 0x01,
	OP_BLTZL = 0x02,
	OP_BGEZL = 0x03,
	//OP_TGEI = 0x08, //Not implemented
	//OP_TGEIU = 0x09, //Not implemented
	//OP_TLTI = 0x0A, //Not implemented
	//OP_TLTIU = 0x0B, //Not implemented
	//OP_TEQI = 0x0C, //Not implemented
	//OP_TNEI = 0x0E, //Not implemented
	OP_BLTZAL = 0x10,
	OP_BGEZAL = 0x11,
	OP_BLTZALL = 0x12,
	OP_BGEZALL = 0x13,

	// Coprocessor 1
	OP_MFC1 = 0x00,
	OP_DMFC1 = 0x01,
	//OP_CFC1 = 0x02
	OP_MTC1 = 0x04,
	OP_DMTC1 = 0x05,
	//OP_CTC1 = 0x06
		
	// Coprocessor 1 - Branch on C1 condition
	OP_BC1F = 0x00,
	OP_BC1T = 0x01,
	OP_BC1FL = 0x02,
	OP_BC1TL = 0x03,

	//Coprocessor 1 - S-format
	OP_ADD_S = 0x00,
	OP_SUB_S = 0x01,
	OP_MUL_S = 0x02,
	OP_DIV_S = 0x03,
	OP_SQRT_S = 0x04,
	OP_ABS_S = 0x05,
	OP_MOV_S = 0x06,
	OP_NEG_S = 0x07,
	OP_ROUND_L_S = 0x08,
	OP_TRUNC_L_S = 0x09,
	OP_CEIL_L_S = 0x0A,
	OP_FLOOR_L_S = 0x0B,
	OP_ROUND_W_S = 0x0C,
	OP_TRUNC_W_S = 0x0D,
	OP_CEIL_W_S = 0x0E,
	OP_FLOOR_W_S = 0x0F,
	OP_CVT_D_S = 0x21,
	OP_CVT_W_S = 0x24,
	OP_CVT_L_S = 0x25,
	OP_C_F_S = 0x30,
	OP_C_UN_S = 0x31,
	OP_C_EQ_S = 0x32,
	OP_C_UEQ_S = 0x33,
	OP_C_OLT_S = 0x34,
	OP_C_ULT_S = 0x35,
	OP_C_OLE_S = 0x36,
	OP_C_ULE_S = 0x37,
	OP_C_SF_S = 0x38,
	OP_C_NGLE_S = 0x39,
	OP_C_SEQ_S = 0x3A,
	OP_C_NGL_S = 0x3B,
	OP_C_LT_S = 0x3C,
	OP_C_NGE_S = 0x3D,
	OP_C_LE_S = 0x3E,
	OP_C_NGT_S = 0x3F,

	// Coprocessor 1 - D-format
	OP_ADD_D = 0x00,
	OP_SUB_D = 0x01,
	OP_MUL_D = 0x02,
	OP_DIV_D = 0x03,
	OP_SQRT_D = 0x04,
	OP_ABS_D = 0x05,
	OP_MOV_D = 0x06,
	OP_NEG_D = 0x07,
	OP_ROUND_L_D = 0x08,
	OP_TRUNC_L_D = 0x09,
	OP_CEIL_L_D = 0x0A,
	OP_FLOOR_L_D = 0x0B,
	OP_ROUND_W_D = 0x0C,
	OP_TRUNC_W_D = 0x0D,
	OP_CEIL_W_D = 0x0E,
	OP_FLOOR_W_D = 0x0F,
	OP_CVT_S_D = 0x21,
	OP_CVT_W_D = 0x24,
	OP_CVT_L_D = 0x25,
	OP_C_F_D = 0x30,
	OP_C_UN_D = 0x31,
	OP_C_EQ_D = 0x32,
	OP_C_UEQ_D = 0x33,
	OP_C_OLT_D = 0x34,
	OP_C_ULT_D = 0x35,
	OP_C_OLE_D = 0x36,
	OP_C_ULE_D = 0x37,
	OP_C_SF_D = 0x38,
	OP_C_NGLE_D = 0x39,
	OP_C_SEQ_D = 0x3A,
	OP_C_NGL_D = 0x3B,
	OP_C_LT_D = 0x3C,
	OP_C_NGE_D = 0x3D,
	OP_C_LE_D = 0x3E,
	OP_C_NGT_D = 0x3F,

	// Coprocessor 1 - W-format
	OP_CVT_S_W = 0x20,
	OP_CVT_D_W = 0x21,	

	// Coprocessor 1 - L-format
	OP_CVT_S_L = 0x20,
	OP_CVT_D_L = 0x21,

	// Major
	OP_J = 0x02,
	OP_JAL = 0x03,
	OP_BEQ = 0x04,
	OP_BNE = 0x05,	
	OP_BLEZ = 0x06,
	OP_BGTZ = 0x07,
	OP_ADDI = 0x08,
	OP_ADDIU = 0x09,
	OP_SLTI = 0x0A,
	OP_SLTIU = 0x0B,
	OP_ANDI = 0x0C,
	OP_ORI = 0x0D,
	OP_XORI = 0x0E,
	OP_LUI = 0x0F,
	OP_BEQL = 0x14,
	OP_BNEL = 0x15,
	OP_BLEZL = 0x16,
	OP_BGTZL = 0x17,
	OP_DADDI = 0x18,
	OP_DADDIU = 0x19,
	OP_LDL = 0x1A,
	OP_LDR = 0x1B,
	OP_LB = 0x20,
	OP_LH = 0x21,
	OP_LWL = 0x22,
	OP_LW = 0x23,
	OP_LBU = 0x24,
	OP_LHU = 0x25,
	OP_LWR = 0x26,
	OP_LWU = 0x27,
	OP_SB = 0x28,	
	OP_SH = 0x29,
	OP_SWL = 0x2A,
	OP_SW = 0x2B,
	OP_SDL = 0x2C,
	OP_SDR = 0x2D,	
	OP_SWR = 0x2E,
	OP_CACHE = 0x2F,
	//OP_LL = 0x30,
	OP_LWC1 = 0x31,
	//OP_LLD = 0x34,	//Not implemented
	OP_LDC1 = 0x35,
	OP_LD = 0x37,
	//OP_SC = 0x38,
	OP_SWC1 = 0x39,
	//OP_SCD = 0x3C,	//Not implemented	
	OP_SDC1 = 0x3D,	
	OP_SD = 0x3F,
};

#define OPCODE(op) ((op.instr >> 26) & 0x3F)

/*********************************************************************************************************
 *  Callback functions from the core
 */

void DebugMessage(int level, const char *message, ...)
{
  char msgbuf[1024];
  va_list args;

  va_start(args, message);
  vsnprintf(msgbuf, 1024, message, args);

  DebugCallback("UI-Console", level, msgbuf);

  va_end(args);
}

void DebugCallback(void *Context, int level, const char *message)
{
#ifdef ANDROID
    if (level == M64MSG_ERROR)
        __android_log_print(ANDROID_LOG_ERROR, (const char *) Context, "%s", message);
    else if (level == M64MSG_WARNING)
        __android_log_print(ANDROID_LOG_WARN, (const char *) Context, "%s", message);
    else if (level == M64MSG_INFO)
        __android_log_print(ANDROID_LOG_INFO, (const char *) Context, "%s", message);
    else if (level == M64MSG_STATUS)
        __android_log_print(ANDROID_LOG_DEBUG, (const char *) Context, "%s", message);
    else if (level == M64MSG_VERBOSE)
    {
        if (g_Verbose)
            __android_log_print(ANDROID_LOG_VERBOSE, (const char *) Context, "%s", message);
    }
    else
        __android_log_print(ANDROID_LOG_ERROR, (const char *) Context, "Unknown: %s", message);
#else
    if (level == M64MSG_ERROR)
        printf("%s Error: %s\n", (const char *) Context, message);
    else if (level == M64MSG_WARNING)
        printf("%s Warning: %s\n", (const char *) Context, message);
    else if (level == M64MSG_INFO)
        printf("%s: %s\n", (const char *) Context, message);
    else if (level == M64MSG_STATUS)
        printf("%s Status: %s\n", (const char *) Context, message);
    else if (level == M64MSG_VERBOSE)
    {
        if (g_Verbose)
            printf("%s: %s\n", (const char *) Context, message);
    }
    else
        printf("%s Unknown: %s\n", (const char *) Context, message);
#endif
}

static uint64_t get_address_value(uint32_t address, uint32_t num_bytes)
{
	if(num_bytes >= 8)
		return DebugMemRead64(address);
	else if(num_bytes >= 4)
		return DebugMemRead32(address);
	else if(num_bytes >= 2)
		return DebugMemRead16(address);
	else if(num_bytes >= 1)
		return DebugMemRead8(address);
	else
		DebugMessage(M64MSG_ERROR, "invalid address size");

	return 0;
}

static void set_status_message(const std::string& text)
{
	SetWindowText(l_statusMsg, text.c_str());
}

static void update_progress_bar(uint32_t current, uint32_t total)
{
	SendMessage(l_progressBar, PBM_SETPOS, current, total);
}

static void update_status_message(uint32_t current, uint32_t total)
{
	std::stringstream stream;

	switch(l_analyserState)
	{
		case AnalyzerState::ANALYZING: 
			stream << "Analyzing... (" << current << " out of " << total << ")";
			break;
		case AnalyzerState::STEPPING_AND_LOGGING: 
			stream << "Logging instructions... (" << total << " logged)";
			break;
		case AnalyzerState::SEARCHING:
			stream << "Searching... (" << current << " out of " << total << ")";
			break;
		default: 
			return;
	}

	set_status_message(stream.str().c_str());	
}

static void start_timer()
{
	l_prevTime = GetTickCount();
}

static bool check_timer(uint32_t interval)
{
	uint32_t time = GetTickCount();

	if((time - l_prevTime) > interval)
	{
		l_prevTime = time;

		return true;
	}

	return false;
}

static void update_watch_values()
{
	l_watchListMutex.lock();
	{
		for(auto& watch : l_watchList)
		{
			watch.value = get_address_value(watch.address, watch.size);
		}
	}
	l_watchListMutex.unlock();
}

static void update_realtime()
{
	SendMessage(l_window, WM_UPDATE_REALTIME, NULL, NULL);
}

static void set_cancel_clicked(bool cancelClicked)
{
	l_cancelMutex.lock();
	l_cancelClicked = cancelClicked;
	l_cancelMutex.unlock();
}

static bool check_cancel_clicked()
{
	bool cancel_clicked = false;

	if(l_cancelMutex.try_lock())
	{
		cancel_clicked = l_cancelClicked;

		l_cancelMutex.unlock();
	}

	return cancel_clicked;
}

static void set_analyzer_state(AnalyzerState analyzerState)
{
	l_analyzerStateMutex.lock();
	l_analyserState = analyzerState;
	l_analyzerStateMutex.unlock();
}

static AnalyzerState get_analyzer_state()
{
	AnalyzerState analyzerState = AnalyzerState::IDLE;

	l_analyzerStateMutex.lock();
	analyzerState = l_analyserState;
	l_analyzerStateMutex.unlock();

	return analyzerState;
}

static void set_analyzer_result(AnalyzerResult analyzerResult)
{
	l_analyzerResultMutex.lock();
	l_analyzerResult = analyzerResult;
	l_analyzerResultMutex.unlock();
}

static AnalyzerResult get_analyzer_result()
{
	AnalyzerResult analyzerResult = AnalyzerResult::NONE;

	l_analyzerResultMutex.lock();
	analyzerResult = l_analyzerResult;
	l_analyzerResultMutex.unlock();

	return analyzerResult;
}

static void send_search_aborted_message()
{
	SendMessage(l_searchWnd, WM_ABORT, NULL, NULL);
}

template<typename T>
static bool evaluate_internal(T value, T comparator, T rangeStart, T rangeEnd, SearchOperator searchOperator)
{
	switch(searchOperator)
	{
		case SearchOperator::EQUAL_TO:
			return value == comparator;

		case SearchOperator::NOT_EQUAL_TO:
			return value != comparator;

		case SearchOperator::LESS_THAN:
			return value < comparator;

		case SearchOperator::GREATER_THAN:
			return value > comparator;

		case SearchOperator::LESS_THAN_OR_EQUAL_TO:
			return value <= comparator;

		case SearchOperator::GREATER_THAN_OR_EQUAL_TO:
			return value >= comparator;

		case SearchOperator::IN_RANGE:
			return (value >= rangeStart) && (value <= rangeEnd);
	}

	return false;
}

static bool evaluate(uint32_t value, uint32_t comparator, uint32_t rangeStart, uint32_t rangeEnd)
{
	switch(l_searchFormat)
	{
	case 0:
	case 1:
		return evaluate_internal<uint32_t>(value, comparator, rangeStart, rangeEnd, l_searchOperator);
	case 2:
		return evaluate_internal<int32_t>((int32_t)value, (int32_t)comparator, (int32_t)rangeStart, (int32_t)rangeEnd, l_searchOperator);
	case 3:
		return evaluate_internal<float>((float)value, (float)comparator, (float)rangeStart, (float)rangeEnd, l_searchOperator);
	}

	return false;
}

static bool search_initial()
{
	for(unsigned int i = 0; i < 0x800000; i += l_searchSize)
	{
		uint32_t value = (uint32_t)get_address_value(0x80000000 + i, l_searchSize);
		uint32_t comparator = l_searchSpecificValue;
		uint32_t rangeStart = l_searchRangeStart;
		uint32_t rangeEnd = l_searchRangeEnd;

		if(evaluate(value, comparator, rangeStart, rangeEnd))
			l_tempSearchResults.push_back(SearchResult(i, value));
		
		if(check_timer(15))
		{
			if(check_cancel_clicked())
			{
				send_search_aborted_message();
				return false;
			}

			update_progress_bar(i, 0x800000);
			update_status_message(i, 0x800000);
		}
	}
	
	return true;
}

static bool search_regular()
{
	for(unsigned int i = 0; i < l_searchResults.size(); i++)
	{
		const auto& searchResult = l_searchResults[i];

		uint32_t address = searchResult.address;
		uint32_t value = (uint32_t)get_address_value(0x80000000 + address, l_searchSize);
		uint32_t comparator = 0;
		uint32_t rangeStart = l_searchRangeStart;
		uint32_t rangeEnd = l_searchRangeEnd;

		if(l_searchComparator == SearchComparator::PREVIOUS_VALUE)
			comparator = searchResult.value;
		else
			comparator = l_searchSpecificValue;
		
		if(evaluate(value, comparator, rangeStart, rangeEnd))
			l_tempSearchResults.push_back(SearchResult(address, value));

		if(check_timer(15))
		{
			if(check_cancel_clicked())
			{
				send_search_aborted_message();
				return false;
			}

			update_progress_bar(i, l_searchResults.size());
			update_status_message(i, l_searchResults.size());
		}
	}

	return true;
}

static void send_search_completed_message()
{
	SendMessage(l_searchWnd, WM_SEARCH_COMPLETED, NULL, NULL);
}

static void	update_search_results()
{
	if(l_searchWnd == NULL)
		return;

	InvalidateRect(l_searchListView, NULL, FALSE);
}

static void FrameCallback(unsigned int FrameIndex)
{
}

static const char* get_mnemonic(uint32_t reg)
{	
	if(reg & REG_LO)
		return "$LO";

	if(reg & REG_HI)
		return "$HI";	
	
	if(reg & REG_CC)
		return "$CC";

	if(reg & REG_COP1)
		return cop1_mnemonic[reg & 0x1F].c_str();

	return register_mnemonic[reg & 0x1F].c_str();
}

static std::string write_op(uint32_t dest, uint32_t lhs, uint32_t rhs, const char* op)
{
	const std::string dest_name = get_mnemonic(dest);
	const std::string lhs_name = get_mnemonic(lhs);
	const std::string rhs_name = get_mnemonic(rhs);

	if(dest == lhs)
		return dest_name + " " + op + "= " + rhs_name;
	else
		return dest_name + " = " + lhs_name + " " + op + " " + rhs_name;
}

static std::string write_op_commutative(uint32_t dest, uint32_t lhs, uint32_t rhs, const char* op)
{
	const std::string dest_name = get_mnemonic(dest);
	const std::string lhs_name = get_mnemonic(lhs);
	const std::string rhs_name = get_mnemonic(rhs);

	if(dest == lhs)
	{
		return dest_name + " " + op + "= " + rhs_name;
	}
	else if(dest == rhs)
	{
		return dest_name + " " + op + "= " + lhs_name;
	}
	else
	{
		return dest_name + " = " + lhs_name + " " + op + " " + rhs_name; 
	}
}

static std::string write_op_immediate_unsigned(uint32_t dest, uint32_t src, uint64_t immediate, const char* op)
{
	const std::string dest_name = get_mnemonic(dest);
	const std::string src_name = get_mnemonic(src);

	std::stringstream stream;

	if(dest == src)
	{
		stream << dest_name << " " << op << "= " << immediate;
	}
	else
	{
		stream << dest_name << " = " << src_name << " " << op << " " << immediate;
	}

	return stream.str();
}

static std::string write_op_immediate_signed(uint32_t dest, uint32_t src, int16_t immediate, const char* op)
{
	const std::string dest_name = get_mnemonic(dest);
	const std::string src_name = get_mnemonic(src);

	std::stringstream stream;

	if(dest == src)
	{
		stream << dest_name << " " << op << "= " << immediate;
	}
	else
	{
		stream << dest_name << " = " << src_name << " " << op << " " << immediate;
	}

	return stream.str();
}

static std::string write_op_immediate_hex(uint32_t dest, uint32_t src, uint32_t immediate, const char* op)
{
	const std::string dest_name = get_mnemonic(dest);
	const std::string src_name = get_mnemonic(src);

	std::stringstream stream;

	if(dest == src)
	{
		stream << std::hex << dest_name << " " << op << "= 0x" << immediate << std::dec;
	}
	else
	{
		stream << std::hex << dest_name << " = " << src_name << " " << op << " 0x" << immediate << std::dec;
	}
	
	return stream.str();
}

static std::string write_op_move(uint32_t dest, uint32_t src)
{
	const std::string dest_name = get_mnemonic(dest);
	const std::string src_name = get_mnemonic(src);

	return dest_name + " = " + src_name;
}

static std::string write_op_move_and_negate(uint32_t dest, uint32_t src)
{
	const std::string dest_name = get_mnemonic(dest);
	const std::string src_name = get_mnemonic(src);

	return dest_name + " = -" + src_name;
}

static std::string write_op_move_immediate(uint32_t dest, uint32_t immediate)
{
	const std::string dest_name = get_mnemonic(dest);
	
	std::stringstream stream;
	
	stream << dest_name << " = 0x" << std::hex << immediate << std::dec;

	return stream.str();
}

static std::string write_op_move_from_address(uint32_t dest, uint32_t address)
{
	const std::string dest_name = get_mnemonic(dest);
	
	std::stringstream stream;

	stream << dest_name << " = 0x" << std::hex << address << std::dec;

	return stream.str();
}

static std::string write_op_move_to_address(uint32_t src, uint32_t address)
{
	const std::string src_name = get_mnemonic(src);
	
	std::stringstream stream;

	stream << "0x" << std::hex << address << std::dec << " = " << src_name;

	return stream.str();
}

static std::string write_op_function(uint32_t dest, uint32_t src, const char* func)
{
	const std::string dest_name = get_mnemonic(dest);
	const std::string src_name = get_mnemonic(src);

	return dest_name + " = " + func + "(" + src_name + ")"; 
}

static std::string decode_op(const Operation& operation)
{
	char op[100];
	char args[100];

	uint32_t instr = operation.instr;
	uint32_t pc = operation.pc;

	DebugDecodeOp(instr, op, args, pc);

	return std::string(op) + " " + std::string(args);
}

static std::string write_hex(uint32_t value, uint32_t width = 0)
{
	std::stringstream stream;

	stream << std::uppercase << std::setfill('0') << std::hex << "0x";
	
	if(width > 0)
		stream << std::setw(width);
	
	stream  << value << std::dec;

	return stream.str();
}

static std::string write_size(uint32_t size)
{
	std::stringstream stream;

	stream << size;

	return stream.str();
}

static void write_op_full(const Operation& operation, std::string func)
{
	std::string pc = write_hex(operation.pc);
	std::string decoded = decode_op(operation);

#ifdef CREATE_RESULT_FILE
	l_resultFile << std::left << std::setw(20) << pc;
	l_resultFile << std::left << std::setw(30) << func;
	l_resultFile << decoded << std::endl;
#endif
	
	l_listItems.push_back(ListItem(pc, func, decoded));
}

#ifdef CREATE_VERIFICATION_FILE
static void write_verification(const Operation& operation)
{
	std::string pc = write_hex(operation.pc);
	std::string decoded = decode_op(operation);

	l_verificationFile << std::left << std::setw(20) << pc;
	l_verificationFile << decoded << std::endl;
}
#endif

static void add_found_operation(const Operation& operation)
{
	l_tempFoundOperations.push_back(operation);
}

#define WRITE_OP(func)	write_op_full(operation, func); \
						add_found_operation(operation)

#define COP1(reg) reg | REG_COP1

#define RS(op) ((op.instr >> 21) & 0x1F)
#define RT(op) ((op.instr >> 16) & 0x1F)
#define RD(op) ((op.instr >> 11) & 0x1F)
#define SA(op) ((op.instr >> 6) & 0x1F)
#define FT(op) COP1((op.instr >> 16) & 0x1F)
#define FS(op) COP1((op.instr >> 11) & 0x1F)
#define FD(op) COP1((op.instr >> 6) & 0x1F)
#define IMMEDIATE(op) (op.instr & 0xFFFF)

static bool is_load(const Operation& operation)
{
	uint32_t opcode = OPCODE(operation);

	switch(opcode)
	{
	case OP_LDL:
	case OP_LDR:
	case OP_LB:
	case OP_LH:
	case OP_LWL:
	case OP_LW:
	case OP_LBU:
	case OP_LHU:
	case OP_LWR:
	case OP_LWU:
	case OP_LWC1:
	case OP_LDC1:
	case OP_LD:
		return true;
	}

	return false;
}

static bool is_store(const Operation& operation)
{
	uint32_t opcode = OPCODE(operation);

	switch(opcode)
	{
	case OP_SB:	
	case OP_SH:
	case OP_SWL:
	case OP_SW:
	case OP_SDL:
	case OP_SDR:
	case OP_SWR:
	case OP_SWC1:
	case OP_SDC1:	
	case OP_SD:
		return true;
	}

	return false;
}

static bool is_store_right(const Operation& operation)
{
	uint32_t opcode = OPCODE(operation);

	switch(opcode)
	{
	case OP_SDR:
	case OP_SWR:
		return true;
	}

	return false;
}

static bool is_load_store(const Operation& operation)
{
	return is_load(operation) || is_store(operation);
}

static uint32_t get_num_bytes(const Operation& operation)
{
	uint32_t opcode = OPCODE(operation);

	switch(opcode)
	{
	case OP_LB:
	case OP_LBU:
	case OP_SB: 
		return 1;
	case OP_LH:
	case OP_LHU:
	case OP_SH: 
		return 2;
	case OP_LW:
	case OP_LWU:
	case OP_LWC1:
	case OP_SW: 
	case OP_SWC1: 
		return 4;
	case OP_LD:
	case OP_LDC1:
	case OP_SD: 
	case OP_SDC1: 
		return 8;
	case OP_LWL:
	case OP_SWL: 
		return 4 - (operation.address & 0x03);
	case OP_LDL:
	case OP_SDL: 
		return 8 - (operation.address & 0x07);
	case OP_LWR:
	case OP_SWR: 
		return (operation.address & 0x03) + 1;
	case OP_LDR:
	case OP_SDR: 
		return (operation.address & 0x07) + 1;
	}

	return 0;
}

static bool is_valid_address(uint32_t address)
{
	return (address > 0);
}

static bool is_temporary_address(uint32_t address)
{
	return ((address & 0x803B3000) == 0x803B3000);
}

static bool is_overlapping(uint32_t x_start, uint32_t x_size, uint32_t y_start, uint32_t y_size)
{
	uint32_t x_end = x_start + x_size - 1;
	uint32_t y_end = y_start + y_size - 1;

	return ((x_start <= y_end) && (y_start <= x_end));
}

static uint32_t get_address(const Operation& operation)
{
	uint32_t base = (operation.instr >> 21) & 0x1F;
	int16_t offset = (int16_t)(operation.instr & 0xFFFF);

	int64_t* reg_ptr = (int64_t*)DebugGetCPUDataPtr(M64P_CPU_REG_REG);
	int32_t base_address = (int32_t)*(reg_ptr + base);

	return (base_address + offset);
}

static bool writes_target_address(const Operation& operation, uint32_t target_address, uint32_t target_size)
{
	if(!is_store(operation))
		return false;

	uint32_t address = operation.address;
	uint32_t num_bytes = get_num_bytes(operation);
		
	if(is_store_right(operation))
		address -= (num_bytes - 1);

	return is_overlapping(target_address, target_size, address, num_bytes);
}

static bool is_nop(const Operation& operation)
{
	return (operation.instr == 0);
}

static bool is_special(const Operation& operation)
{
	return (OPCODE(operation) == OP_SPECIAL);
}

static bool is_regimm(const Operation& operation)
{
	return (OPCODE(operation) == OP_REGIMM);
}

static bool is_cop0(const Operation& operation)
{
	return (OPCODE(operation) == OP_COP0);
}

static bool is_cop1(const Operation& operation)
{
	return (OPCODE(operation) == OP_COP1);
}

static Result get_result(const Operation& operation)
{
	Result result;

	uint32_t reg_dst = 0;

	if(is_nop(operation))
	{
		return result;
	}
	else if(is_special(operation))
	{
		uint32_t special_opcode = operation.instr & 0x3F;

		switch(special_opcode)
		{			
			case OP_JR:
			case OP_JALR:
			case OP_SYNC:
			case OP_TEQ:
				result.size = 0; 
				break;

			case OP_SLL:
			case OP_SRL:
			case OP_SRA: 
			case OP_SLLV:
			case OP_SRLV:
			case OP_SRAV:	
			case OP_MULT:
			case OP_MULTU:
			case OP_DIV:
			case OP_DIVU:
			case OP_ADD:
			case OP_ADDU:
			case OP_SUB:
			case OP_SUBU:
				result.size = 4; 
				break;

			case OP_MFHI:
			case OP_MTHI:
			case OP_MFLO:
			case OP_MTLO:
			case OP_DSLLV:
			case OP_DSRLV:
			case OP_DSRAV:
			case OP_DMULT:
			case OP_DMULTU:
			case OP_DDIV:
			case OP_DDIVU:
			case OP_AND:
			case OP_OR:
			case OP_XOR:
			case OP_NOR:
			case OP_SLT:
			case OP_SLTU:
			case OP_DADD:
			case OP_DADDU:
			case OP_DSUB:
			case OP_DSUBU:
			case OP_DSLL:
			case OP_DSRL:
			case OP_DSRA:
			case OP_DSLL32:
			case OP_DSRL32:
			case OP_DSRA32:
				result.size = 8; 
				break;			
		}

		// TODO: Handle $HI results
		switch(special_opcode)
		{
			case OP_MULT:
			case OP_MULTU:
			case OP_DIV:
			case OP_DIVU:
			case OP_MTLO:
			case OP_DMULT:
			case OP_DMULTU:
			case OP_DDIV:
			case OP_DDIVU:
				reg_dst = REG_LO; 
				break;

			case OP_MTHI:
				reg_dst = REG_HI; 
				break;

			default:
				reg_dst = RD(operation); 
				break;
		}
	}
	else if(is_regimm(operation))
	{
		return result;
	}
	else if(is_cop0(operation))
	{
		return result;
	}
	else if(is_cop1(operation))
	{	
		uint32_t cop1_opcode = (operation.instr >> 21) & 0x1F;

		switch(cop1_opcode)
		{
			case OP_BC1:
				result.size = 0; 
				break;

			case OP_MFC1:
			case OP_MTC1:
				result.size = 4; 
				break;
			
			case OP_DMFC1:
			case OP_DMTC1:
				result.size = 8; 
				break;

			case OP_COP1_S:
			{
				uint32_t opcode = (operation.instr & 0x3F);

				switch(opcode)
				{
				case OP_C_F_S:
				case OP_C_UN_S:
				case OP_C_EQ_S:
				case OP_C_UEQ_S:
				case OP_C_OLT_S:
				case OP_C_ULT_S:
				case OP_C_OLE_S:
				case OP_C_ULE_S:
				case OP_C_SF_S:
				case OP_C_NGLE_S:
				case OP_C_SEQ_S:
				case OP_C_NGL_S:
				case OP_C_LT_S:
				case OP_C_NGE_S:
				case OP_C_LE_S:
				case OP_C_NGT_S:
					result.size = 0;
					break;

				case OP_ADD_S:
				case OP_SUB_S:
				case OP_MUL_S:
				case OP_DIV_S:
				case OP_SQRT_S:
				case OP_ABS_S:
				case OP_MOV_S:
				case OP_NEG_S:
				case OP_ROUND_W_S:
				case OP_TRUNC_W_S:
				case OP_CEIL_W_S:
				case OP_FLOOR_W_S:
				case OP_CVT_W_S:
					result.size = 4;
					break;
				
				case OP_ROUND_L_S:
				case OP_TRUNC_L_S:
				case OP_CEIL_L_S:
				case OP_FLOOR_L_S:
				case OP_CVT_D_S:
				case OP_CVT_L_S:
					result.size = 8;
					break;
				}

				break;
			}

			case OP_COP1_D:
			{
				uint32_t opcode = (operation.instr & 0x3F);

				switch(opcode)
				{
					case OP_C_F_D:
					case OP_C_UN_D:
					case OP_C_EQ_D:
					case OP_C_UEQ_D:
					case OP_C_OLT_D:
					case OP_C_ULT_D:
					case OP_C_OLE_D:
					case OP_C_ULE_D:
					case OP_C_SF_D:
					case OP_C_NGLE_D:
					case OP_C_SEQ_D:
					case OP_C_NGL_D:
					case OP_C_LT_D:
					case OP_C_NGE_D:
					case OP_C_LE_D:
					case OP_C_NGT_D:
						result.size = 0;
						break;

					case OP_ROUND_W_D:
					case OP_TRUNC_W_D:
					case OP_CEIL_W_D:
					case OP_FLOOR_W_D:
					case OP_CVT_S_D:
					case OP_CVT_W_D:
						result.size = 4;
						break;
			
					case OP_ADD_D:
					case OP_SUB_D:
					case OP_MUL_D:
					case OP_DIV_D:
					case OP_SQRT_D:
					case OP_ABS_D:
					case OP_MOV_D:
					case OP_NEG_D:	
					case OP_ROUND_L_D:
					case OP_TRUNC_L_D:
					case OP_CEIL_L_D:
					case OP_FLOOR_L_D:			
					case OP_CVT_L_D:
						result.size = 8;
						break;
				}

				break;
			}

			case OP_COP1_W:
			{
				uint32_t opcode = (operation.instr & 0x3F);

				switch(opcode)
				{
					case OP_CVT_S_W:
						result.size = 4;
						break;

					case OP_CVT_D_W:
						result.size = 8;
						break;
				}

				break;
			}

			case OP_COP1_L:
			{
				uint32_t opcode = (operation.instr & 0x3F);

				switch(opcode)
				{
					case OP_CVT_S_L:
						result.size = 4;
						break;

					case OP_CVT_D_L:
						result.size = 8;
						break;
				}

				break;
			}
		}

		switch(cop1_opcode)
		{
		case OP_MFC1:
		case OP_DMFC1:
			reg_dst = RT(operation);
			break;
		case OP_MTC1:
		case OP_DMTC1:
			reg_dst = FS(operation);
			break;
		default: 
			reg_dst = FD(operation);
			break;
		}
	}
	else // I-type
	{
		reg_dst = RT(operation);

		if(is_store(operation))
		{
			result.size = get_num_bytes(operation);
			result.value = get_address_value(operation.address, result.size);

			return result;
		}
		else if(is_load(operation))
		{
			if(	OPCODE(operation) == OP_LWC1 ||
				OPCODE(operation) == OP_LDC1)
				reg_dst = COP1(reg_dst);

			result.size = get_num_bytes(operation);
		}
		else
		{
			switch(OPCODE(operation))
			{
				case OP_J:
				case OP_JAL:
				case OP_BEQ:
				case OP_BNE:
				case OP_BLEZ:
				case OP_BGTZ:
				case OP_BEQL:
				case OP_BNEL:
				case OP_BLEZL:
				case OP_BGTZL:
					result.size = 0;
					break;
				case OP_ADDI:
				case OP_ADDIU:
				case OP_SLTI:
				case OP_SLTIU:
				case OP_ANDI:
				case OP_ORI:
				case OP_XORI:
				case OP_LUI:
					result.size = 4;
					break;
				case OP_DADDI:
				case OP_DADDIU:
					result.size = 8;
					break;
				default:
					return result;
			}
		}
	}

	int64_t* reg_ptr = nullptr;
	uint64_t reg_val = 0;

	if(reg_dst == REG_LO)
	{
		reg_ptr = (int64_t*)DebugGetCPUDataPtr(M64P_CPU_REG_LO);
		reg_dst = 0;
	}
	else if(reg_dst == REG_HI)
	{
		reg_ptr = (int64_t*)DebugGetCPUDataPtr(M64P_CPU_REG_HI);
		reg_dst = 0;
	}
	else if((reg_dst & REG_COP1) == REG_COP1)
	{
		reg_dst ^= REG_COP1;

		if(result.size == 4)
		{
			float** simple_ptr = (float**)DebugGetCPUDataPtr(M64P_CPU_REG_COP1_SIMPLE_PTR);

			result.value = **(uint32_t**)(simple_ptr + reg_dst);
		}
		else
		{
			double** double_ptr = (double**)DebugGetCPUDataPtr(M64P_CPU_REG_COP1_DOUBLE_PTR);
			
			result.value = **(uint64_t**)(double_ptr + reg_dst);
		}

		return result;
	}
	else
	{
		reg_ptr = (int64_t*)DebugGetCPUDataPtr(M64P_CPU_REG_REG);
	}

	result.value = (uint64_t)*(reg_ptr + reg_dst);

	return result;
}

static void DebugInitCallback()
{
	DebugSetRunState(M64P_DBG_RUNSTATE_RUNNING);
}

static void DebugUpdateCallback(unsigned int instruction, unsigned int pc)
{
	l_analyzerStateMutex.lock();

	switch(l_analyserState)
	{
		case AnalyzerState::STEPPING_AND_LOGGING:
		{
			if(!l_tempOperations.empty())
			{
				Result result = get_result(*l_tempOperations.rbegin());

				l_tempResults.push_back(result);
			}

			if(l_lastStep)
			{
				l_analyserState = AnalyzerState::ANALYZING;
			}
			else
			{				
				Operation operation(instruction, pc);

				if(is_load_store(operation))
					operation.address = get_address(operation);			

				l_tempOperations.push_back(operation);

				if(writes_target_address(operation, l_address, l_size))
					l_lastStep = true;
			
				if(check_timer(30))
					update_status_message(0, l_tempOperations.size());
			}
			
			DebugContinue();

			break;
		}

		case AnalyzerState::DEBUGGING:
		{
			SendMessage(l_window, WM_BREAKPOINT_HIT, NULL, (LPARAM)pc);
			break;
		}
	}

	l_analyzerStateMutex.unlock();
}

static void DebugVICallback()
{
	l_analyzerStateMutex.lock();
	{
		switch(l_analyserState)
		{
			case AnalyzerState::WAITING_FOR_VI_CALLBACK:
			{
				l_analyserState = AnalyzerState::STEPPING_AND_LOGGING;

				update_status_message(0, 0);
				start_timer();		
				DebugSetRunState(M64P_DBG_RUNSTATE_PAUSED);

				break;
			}

			case AnalyzerState::STEPPING_AND_LOGGING:
			{
				set_analyzer_result(AnalyzerResult::ADDRESS_WAS_NEVER_WRITTEN);
				set_cancel_clicked(true);

				break;
			}

			case AnalyzerState::SEARCHING:
			{
				bool completed = false;

				if(l_searchResults.empty())
					completed = search_initial();
				else
					completed = search_regular();

				if(completed)
					send_search_completed_message();
				else
					send_search_aborted_message();

				break;
			}
		}
	}
	l_analyzerStateMutex.unlock();

	update_watch_values();
	update_realtime();
	update_search_results();
}

static void reset()
{	
	set_analyzer_state(AnalyzerState::IDLE);
	set_analyzer_result(AnalyzerResult::NONE);

	l_registers.clear();
	l_addresses.clear();
	l_tempOperations.clear();
	l_tempResults.clear();
	l_tempFoundOperations.clear();
	l_tempIndices.clear();
	l_listItems.clear();

	l_lastStep = false;

	set_cancel_clicked(false);
}

static void add_register(uint32_t reg)
{
	l_registers.insert(reg);
}

static void remove_register(uint32_t reg)
{
	l_registers.erase(reg);
}

static bool has_register(uint32_t reg)
{
	if(reg == 0)
		return false;

	return (l_registers.find(reg) != l_registers.cend());
}

static void add_address(uint32_t address)
{
	l_addresses.insert(address);
}

static void remove_address(uint32_t address)
{
	l_addresses.erase(address);
}

static bool has_address(uint32_t address)
{
	return (l_addresses.find(address) != l_addresses.cend());
}

static void sll(const Operation& operation)
{
	uint32_t rt = RT(operation);
	uint32_t rd = RD(operation);
	uint32_t sa = SA(operation);

	if(rd == 0)
		return;

	if(has_register(rd))
	{
		remove_register(rd);
		add_register(rt);

		WRITE_OP(write_op_immediate_unsigned(rd, rt, sa, "<<"));
	}
}

static void srl(const Operation& operation)
{
	uint32_t rt = RT(operation);
	uint32_t rd = RD(operation);
	uint32_t sa = SA(operation);

	if(rd == 0)
		return;

	if(has_register(rd))
	{
		remove_register(rd);
		add_register(rt);
		
		WRITE_OP(write_op_immediate_unsigned(rd, rt, sa, ">>"));
	}
}

static void sra(const Operation& operation)
{
	uint32_t rt = RT(operation);
	uint32_t rd = RD(operation);
	uint32_t sa = SA(operation);

	if(rd == 0)
		return;

	if(has_register(rd))
	{
		remove_register(rd);
		add_register(rt);

		WRITE_OP(write_op_immediate_unsigned(rd, rt, sa, ">>"));
	}
}

static void sllv(const Operation& operation)
{
	uint32_t rt = RT(operation);
	uint32_t rd = RD(operation);
	uint32_t rs = RS(operation);
	
	if(rd == 0)
		return;

	if(has_register(rd))
	{
		remove_register(rd);
		add_register(rt);
		add_register(rs);
		
		WRITE_OP(write_op(rd, rt, rs, "<<"));
	}
}

static void srlv(const Operation& operation)
{
	uint32_t rt = RT(operation);
	uint32_t rd = RD(operation);
	uint32_t rs = RS(operation);
	
	if(rd == 0)
		return;

	if(has_register(rd))
	{
		remove_register(rd);
		add_register(rt);
		add_register(rs);
		
		WRITE_OP(write_op(rd, rt, rs, ">>"));
	}
}


static void srav(const Operation& operation)
{
	uint32_t rt = RT(operation);
	uint32_t rd = RD(operation);
	uint32_t rs = RS(operation);
	
	if(rd == 0)
		return;

	if(has_register(rd))
	{
		remove_register(rd);
		add_register(rt);
		add_register(rs);
		
		WRITE_OP(write_op(rd, rt, rs, ">>"));
	}
}

static void jr(const Operation& operation)
{
	// Do nothing
}

static void jalr(const Operation& operation)
{
	// Do nothing
}

static void sync(const Operation& operation)
{
	//Do nothing
}

static void mfhi(const Operation& operation)
{
	uint32_t rd = RD(operation);

	if(rd == 0)
		return;

	if(has_register(rd))
	{
		remove_register(rd);
		add_register(REG_HI);

		WRITE_OP(write_op_move(rd, REG_HI));
	}
}

static void mthi(const Operation& operation)
{	
	uint32_t rs = RS(operation);

	if(has_register(REG_HI))
	{
		remove_register(REG_HI);
		add_register(rs);
		
		WRITE_OP(write_op_move(REG_HI, rs));
	}
}

static void mflo(const Operation& operation)
{
	uint32_t rd = RD(operation);

	if(rd == 0)
		return;

	if(has_register(rd))
	{
		remove_register(rd);
		add_register(REG_LO);
		
		WRITE_OP(write_op_move(rd, REG_LO));
	}
}

static void mtlo(const Operation& operation)
{
	uint32_t rs = RS(operation);

	if(has_register(REG_LO))
	{
		remove_register(REG_LO);
		add_register(rs);
		
		WRITE_OP(write_op_move(REG_LO, rs));
	}
}

static void dsllv(const Operation& operation)
{
	uint32_t rs = RS(operation);
	uint32_t rt = RT(operation);
	uint32_t rd = RD(operation);

	if(rd == 0)
		return;

	if(has_register(rd))
	{
		remove_register(rd);
		add_register(rs);
		add_register(rt);

		WRITE_OP(write_op(rd, rt, rs, "<<"));
	}
}

static void dsrlv(const Operation& operation)
{
	uint32_t rs = RS(operation);
	uint32_t rt = RT(operation);
	uint32_t rd = RD(operation);

	if(rd == 0)
		return;

	if(has_register(rd))
	{
		remove_register(rd);
		add_register(rs);
		add_register(rt);
		
		WRITE_OP(write_op(rd, rt, rs, ">>"));
	}
}

static void dsrav(const Operation& operation)
{
	uint32_t rs = RS(operation);
	uint32_t rt = RT(operation);
	uint32_t rd = RD(operation);

	if(rd == 0)
		return;

	if(has_register(rd))
	{
		remove_register(rd);
		add_register(rs);
		add_register(rt);
		
		WRITE_OP(write_op(rd, rt, rs, ">>"));
	}
}

static void mult(const Operation& operation)
{
	uint32_t rs = RS(operation);
	uint32_t rt = RT(operation);

	if(has_register(REG_HI) || has_register(REG_LO))
	{
		if(has_register(REG_HI))
			remove_register(REG_HI);
			
		if(has_register(REG_LO))
			remove_register(REG_LO);

		add_register(rs);
		add_register(rt);
		
		WRITE_OP(write_op_commutative(REG_LO | REG_HI, rs, rt, "*"));
	}
}

static void multu(const Operation& operation)
{
	uint32_t rs = RS(operation);
	uint32_t rt = RT(operation);

	if(has_register(REG_HI) || has_register(REG_LO))
	{
		if(has_register(REG_HI))
			remove_register(REG_HI);
			
		if(has_register(REG_LO))
			remove_register(REG_LO);

		add_register(rs);
		add_register(rt);
		
		WRITE_OP(write_op_commutative(REG_LO | REG_HI, rs, rt, "*"));
	}
}

static void div(const Operation& operation)
{
	uint32_t rs = RS(operation);
	uint32_t rt = RT(operation);

	if(has_register(REG_HI) || has_register(REG_LO))
	{
		if(has_register(REG_HI))
			remove_register(REG_HI);
			
		if(has_register(REG_LO))
			remove_register(REG_LO);

		add_register(rs);
		add_register(rt);
		
		WRITE_OP(write_op(REG_LO | REG_HI, rs, rt, "/"));
	}
}

static void divu(const Operation& operation)
{
	uint32_t rs = RS(operation);
	uint32_t rt = RT(operation);

	if(has_register(REG_HI) || has_register(REG_LO))
	{
		if(has_register(REG_HI))
			remove_register(REG_HI);
			
		if(has_register(REG_LO))
			remove_register(REG_LO);

		add_register(rs);
		add_register(rt);
		
		WRITE_OP(write_op(REG_LO | REG_HI, rs, rt, "/"));
	}
}

static void dmult(const Operation& operation)
{
	uint32_t rs = RS(operation);
	uint32_t rt = RT(operation);

	if(has_register(REG_HI) || has_register(REG_LO))
	{
		if(has_register(REG_HI))
			remove_register(REG_HI);
			
		if(has_register(REG_LO))
			remove_register(REG_LO);

		add_register(rs);
		add_register(rt);
		
		WRITE_OP(write_op_commutative(REG_LO | REG_HI, rs, rt, "*"));
	}
}

static void dmultu(const Operation& operation)
{
	uint32_t rs = RS(operation);
	uint32_t rt = RT(operation);

	if(has_register(REG_HI) || has_register(REG_LO))
	{
		if(has_register(REG_HI))
			remove_register(REG_HI);
			
		if(has_register(REG_LO))
			remove_register(REG_LO);

		add_register(rs);
		add_register(rt);
		
		WRITE_OP(write_op_commutative(REG_LO | REG_HI, rs, rt, "*"));
	}
}

static void ddiv(const Operation& operation)
{
	uint32_t rs = RS(operation);
	uint32_t rt = RT(operation);

	if(has_register(REG_HI) || has_register(REG_LO))
	{
		if(has_register(REG_HI))
			remove_register(REG_HI);
			
		if(has_register(REG_LO))
			remove_register(REG_LO);

		add_register(rs);
		add_register(rt);
		
		WRITE_OP(write_op(REG_LO | REG_HI, rs, rt, "/"));
	}
}

static void ddivu(const Operation& operation)
{
	uint32_t rs = RS(operation);
	uint32_t rt = RT(operation);

	if(has_register(REG_HI) || has_register(REG_LO))
	{
		if(has_register(REG_HI))
			remove_register(REG_HI);
			
		if(has_register(REG_LO))
			remove_register(REG_LO);

		add_register(rs);
		add_register(rt);
		
		WRITE_OP(write_op(REG_LO | REG_HI, rs, rt, "/"));
	}
}

static void add(const Operation& operation)
{
	uint32_t rs = RS(operation);
	uint32_t rt = RT(operation);
	uint32_t rd = RD(operation);

	if(rd == 0) 
		return;

	if(has_register(rd))
	{
		remove_register(rd);
		add_register(rs);
		add_register(rt);
		
		WRITE_OP(write_op_commutative(rd, rs, rt, "+"));
	}
}

static void addu(const Operation& operation)
{
	uint32_t rs = RS(operation);
	uint32_t rt = RT(operation);
	uint32_t rd = RD(operation);

	if(rd == 0) 
		return;

	if(has_register(rd))
	{
		remove_register(rd);
		add_register(rs);
		add_register(rt);
		
		WRITE_OP(write_op_commutative(rd, rs, rt, "+"));
	}
}

static void sub(const Operation& operation)
{
	uint32_t rs = RS(operation);
	uint32_t rt = RT(operation);
	uint32_t rd = RD(operation);

	if(rd == 0) 
		return;

	if(has_register(rd))
	{
		remove_register(rd);
		add_register(rs);
		add_register(rt);
		
		WRITE_OP(write_op(rd, rs, rt, "-"));
	}
}

static void subu(const Operation& operation)
{
	uint32_t rs = RS(operation);
	uint32_t rt = RT(operation);
	uint32_t rd = RD(operation);

	if(rd == 0) 
		return;

	if(has_register(rd))
	{
		remove_register(rd);
		add_register(rs);
		add_register(rt);
		
		WRITE_OP(write_op(rd, rs, rt, "-"));
	}
}

static void and(const Operation& operation)
{
	uint32_t rs = RS(operation);
	uint32_t rt = RT(operation);
	uint32_t rd = RD(operation);

	if(rd == 0) 
		return;

	if(has_register(rd))
	{
		remove_register(rd);
		add_register(rs);
		add_register(rt);
		
		WRITE_OP(write_op_commutative(rd, rs, rt, "&"));
	}
}

static void or(const Operation& operation)
{
	uint32_t rs = RS(operation);
	uint32_t rt = RT(operation);
	uint32_t rd = RD(operation);

	if(rd == 0) 
		return;

	if(has_register(rd))
	{
		remove_register(rd);
		add_register(rs);
		add_register(rt);
		
		WRITE_OP(write_op_commutative(rd, rs, rt, "|"));
	}
}

static void xor(const Operation& operation)
{
	uint32_t rs = RS(operation);
	uint32_t rt = RT(operation);
	uint32_t rd = RD(operation);

	if(rd == 0) 
		return;

	if(has_register(rd))
	{
		remove_register(rd);
		add_register(rs);
		add_register(rt);
		
		WRITE_OP(write_op_commutative(rd, rs, rt, "^"));
	}
}

static void nor(const Operation& operation)
{
	uint32_t rs = RS(operation);
	uint32_t rt = RT(operation);
	uint32_t rd = RD(operation);

	if(rd == 0) 
		return;

	if(has_register(rd))
	{
		remove_register(rd);
		add_register(rs);
		add_register(rt);
		
		WRITE_OP(write_op_commutative(rd, rs, rt, "~|"));
	}
}

static void slt(const Operation& operation)
{
	uint32_t rs = RS(operation);
	uint32_t rt = RT(operation);
	uint32_t rd = RD(operation);

	if(rd == 0) 
		return;

	if(has_register(rd))
	{
		remove_register(rd);
		add_register(rs);
		add_register(rt);
		
		WRITE_OP(write_op(rd, rs, rt, "<"));
	}
}

static void sltu(const Operation& operation)
{
	uint32_t rs = RS(operation);
	uint32_t rt = RT(operation);
	uint32_t rd = RD(operation);

	if(rd == 0) 
		return;

	if(has_register(rd))
	{
		remove_register(rd);
		add_register(rs);
		add_register(rt);
		
		WRITE_OP(write_op(rd, rs, rt, "<"));
	}
}

static void dadd(const Operation& operation)
{
	uint32_t rs = RS(operation);
	uint32_t rt = RT(operation);
	uint32_t rd = RD(operation);

	if(rd == 0)
		return;

	if(has_register(rd))
	{
		remove_register(rd);
		add_register(rs);
		add_register(rt);
		
		WRITE_OP(write_op_commutative(rd, rs, rt, "+"));
	}
}

static void daddu(const Operation& operation)
{
	uint32_t rs = RS(operation);
	uint32_t rt = RT(operation);
	uint32_t rd = RD(operation);

	if(rd == 0)
		return;

	if(has_register(rd))
	{
		remove_register(rd);
		add_register(rs);
		add_register(rt);
		
		WRITE_OP(write_op_commutative(rd, rs, rt, "+"));
	}
}

static void dsub(const Operation& operation)
{
	uint32_t rs = RS(operation);
	uint32_t rt = RT(operation);
	uint32_t rd = RD(operation);

	if(rd == 0)
		return;

	if(has_register(rd))
	{
		remove_register(rd);
		add_register(rs);
		add_register(rt);
		
		WRITE_OP(write_op(rd, rs, rt, "-"));
	}
}

static void dsubu(const Operation& operation)
{
	uint32_t rs = RS(operation);
	uint32_t rt = RT(operation);
	uint32_t rd = RD(operation);

	if(rd == 0)
		return;

	if(has_register(rd))
	{
		remove_register(rd);
		add_register(rs);
		add_register(rt);
		
		WRITE_OP(write_op(rd, rs, rt, "-"));
	}
}

static void teq(const Operation& operation)
{
	//Do nothing
}

static void dsll(const Operation& operation)
{
	uint32_t rt = RT(operation);
	uint32_t rd = RD(operation);
	uint32_t sa = SA(operation);

	if(rd == 0) 
		return;

	if(has_register(rd))
	{
		remove_register(rd);
		add_register(rt);		
		
		WRITE_OP(write_op_immediate_unsigned(rd, rt, sa, "<<"));
	}
}

static void dsrl(const Operation& operation)
{
	uint32_t rt = RT(operation);
	uint32_t rd = RD(operation);
	uint32_t sa = SA(operation);

	if(rd == 0) 
		return;

	if(has_register(rd))
	{
		remove_register(rd);
		add_register(rt);
		
		WRITE_OP(write_op_immediate_unsigned(rd, rt, sa, ">>"));
	}
}

static void dsra(const Operation& operation)
{
	uint32_t rt = RT(operation);
	uint32_t rd = RD(operation);
	uint32_t sa = SA(operation);

	if(rd == 0) 
		return;

	if(has_register(rd))
	{
		remove_register(rd);
		add_register(rt);
		
		WRITE_OP(write_op_immediate_unsigned(rd, rt, sa, ">>"));
	}
}

static void dsll32(const Operation& operation)
{
	uint32_t rt = RT(operation);
	uint32_t rd = RD(operation);
	uint32_t sa = SA(operation);

	if(rd == 0) 
		return;

	if(has_register(rd))
	{
		remove_register(rd);
		add_register(rt);
		
		WRITE_OP(write_op_immediate_unsigned(rd, rt, (sa + 32), "<<"));
	}
}

static void dsrl32(const Operation& operation)
{
	uint32_t rt = RT(operation);
	uint32_t rd = RD(operation);
	uint32_t sa = SA(operation);

	if(rd == 0) 
		return;

	if(has_register(rd))
	{
		remove_register(rd);
		add_register(rt);
		
		WRITE_OP(write_op_immediate_unsigned(rd, rt, (sa + 32), ">>"));
	}
}

static void dsra32(const Operation& operation)
{
	uint32_t rt = RT(operation);
	uint32_t rd = RD(operation);
	uint32_t sa = SA(operation);

	if(rd == 0) 
		return;

	if(has_register(rd))
	{
		remove_register(rd);
		add_register(rt);
		
		WRITE_OP(write_op_immediate_unsigned(rd, rt, (sa + 32), ">>"));
	}
}

static void special(const Operation& operation)
{
	uint32_t instr = operation.instr;
	uint32_t opcode = (instr >> 0) & 0x3F;

	switch(opcode)
	{
	case OP_SLL: sll(operation); break;
	case OP_SRL: srl(operation); break;
	case OP_SRA: sra(operation); break;
	case OP_SLLV: sllv(operation); break;
	case OP_SRLV: srlv(operation); break;
	case OP_SRAV: srav(operation); break;
	case OP_JR: jr(operation); break;
	case OP_JALR: jalr(operation); break;
	//case OP_SYSCALL: syscall(operation); break;
	//case OP_BREAK: break(operation); break;	//Not implemented
	case OP_SYNC: sync(operation); break;
	case OP_MFHI: mfhi(operation); break;
	case OP_MTHI: mthi(operation); break;
	case OP_MFLO: mflo(operation); break;
	case OP_MTLO: mtlo(operation); break;
	case OP_DSLLV: dsllv(operation); break;
	case OP_DSRLV: dsrlv(operation); break;
	case OP_DSRAV: dsrav(operation); break;
	case OP_MULT: mult(operation); break;
	case OP_MULTU: multu(operation); break;
	case OP_DIV: div(operation); break;
	case OP_DIVU: divu(operation); break;
	case OP_DMULT: dmult(operation); break;
	case OP_DMULTU: dmultu(operation); break;
	case OP_DDIV: ddiv(operation); break;
	case OP_DDIVU: ddivu(operation); break;
	case OP_ADD: add(operation); break;
	case OP_ADDU: addu(operation); break;
	case OP_SUB: sub(operation); break;
	case OP_SUBU: subu(operation); break;
	case OP_AND: and(operation); break;
	case OP_OR: or(operation); break;
	case OP_XOR: xor(operation); break;
	case OP_NOR: nor(operation); break;
	case OP_SLT: slt(operation); break;
	case OP_SLTU: sltu(operation); break;
	case OP_DADD: dadd(operation); break;
	case OP_DADDU: daddu(operation); break;
	case OP_DSUB: dsub(operation); break;
	case OP_DSUBU: dsubu(operation); break;
	//case OP_TGE: tge(operation); break;		//Not implemented
	//case OP_TGEU: tgeu(operation); break;		//Not implemented
	//case OP_TLT: tlt(operation); break;		//Not implemented
	//case OP_TLTU: tltu(operation); break;		//Not implemented
	case OP_TEQ: teq(operation); break;
	//case OP_TNE: tne(operation); break;		//Not implemented
	case OP_DSLL: dsll(operation); break;
	case OP_DSRL: dsrl(operation); break;
	case OP_DSRA: dsra(operation); break;
	case OP_DSLL32: dsll32(operation); break;
	case OP_DSRL32: dsrl32(operation); break;
	case OP_DSRA32: dsra32(operation); break;
	}
}

static void bltz(const Operation& operation)
{
	/*uint32_t rs = RS(operation);

	add_register(rs);*/
}

static void bgez(const Operation& operation)
{
	/*uint32_t rs = RS(operation);

	add_register(rs);*/
}

static void bltzl(const Operation& operation)
{
	/*uint32_t rs = RS(operation);

	add_register(rs);*/
}

static void bgezl(const Operation& operation)
{
	/*uint32_t rs = RS(operation);

	add_register(rs);*/
}

static void bltzal(const Operation& operation)
{
	/*uint32_t rs = RS(operation);

	add_register(rs);*/
}

static void bgezal(const Operation& operation)
{
	/*uint32_t rs = RS(operation);

	add_register(rs);*/
}

static void bltzall(const Operation& operation)
{
	/*uint32_t rs = RS(operation);

	add_register(rs);*/
}

static void bgezall(const Operation& operation)
{
	/*uint32_t rs = RS(operation);

	add_register(rs);*/
}

static void regimm(const Operation& operation)
{
	uint32_t instr = operation.instr;
	uint32_t opcode = (instr >> 16) & 0x1F;

	switch(opcode)
	{
	case OP_BLTZ: bltz(operation); break;
	case OP_BGEZ: bgez(operation); break;
	case OP_BLTZL: bltzl(operation); break;
	case OP_BGEZL: bgezl(operation); break;
	//case OP_TGEI: tgei(operation); break;		//Not implemented
	//case OP_TGEIU: tgeiu(operation); break;	//Not implemented
	//case OP_TLTI: tlti(operation); break;		//Not implemented
	//case OP_TLTIU: tltiu(operation); break;	//Not implemented
	//case OP_TEQI: teqi(operation); break;		//Not implemented
	//case OP_TNEI: tnei(operation); break;		//Not implemented
	case OP_BLTZAL: bltzal(operation); break;
	case OP_BGEZAL: bgezal(operation); break;
	case OP_BLTZALL: bltzall(operation); break;
	case OP_BGEZALL: bgezall(operation); break;
	}
}

static void cop0(const Operation& operation)
{
	//uint32_t instr = operation.instr;
	//uint32_t opcode = (instr >> 21) & 0x1F;
}

static void bc1f(const Operation& operation)
{
	//add_register(REG_CC);
}

static void bc1t(const Operation& operation)
{
	//add_register(REG_CC);
}

static void bc1fl(const Operation& operation)
{
	//add_register(REG_CC);
}

static void bc1tl(const Operation& operation)
{
	//add_register(REG_CC);
}

static void bc1(const Operation& operation)
{
	uint32_t instr = operation.instr;
	uint32_t opcode = (instr >> 16) & 0x03;

	switch(opcode)
	{
	case OP_BC1F: bc1f(operation); break;
	case OP_BC1T: bc1t(operation); break;
	case OP_BC1FL: bc1fl(operation); break;
	case OP_BC1TL: bc1tl(operation); break;
	}
}

static void add_s(const Operation& operation)
{
	uint32_t ft = FT(operation);
	uint32_t fs = FS(operation);
	uint32_t fd = FD(operation);

	if(has_register(fd))
	{
		remove_register(fd);
		add_register(fs);
		add_register(ft);

		WRITE_OP(write_op_commutative(fd, fs, ft, "+"));
	}
}

static void sub_s(const Operation& operation)
{
	uint32_t ft = FT(operation);
	uint32_t fs = FS(operation);
	uint32_t fd = FD(operation);

	if(has_register(fd))
	{
		remove_register(fd);
		add_register(fs);
		add_register(ft);

		WRITE_OP(write_op(fd, fs, ft, "-"));
	}
}

static void mul_s(const Operation& operation)
{
	uint32_t ft = FT(operation);
	uint32_t fs = FS(operation);
	uint32_t fd = FD(operation);

	if(has_register(fd))
	{
		remove_register(fd);
		add_register(fs);
		add_register(ft);
		
		WRITE_OP(write_op_commutative(fd, fs, ft, "*"));
	}
}

static void div_s(const Operation& operation)
{
	uint32_t ft = FT(operation);
	uint32_t fs = FS(operation);
	uint32_t fd = FD(operation);

	if(has_register(fd))
	{
		remove_register(fd);
		add_register(fs);
		add_register(ft);
		
		WRITE_OP(write_op(fd, fs, ft, "/"));
	}
}

static void sqrt_s(const Operation& operation)
{
	uint32_t fs = FS(operation);
	uint32_t fd = FD(operation);

	if(has_register(fd))
	{
		remove_register(fd);
		add_register(fs);

		WRITE_OP(write_op_function(fd, fs, "sqrt"));
	}
}

static void abs_s(const Operation& operation)
{
	uint32_t fs = FS(operation);
	uint32_t fd = FD(operation);

	if(has_register(fd))
	{
		remove_register(fd);
		add_register(fs);
		
		WRITE_OP(write_op_function(fd, fs, "abs"));
	}
}

static void mov_s(const Operation& operation)
{
	uint32_t fs = FS(operation);
	uint32_t fd = FD(operation);

	if(has_register(fd))
	{
		remove_register(fd);
		add_register(fs);

		WRITE_OP(write_op_move(fd, fs));
	}
}

static void neg_s(const Operation& operation)
{
	uint32_t fs = FS(operation);
	uint32_t fd = FD(operation);

	if(has_register(fd))
	{
		remove_register(fd);
		add_register(fs);

		WRITE_OP(write_op_move_and_negate(fd, fs));
	}
}

static void round_l_s(const Operation& operation)
{
	uint32_t fs = FS(operation);
	uint32_t fd = FD(operation);

	if(has_register(fd))
	{
		remove_register(fd);
		add_register(fs);

		WRITE_OP(write_op_function(fd, fs, "round"));
	}
}

static void trunc_l_s(const Operation& operation)
{
	uint32_t fs = FS(operation);
	uint32_t fd = FD(operation);

	if(has_register(fd))
	{
		remove_register(fd);
		add_register(fs);

		WRITE_OP(write_op_function(fd, fs, "trunc"));
	}
}

static void ceil_l_s(const Operation& operation)
{
	uint32_t fs = FS(operation);
	uint32_t fd = FD(operation);

	if(has_register(fd))
	{
		remove_register(fd);
		add_register(fs);

		WRITE_OP(write_op_function(fd, fs, "ceil"));
	}
}

static void floor_l_s(const Operation& operation)
{
	uint32_t fs = FS(operation);
	uint32_t fd = FD(operation);

	if(has_register(fd))
	{
		remove_register(fd);
		add_register(fs);

		WRITE_OP(write_op_function(fd, fs, "floor"));
	}
}

static void round_w_s(const Operation& operation)
{
	uint32_t fs = FS(operation);
	uint32_t fd = FD(operation);

	if(has_register(fd))
	{
		remove_register(fd);
		add_register(fs);

		WRITE_OP(write_op_function(fd, fs, "round"));
	}
}

static void trunc_w_s(const Operation& operation)
{
	uint32_t fs = FS(operation);
	uint32_t fd = FD(operation);

	if(has_register(fd))
	{
		remove_register(fd);
		add_register(fs);

		WRITE_OP(write_op_function(fd, fs, "trunc"));
	}
}

static void ceil_w_s(const Operation& operation)
{
	uint32_t fs = FS(operation);
	uint32_t fd = FD(operation);

	if(has_register(fd))
	{
		remove_register(fd);
		add_register(fs);

		WRITE_OP(write_op_function(fd, fs, "ceil"));
	}
}

static void floor_w_s(const Operation& operation)
{
	uint32_t fs = FS(operation);
	uint32_t fd = FD(operation);

	if(has_register(fd))
	{
		remove_register(fd);
		add_register(fs);

		WRITE_OP(write_op_function(fd, fs, "floor"));
	}
}

static void cvt_d_s(const Operation& operation)
{
	uint32_t fs = FS(operation);
	uint32_t fd = FD(operation);

	if(has_register(fd))
	{
		remove_register(fd);
		add_register(fs);

		WRITE_OP(write_op_move(fd, fs));
	}
}

static void cvt_w_s(const Operation& operation)
{
	uint32_t fs = FS(operation);
	uint32_t fd = FD(operation);

	if(has_register(fd))
	{
		remove_register(fd);
		add_register(fs);

		WRITE_OP(write_op_move(fd, fs));
	}
}

static void cvt_l_s(const Operation& operation)
{
	uint32_t fs = FS(operation);
	uint32_t fd = FD(operation);

	if(has_register(fd))
	{
		remove_register(fd);
		add_register(fs);

		WRITE_OP(write_op_move(fd, fs));
	}
}

static void c_f_s(const Operation& operation)
{
	//if(has_cop1_register(cc))
	//{
	//	remove_cop1_register(cc);
	//}
}

static void c_un_s(const Operation& operation)
{
	//uint32_t fs = (instruction >> 11) & 0x1F;
	//uint32_t ft = (instruction >> 16) & 0x1F;

	//if(has_cop1_register(cc))
	//{
	//	remove_cop1_register(cc);
	//	add_cop1_register(fs);
	//	add_cop1_register(ft);
	//}
}

static void c_eq_s(const Operation& operation)
{
	//uint32_t fs = (instruction >> 11) & 0x1F;
	//uint32_t ft = (instruction >> 16) & 0x1F;

	//if(has_cop1_register(cc))
	//{
	//	remove_cop1_register(cc);
	//	add_cop1_register(fs);
	//	add_cop1_register(ft);
	//}
}

static void c_ueq_s(const Operation& operation)
{
	//uint32_t fs = (instruction >> 11) & 0x1F;
	//uint32_t ft = (instruction >> 16) & 0x1F;

	//if(has_cop1_register(cc))
	//{
	//	remove_cop1_register(cc);
	//	add_cop1_register(fs);
	//	add_cop1_register(ft);
	//}
}

static void c_olt_s(const Operation& operation)
{
	//uint32_t fs = (instruction >> 11) & 0x1F;
	//uint32_t ft = (instruction >> 16) & 0x1F;

	//if(has_cop1_register(cc))
	//{
	//	remove_cop1_register(cc);
	//	add_cop1_register(fs);
	//	add_cop1_register(ft);
	//}
}

static void c_ult_s(const Operation& operation)
{
	//uint32_t fs = (instruction >> 11) & 0x1F;
	//uint32_t ft = (instruction >> 16) & 0x1F;

	//if(has_cop1_register(cc))
	//{
	//	remove_cop1_register(cc);
	//	add_cop1_register(fs);
	//	add_cop1_register(ft);
	//}
}

static void c_ole_s(const Operation& operation)
{
	//uint32_t fs = (instruction >> 11) & 0x1F;
	//uint32_t ft = (instruction >> 16) & 0x1F;

	//if(has_cop1_register(cc))
	//{
	//	remove_cop1_register(cc);
	//	add_cop1_register(fs);
	//	add_cop1_register(ft);
	//}
}

static void c_ule_s(const Operation& operation)
{
	//uint32_t fs = (instruction >> 11) & 0x1F;
	//uint32_t ft = (instruction >> 16) & 0x1F;

	//if(has_cop1_register(cc))
	//{
	//	remove_cop1_register(cc);
	//	add_cop1_register(fs);
	//	add_cop1_register(ft);
	//}
}

static void c_sf_s(const Operation& operation)
{
	//if(has_cop1_register(cc))
	//{
	//	remove_cop1_register(cc);
	//}
}

static void c_ngle_s(const Operation& operation)
{
	//uint32_t fs = (instruction >> 11) & 0x1F;
	//uint32_t ft = (instruction >> 16) & 0x1F;

	//if(has_cop1_register(cc))
	//{
	//	remove_cop1_register(cc);
	//	add_cop1_register(fs);
	//	add_cop1_register(ft);
	//}
}

static void c_seq_s(const Operation& operation)
{
	//uint32_t fs = (instruction >> 11) & 0x1F;
	//uint32_t ft = (instruction >> 16) & 0x1F;

	//if(has_cop1_register(cc))
	//{
	//	remove_cop1_register(cc);
	//	add_cop1_register(fs);
	//	add_cop1_register(ft);
	//}
}

static void c_ngl_s(const Operation& operation)
{
	//uint32_t fs = (instruction >> 11) & 0x1F;
	//uint32_t ft = (instruction >> 16) & 0x1F;

	//if(has_cop1_register(cc))
	//{
	//	remove_cop1_register(cc);
	//	add_cop1_register(fs);
	//	add_cop1_register(ft);
	//}
}

static void c_lt_s(const Operation& operation)
{
	//uint32_t fs = (instruction >> 11) & 0x1F;
	//uint32_t ft = (instruction >> 16) & 0x1F;

	//if(has_cop1_register(cc))
	//{
	//	remove_cop1_register(cc);
	//	add_cop1_register(fs);
	//	add_cop1_register(ft);
	//}
}

static void c_nge_s(const Operation& operation)
{
	//uint32_t fs = (instruction >> 11) & 0x1F;
	//uint32_t ft = (instruction >> 16) & 0x1F;

	//if(has_cop1_register(cc))
	//{
	//	remove_cop1_register(cc);
	//	add_cop1_register(fs);
	//	add_cop1_register(ft);
	//}
}

static void c_le_s(const Operation& operation)
{
	//uint32_t fs = (instruction >> 11) & 0x1F;
	//uint32_t ft = (instruction >> 16) & 0x1F;

	//if(has_cop1_register(cc))
	//{
	//	remove_cop1_register(cc);
	//	add_cop1_register(fs);
	//	add_cop1_register(ft);
	//}
}

static void c_ngt_s(const Operation& operation)
{
	//uint32_t fs = (instruction >> 11) & 0x1F;
	//uint32_t ft = (instruction >> 16) & 0x1F;

	//if(has_cop1_register(cc))
	//{
	//	remove_cop1_register(cc);
	//	add_cop1_register(fs);
	//	add_cop1_register(ft);
	//}
}

static void cop1_s(const Operation& operation)
{
	uint32_t instr = operation.instr;
	uint32_t opcode = (instr >> 0) & 0x3F;

	switch(opcode)
	{
	case OP_ADD_S: add_s(operation); break;
	case OP_SUB_S: sub_s(operation); break;
	case OP_MUL_S: mul_s(operation); break;
	case OP_DIV_S: div_s(operation); break;
	case OP_SQRT_S: sqrt_s(operation); break;
	case OP_ABS_S: abs_s(operation); break;
	case OP_MOV_S: mov_s(operation); break;
	case OP_NEG_S: neg_s(operation); break;
	case OP_ROUND_L_S: round_l_s(operation); break;
	case OP_TRUNC_L_S: trunc_l_s(operation); break;
	case OP_CEIL_L_S: ceil_l_s(operation); break;
	case OP_FLOOR_L_S: floor_l_s(operation); break;
	case OP_ROUND_W_S: round_w_s(operation); break;
	case OP_TRUNC_W_S: trunc_w_s(operation); break;
	case OP_CEIL_W_S: ceil_w_s(operation); break;
	case OP_FLOOR_W_S: floor_w_s(operation); break;
	case OP_CVT_D_S: cvt_d_s(operation); break;
	case OP_CVT_W_S: cvt_w_s(operation); break;
	case OP_CVT_L_S: cvt_l_s(operation); break;
	case OP_C_F_S: c_f_s(operation); break;
	case OP_C_UN_S: c_un_s(operation); break;
	case OP_C_EQ_S: c_eq_s(operation); break;
	case OP_C_UEQ_S: c_ueq_s(operation); break;
	case OP_C_OLT_S: c_olt_s(operation); break;
	case OP_C_ULT_S: c_ult_s(operation); break;
	case OP_C_OLE_S: c_ole_s(operation); break;
	case OP_C_ULE_S: c_ule_s(operation); break;
	case OP_C_SF_S: c_sf_s(operation); break;
	case OP_C_NGLE_S: c_ngle_s(operation); break;
	case OP_C_SEQ_S: c_seq_s(operation); break;
	case OP_C_NGL_S: c_ngl_s(operation); break;
	case OP_C_LT_S: c_lt_s(operation); break;
	case OP_C_NGE_S: c_nge_s(operation); break;
	case OP_C_LE_S: c_le_s(operation); break;
	case OP_C_NGT_S: c_ngt_s(operation); break;
	}
}

static void add_d(const Operation& operation)
{
	uint32_t ft = FT(operation);
	uint32_t fs = FS(operation);
	uint32_t fd = FD(operation);

	if(has_register(fd))
	{
		remove_register(fd);
		add_register(fs);
		add_register(ft);

		WRITE_OP(write_op_commutative(fd, fs, ft, "+"));
	}
}

static void sub_d(const Operation& operation)
{
	uint32_t ft = FT(operation);
	uint32_t fs = FS(operation);
	uint32_t fd = FD(operation);

	if(has_register(fd))
	{
		remove_register(fd);
		add_register(fs);
		add_register(ft);

		WRITE_OP(write_op(fd, fs, ft, "-"));
	}
}

static void mul_d(const Operation& operation)
{
	uint32_t ft = FT(operation);
	uint32_t fs = FS(operation);
	uint32_t fd = FD(operation);

	if(has_register(fd))
	{
		remove_register(fd);
		add_register(fs);
		add_register(ft);
		
		WRITE_OP(write_op_commutative(fd, fs, ft, "*"));
	}
}

static void div_d(const Operation& operation)
{
	uint32_t ft = FT(operation);
	uint32_t fs = FS(operation);
	uint32_t fd = FD(operation);

	if(has_register(fd))
	{
		remove_register(fd);
		add_register(fs);
		add_register(ft);
		
		WRITE_OP(write_op(fd, fs, ft, "/"));
	}
}

static void sqrt_d(const Operation& operation)
{
	uint32_t fs = FS(operation);
	uint32_t fd = FD(operation);

	if(has_register(fd))
	{
		remove_register(fd);
		add_register(fs);

		WRITE_OP(write_op_function(fd, fs, "sqrt"));
	}
}

static void abs_d(const Operation& operation)
{
	uint32_t fs = FS(operation);
	uint32_t fd = FD(operation);

	if(has_register(fd))
	{
		remove_register(fd);
		add_register(fs);
		
		WRITE_OP(write_op_function(fd, fs, "abs"));
	}
}

static void mov_d(const Operation& operation)
{
	uint32_t fs = FS(operation);
	uint32_t fd = FD(operation);

	if(has_register(fd))
	{
		remove_register(fd);
		add_register(fs);

		WRITE_OP(write_op_move(fd, fs));
	}
}

static void neg_d(const Operation& operation)
{
	uint32_t fs = FS(operation);
	uint32_t fd = FD(operation);

	if(has_register(fd))
	{
		remove_register(fd);
		add_register(fs);
		
		WRITE_OP(write_op_move_and_negate(fd, fs));
	}
}

static void round_l_d(const Operation& operation)
{
	uint32_t fs = FS(operation);
	uint32_t fd = FD(operation);

	if(has_register(fd))
	{
		remove_register(fd);
		add_register(fs);

		WRITE_OP(write_op_function(fd, fs, "round"));
	}
}

static void trunc_l_d(const Operation& operation)
{
	uint32_t fs = FS(operation);
	uint32_t fd = FD(operation);

	if(has_register(fd))
	{
		remove_register(fd);
		add_register(fs);

		WRITE_OP(write_op_function(fd, fs, "trunc"));
	}
}

static void ceil_l_d(const Operation& operation)
{
	uint32_t fs = FS(operation);
	uint32_t fd = FD(operation);

	if(has_register(fd))
	{
		remove_register(fd);
		add_register(fs);

		WRITE_OP(write_op_function(fd, fs, "ceil"));
	}
}

static void floor_l_d(const Operation& operation)
{
	uint32_t fs = FS(operation);
	uint32_t fd = FD(operation);

	if(has_register(fd))
	{
		remove_register(fd);
		add_register(fs);

		WRITE_OP(write_op_function(fd, fs, "floor"));
	}
}

static void round_w_d(const Operation& operation)
{
	uint32_t fs = FS(operation);
	uint32_t fd = FD(operation);

	if(has_register(fd))
	{
		remove_register(fd);
		add_register(fs);

		WRITE_OP(write_op_function(fd, fs, "round"));
	}
}

static void trunc_w_d(const Operation& operation)
{
	uint32_t fs = FS(operation);
	uint32_t fd = FD(operation);

	if(has_register(fd))
	{
		remove_register(fd);
		add_register(fs);

		WRITE_OP(write_op_function(fd, fs, "trunc"));
	}
}

static void ceil_w_d(const Operation& operation)
{
	uint32_t fs = FS(operation);
	uint32_t fd = FD(operation);

	if(has_register(fd))
	{
		remove_register(fd);
		add_register(fs);

		WRITE_OP(write_op_function(fd, fs, "ceil"));
	}
}

static void floor_w_d(const Operation& operation)
{
	uint32_t fs = FS(operation);
	uint32_t fd = FD(operation);

	if(has_register(fd))
	{
		remove_register(fd);
		add_register(fs);

		WRITE_OP(write_op_function(fd, fs, "floor"));
	}
}

static void cvt_s_d(const Operation& operation)
{
	uint32_t fs = FS(operation);
	uint32_t fd = FD(operation);

	if(has_register(fd))
	{
		remove_register(fd);
		add_register(fs);

		WRITE_OP(write_op_move(fd, fs));
	}
}

static void cvt_w_d(const Operation& operation)
{
	uint32_t fs = FS(operation);
	uint32_t fd = FD(operation);

	if(has_register(fd))
	{
		remove_register(fd);
		add_register(fs);

		WRITE_OP(write_op_move(fd, fs));
	}
}

static void cvt_l_d(const Operation& operation)
{
	uint32_t fs = FS(operation);
	uint32_t fd = FD(operation);

	if(has_register(fd))
	{
		remove_register(fd);
		add_register(fs);

		WRITE_OP(write_op_move(fd, fs));
	}
}

static void c_f_d(const Operation& operation)
{
	//if(has_cop1_register(cc))
	//{
	//	remove_cop1_register(cc);
	//}
}

static void c_un_d(const Operation& operation)
{
	//uint32_t fs = (instruction >> 11) & 0x1F;
	//uint32_t ft = (instruction >> 16) & 0x1F;

	//if(has_cop1_register(cc))
	//{
	//	remove_cop1_register(cc);
	//	add_cop1_register(fs);
	//	add_cop1_register(ft);
	//}
}

static void c_eq_d(const Operation& operation)
{
	//uint32_t fs = (instruction >> 11) & 0x1F;
	//uint32_t ft = (instruction >> 16) & 0x1F;

	//if(has_cop1_register(cc))
	//{
	//	remove_cop1_register(cc);
	//	add_cop1_register(fs);
	//	add_cop1_register(ft);
	//}
}

static void c_ueq_d(const Operation& operation)
{
	//uint32_t fs = (instruction >> 11) & 0x1F;
	//uint32_t ft = (instruction >> 16) & 0x1F;

	//if(has_cop1_register(cc))
	//{
	//	remove_cop1_register(cc);
	//	add_cop1_register(fs);
	//	add_cop1_register(ft);
	//}
}

static void c_olt_d(const Operation& operation)
{
	//uint32_t fs = (instruction >> 11) & 0x1F;
	//uint32_t ft = (instruction >> 16) & 0x1F;

	//if(has_cop1_register(cc))
	//{
	//	remove_cop1_register(cc);
	//	add_cop1_register(fs);
	//	add_cop1_register(ft);
	//}
}

static void c_ult_d(const Operation& operation)
{
	//uint32_t fs = (instruction >> 11) & 0x1F;
	//uint32_t ft = (instruction >> 16) & 0x1F;

	//if(has_cop1_register(cc))
	//{
	//	remove_cop1_register(cc);
	//	add_cop1_register(fs);
	//	add_cop1_register(ft);
	//}
}

static void c_ole_d(const Operation& operation)
{
	//uint32_t fs = (instruction >> 11) & 0x1F;
	//uint32_t ft = (instruction >> 16) & 0x1F;

	//if(has_cop1_register(cc))
	//{
	//	remove_cop1_register(cc);
	//	add_cop1_register(fs);
	//	add_cop1_register(ft);
	//}
}

static void c_ule_d(const Operation& operation)
{
	//uint32_t fs = (instruction >> 11) & 0x1F;
	//uint32_t ft = (instruction >> 16) & 0x1F;

	//if(has_cop1_register(cc))
	//{
	//	remove_cop1_register(cc);
	//	add_cop1_register(fs);
	//	add_cop1_register(ft);
	//}
}

static void c_sf_d(const Operation& operation)
{
	//if(has_cop1_register(cc))
	//{
	//	remove_cop1_register(cc);
	//}
}

static void c_ngle_d(const Operation& operation)
{
	//uint32_t fs = (instruction >> 11) & 0x1F;
	//uint32_t ft = (instruction >> 16) & 0x1F;

	//if(has_cop1_register(cc))
	//{
	//	remove_cop1_register(cc);
	//	add_cop1_register(fs);
	//	add_cop1_register(ft);
	//}
}

static void c_seq_d(const Operation& operation)
{
	//uint32_t fs = (instruction >> 11) & 0x1F;
	//uint32_t ft = (instruction >> 16) & 0x1F;

	//if(has_cop1_register(cc))
	//{
	//	remove_cop1_register(cc);
	//	add_cop1_register(fs);
	//	add_cop1_register(ft);
	//}
}

static void c_ngl_d(const Operation& operation)
{
	//uint32_t fs = (instruction >> 11) & 0x1F;
	//uint32_t ft = (instruction >> 16) & 0x1F;

	//if(has_cop1_register(cc))
	//{
	//	remove_cop1_register(cc);
	//	add_cop1_register(fs);
	//	add_cop1_register(ft);
	//}
}

static void c_lt_d(const Operation& operation)
{
	//uint32_t fs = (instruction >> 11) & 0x1F;
	//uint32_t ft = (instruction >> 16) & 0x1F;

	//if(has_cop1_register(cc))
	//{
	//	remove_cop1_register(cc);
	//	add_cop1_register(fs);
	//	add_cop1_register(ft);
	//}
}

static void c_nge_d(const Operation& operation)
{
	//uint32_t fs = (instruction >> 11) & 0x1F;
	//uint32_t ft = (instruction >> 16) & 0x1F;

	//if(has_cop1_register(cc))
	//{
	//	remove_cop1_register(cc);
	//	add_cop1_register(fs);
	//	add_cop1_register(ft);
	//}
}

static void c_le_d(const Operation& operation)
{
	//uint32_t fs = (instruction >> 11) & 0x1F;
	//uint32_t ft = (instruction >> 16) & 0x1F;

	//if(has_cop1_register(cc))
	//{
	//	remove_cop1_register(cc);
	//	add_cop1_register(fs);
	//	add_cop1_register(ft);
	//}
}

static void c_ngt_d(const Operation& operation)
{
	//uint32_t fs = (instruction >> 11) & 0x1F;
	//uint32_t ft = (instruction >> 16) & 0x1F;

	//if(has_cop1_register(cc))
	//{
	//	remove_cop1_register(cc);
	//	add_cop1_register(fs);
	//	add_cop1_register(ft);
	//}
}

static void cop1_d(const Operation& operation)
{
	uint32_t instr = operation.instr;
	uint32_t opcode = (instr >> 0) & 0x3F;

	switch(opcode)
	{
	case OP_ADD_D: add_d(operation); break;
	case OP_SUB_D: sub_d(operation); break;
	case OP_MUL_D: mul_d(operation); break;
	case OP_DIV_D: div_d(operation); break;
	case OP_SQRT_D: sqrt_d(operation); break;
	case OP_ABS_D: abs_d(operation); break;
	case OP_MOV_D: mov_d(operation); break;
	case OP_NEG_D: neg_d(operation); break;
	case OP_ROUND_L_D: round_l_d(operation); break;
	case OP_TRUNC_L_D: trunc_l_d(operation); break;
	case OP_CEIL_L_D: ceil_l_d(operation); break;
	case OP_FLOOR_L_D: floor_l_d(operation); break;
	case OP_ROUND_W_D: round_w_d(operation); break;
	case OP_TRUNC_W_D: trunc_w_d(operation); break;
	case OP_CEIL_W_D: ceil_w_d(operation); break;
	case OP_FLOOR_W_D: floor_w_d(operation); break;
	case OP_CVT_S_D: cvt_s_d(operation); break;
	case OP_CVT_W_D: cvt_w_d(operation); break;
	case OP_CVT_L_D: cvt_l_d(operation); break;
	case OP_C_F_D: c_f_d(operation); break;
	case OP_C_UN_D: c_un_d(operation); break;
	case OP_C_EQ_D: c_eq_d(operation); break;
	case OP_C_UEQ_D: c_ueq_d(operation); break;
	case OP_C_OLT_D: c_olt_d(operation); break;
	case OP_C_ULT_D: c_ult_d(operation); break;
	case OP_C_OLE_D: c_ole_d(operation); break;
	case OP_C_ULE_D: c_ule_d(operation); break;
	case OP_C_SF_D: c_sf_d(operation); break;
	case OP_C_NGLE_D: c_ngle_d(operation); break;
	case OP_C_SEQ_D: c_seq_d(operation); break;
	case OP_C_NGL_D: c_ngl_d(operation); break;
	case OP_C_LT_D: c_lt_d(operation); break;
	case OP_C_NGE_D: c_nge_d(operation); break;
	case OP_C_LE_D: c_le_d(operation); break;
	case OP_C_NGT_D: c_ngt_d(operation); break;
	}
}

static void cvt_s_w(const Operation& operation)
{
	uint32_t fs = FS(operation);
	uint32_t fd = FD(operation);

	if(has_register(fd))
	{
		remove_register(fd);
		add_register(fs);

		WRITE_OP(write_op_move(fd, fs));
	}
}

static void cvt_d_w(const Operation& operation)
{
	uint32_t fs = FS(operation);
	uint32_t fd = FD(operation);

	if(has_register(fd))
	{
		remove_register(fd);
		add_register(fs);

		WRITE_OP(write_op_move(fd, fs));
	}
}

static void cop1_w(const Operation& operation)
{
	uint32_t instr = operation.instr;
	uint32_t opcode = (instr >> 0) & 0x3F;

	switch(opcode)
	{
	case OP_CVT_S_W: cvt_s_w(operation); break;
	case OP_CVT_D_W: cvt_d_w(operation); break;
	}
}

static void cvt_s_l(const Operation& operation)
{
	uint32_t fs = FS(operation);
	uint32_t fd = FD(operation);

	if(has_register(fd))
	{
		remove_register(fd);
		add_register(fs);

		WRITE_OP(write_op_move(fd, fs));
	}
}

static void cvt_d_l(const Operation& operation)
{
	uint32_t fs = FS(operation);
	uint32_t fd = FD(operation);

	if(has_register(fd))
	{
		remove_register(fd);
		add_register(fs);

		WRITE_OP(write_op_move(fd, fs));
	}
}

static void cop1_l(const Operation& operation)
{
	uint32_t instr = operation.instr;
	uint32_t opcode = (instr >> 0) & 0x3F;

	switch(opcode)
	{
	case OP_CVT_S_L: cvt_s_l(operation); break;
	case OP_CVT_D_L: cvt_d_l(operation); break;
	}
}

static void mfc1(const Operation& operation)
{
	uint32_t rt = RT(operation);
	uint32_t fs = FS(operation);
	
	if(rt == 0)
		return;

	if(has_register(rt))
	{
		remove_register(rt);
		add_register(fs);

		WRITE_OP(write_op_move(rt, fs));
	}
}

static void dmfc1(const Operation& operation)
{
	uint32_t rt = RT(operation);
	uint32_t fs = FS(operation);

	if(rt == 0)
		return;

	if(has_register(rt))
	{
		remove_register(rt);
		add_register(fs);

		WRITE_OP(write_op_move(rt, fs));
	}
}

static void mtc1(const Operation& operation)
{
	uint32_t rt = RT(operation);
	uint32_t fs = FS(operation);

	if(rt == 0)
		return;

	if(has_register(fs))
	{
		remove_register(fs);
		add_register(rt);
		
		WRITE_OP(write_op_move(fs, rt));
	}
}

static void dmtc1(const Operation& operation)
{
	uint32_t rt = RT(operation);
	uint32_t fs = FS(operation);

	if(rt == 0)
		return;

	if(has_register(fs))
	{
		remove_register(fs);
		add_register(rt);
		
		WRITE_OP(write_op_move(fs, rt));
	}
}

static void cop1(const Operation& operation)
{
	uint32_t instr = operation.instr;
	uint32_t opcode = (instr >> 21) & 0x1F;

	switch(opcode)
	{
	case OP_MFC1: mfc1(operation); break;
	case OP_DMFC1: dmfc1(operation); break;
	//case OP_CFC1: cfc1(operation); break; //Control register shit...
	case OP_MTC1: mtc1(operation); break;
	case OP_DMTC1: dmtc1(operation); break;
	//case OP_CTC1: ctc1(operation); break; //Control register shit...
	case OP_BC1: bc1(operation); break;
	case OP_COP1_S: cop1_s(operation); break;
	case OP_COP1_D: cop1_d(operation); break;
	case OP_COP1_W: cop1_w(operation); break;
	case OP_COP1_L: cop1_l(operation); break;
	}
}

static void j(const Operation& operation)
{
	// Do nothing
}

static void jal(const Operation& operation)
{
	// Do nothing
}

// Should registers involved in branches be tracked?
// We don't know the outcome of the branch... and we don't know if the branch is even involved in the final value?
static void beq(const Operation& operation)
{
	/*uint32_t rs = RS(operation);
	uint32_t rt = RT(operation);
	
	add_register(rs);
	add_register(rt);*/
}

static void bne(const Operation& operation)
{
	/*uint32_t rs = RS(operation);
	uint32_t rt = RT(operation);
	
	add_register(rs);
	add_register(rt);*/
}

static void blez(const Operation& operation)
{
	/*uint32_t rs = RS(operation);

	add_register(rs);*/
}

static void bgtz(const Operation& operation)
{
	/*uint32_t rs = RS(operation);

	add_register(rs);*/
}

static void addi(const Operation& operation)
{
	uint32_t rs = RS(operation);
	uint32_t rt = RT(operation);
	int16_t immediate = (int16_t)IMMEDIATE(operation);

	if(has_register(rt))
	{
		remove_register(rt);
		add_register(rs);

		if(immediate < 0)
		{
			WRITE_OP(write_op_immediate_unsigned(rt, rs, std::abs(immediate), "-"));
		}
		else
		{
			WRITE_OP(write_op_immediate_unsigned(rt, rs, immediate, "+"));
		}
	}
}

static void addiu(const Operation& operation)
{
	uint32_t rs = RS(operation);
	uint32_t rt = RT(operation);
	int16_t immediate = (int16_t)IMMEDIATE(operation);

	if(has_register(rt))
	{
		remove_register(rt);
		add_register(rs);
		
		if(immediate < 0)
		{
			WRITE_OP(write_op_immediate_unsigned(rt, rs, std::abs(immediate), "-"));
		}
		else
		{
			WRITE_OP(write_op_immediate_unsigned(rt, rs, immediate, "+"));
		}
	}
}

static void slti(const Operation& operation)
{
	uint32_t rs = RS(operation);
	uint32_t rt = RT(operation);
	uint32_t immediate = IMMEDIATE(operation);

	if(has_register(rt))
	{
		remove_register(rt);
		add_register(rs);
		
		WRITE_OP(write_op_immediate_signed(rt, rs, immediate, "<"));
	}
}

static void sltiu(const Operation& operation)
{
	uint32_t rs = RS(operation);
	uint32_t rt = RT(operation);
	uint64_t immediate = (uint64_t)(int64_t)(int16_t)IMMEDIATE(operation);

	if(has_register(rt))
	{
		remove_register(rt);
		add_register(rs);
		
		WRITE_OP(write_op_immediate_unsigned(rt, rs, immediate, "<"));
	}
}

static void andi(const Operation& operation)
{
	uint32_t rs = RS(operation);
	uint32_t rt = RT(operation);
	uint32_t immediate = IMMEDIATE(operation);

	if(has_register(rt))
	{
		remove_register(rt);
		add_register(rs);
		
		WRITE_OP(write_op_immediate_hex(rt, rs, immediate, "&"));
	}
}

static void ori(const Operation& operation)
{
	uint32_t rs = RS(operation);
	uint32_t rt = RT(operation);
	uint32_t immediate = IMMEDIATE(operation);

	if(has_register(rt))
	{
		remove_register(rt);
		add_register(rs);
		
		WRITE_OP(write_op_immediate_hex(rt, rs, immediate, "|"));
	}
}

static void xori(const Operation& operation)
{
	uint32_t rs = RS(operation);
	uint32_t rt = RT(operation);
	uint32_t immediate = IMMEDIATE(operation);

	if(has_register(rt))
	{
		remove_register(rt);
		add_register(rs);
		
		WRITE_OP(write_op_immediate_hex(rt, rs, immediate, "^"));
	}
}

static void lui(const Operation& operation)
{
	uint32_t rt = RT(operation);
	uint32_t immediate = IMMEDIATE(operation);

	if(has_register(rt))
	{
		remove_register(rt);
		
		WRITE_OP(write_op_move_immediate(rt, (immediate << 16)));
	}
}

static void beql(const Operation& operation)
{
	/*uint32_t rs = RS(operation);
	uint32_t rt = RT(operation);
	
	add_register(rs);
	add_register(rt);*/
}

static void bnel(const Operation& operation)
{
	/*uint32_t rs = RS(operation);
	uint32_t rt = RT(operation);

	add_register(rs);
	add_register(rt);*/
}

static void blezl(const Operation& operation)
{
	/*uint32_t rs = RS(operation);

	add_register(rs);*/
}

static void bgtzl(const Operation& operation)
{
	/*uint32_t rs = RS(operation);

	add_register(rs);*/
}

static void daddi(const Operation& operation)
{
	uint32_t rs = RS(operation);
	uint32_t rt = RT(operation);
	int16_t immediate = (int16_t)IMMEDIATE(operation);
	
	if(rt == 0)
		return;

	if(has_register(rt))
	{
		remove_register(rt);
		add_register(rs);
		
		if(immediate < 0)
		{
			WRITE_OP(write_op_immediate_unsigned(rt, rs, std::abs(immediate), "-"));
		}
		else
		{
			WRITE_OP(write_op_immediate_unsigned(rt, rs, immediate, "+"));
		}
	}
}

static void daddiu(const Operation& operation)
{
	uint32_t rs = RS(operation);
	uint32_t rt = RT(operation);
	int16_t immediate = (int16_t)IMMEDIATE(operation);
	
	if(rt == 0)
		return;

	if(has_register(rt))
	{
		remove_register(rt);
		add_register(rs);
		
		if(immediate < 0)
		{
			WRITE_OP(write_op_immediate_unsigned(rt, rs, std::abs(immediate), "-"));
		}
		else
		{
			WRITE_OP(write_op_immediate_unsigned(rt, rs, immediate, "+"));
		}
	}
}

static void ldl(const Operation& operation)	
{
	uint32_t rt = RT(operation);
	uint32_t num_bytes = get_num_bytes(operation);

	if(rt == 0)
		return;

	if(has_register(rt))
	{
		remove_register(rt);

		for(uint32_t i = 0; i < num_bytes; i++)
			add_address(operation.address + i);
		
		WRITE_OP(write_op_move_from_address(rt, operation.address));
	}
}

static void ldr(const Operation& operation)
{
	uint32_t rt = RT(operation);
	uint32_t num_bytes = get_num_bytes(operation);

	if(rt == 0)
		return;

	if(has_register(rt))
	{
		remove_register(rt);

		for(uint32_t i = 0; i < num_bytes; i++)
			add_address(operation.address - i);
		
		WRITE_OP(write_op_move_from_address(rt, operation.address));
	}
}

static void lb(const Operation& operation)
{
	uint32_t rt = RT(operation);
	
	if(rt == 0)
		return;

	if(has_register(rt))
	{
		remove_register(rt);
		add_address(operation.address);
		
		WRITE_OP(write_op_move_from_address(rt, operation.address));
	}
}

static void lh(const Operation& operation)
{
	uint32_t rt = RT(operation);
	
	if(rt == 0)
		return;

	if(has_register(rt))
	{
		remove_register(rt);

		add_address(operation.address);
		add_address(operation.address + 0x01);
		
		WRITE_OP(write_op_move_from_address(rt, operation.address));
	}
}

static void lwl(const Operation& operation)
{
	uint32_t rt = RT(operation);
	uint32_t num_bytes = get_num_bytes(operation);

	if(rt == 0)
		return;

	if(has_register(rt))
	{
		remove_register(rt);
		
		for(uint32_t i = 0; i < num_bytes; i++)
			add_address(operation.address + i);
		
		WRITE_OP(write_op_move_from_address(rt, operation.address));
	}
}

static void lw(const Operation& operation)
{	
	uint32_t rt = RT(operation);
	
	if(rt == 0)
		return;

	if(has_register(rt))
	{
		add_address(operation.address);
		add_address(operation.address + 0x01);
		add_address(operation.address + 0x02);
		add_address(operation.address + 0x03);
		remove_register(rt);
		
		WRITE_OP(write_op_move_from_address(rt, operation.address));
	}
}

static void lbu(const Operation& operation)
{
	uint32_t rt = RT(operation);
	
	if(rt == 0)
		return;

	if(has_register(rt))
	{
		add_address(operation.address);
		remove_register(rt);
		
		WRITE_OP(write_op_move_from_address(rt, operation.address));
	}
}

static void lhu(const Operation& operation)
{
	uint32_t rt = RT(operation);
	
	if(rt == 0)
		return;

	if(has_register(rt))
	{
		add_address(operation.address);
		add_address(operation.address + 0x01);
		remove_register(rt);
		
		WRITE_OP(write_op_move_from_address(rt, operation.address));
	}
}

static void lwr(const Operation& operation)
{
	uint32_t rt = RT(operation);
	uint32_t num_bytes = get_num_bytes(operation);

	if(rt == 0)
		return;

	if(has_register(rt))
	{
		remove_register(rt);

		for(uint32_t i = 0; i < num_bytes; i++)
			add_address(operation.address - i);
		
		WRITE_OP(write_op_move_from_address(rt, operation.address));
	}
}

static void lwu(const Operation& operation)
{
	uint32_t rt = RT(operation);
	
	if(rt == 0)
		return;

	if(has_register(rt))
	{
		add_address(operation.address);
		add_address(operation.address + 0x01);
		add_address(operation.address + 0x02);
		add_address(operation.address + 0x03);
		remove_register(rt);
		
		WRITE_OP(write_op_move_from_address(rt, operation.address));
	}
}

static void sb(const Operation& operation)
{
	uint32_t rt = RT(operation);
	
	if(rt == 0)
		return;

	if(has_address(operation.address))
	{
		remove_address(operation.address);
		add_register(rt);
		
		WRITE_OP(write_op_move_to_address(rt, operation.address));
	}
}

static void sh(const Operation& operation)
{
	uint32_t rt = RT(operation);

	if(rt == 0)
		return;

	if(has_address(operation.address) || has_address(operation.address + 0x01))
	{
		if(has_address(operation.address))
			remove_address(operation.address);

		if(has_address(operation.address + 0x01))
			remove_address(operation.address + 0x01);

		add_register(rt);
		
		WRITE_OP(write_op_move_to_address(rt, operation.address));
	}
}

static void swl(const Operation& operation)
{
	uint32_t rt = RT(operation);
	uint32_t num_bytes = get_num_bytes(operation);

	if(rt == 0)
		return;
	
	bool trace = false;
	for(uint32_t i = 0; i < num_bytes; i++)
	{
		if(has_address(operation.address + i))
		{
			remove_address(operation.address + i);
			trace = true;
		}
	}

	if(trace)
	{
		add_register(rt);
		
		WRITE_OP(write_op_move_to_address(rt, operation.address));
	}
}

static void sw(const Operation& operation)
{
	uint32_t rt = RT(operation);
	
	if(rt == 0)
		return;

	if(has_address(operation.address) || 
		has_address(operation.address + 0x01) || 
		has_address(operation.address + 0x02) || 
		has_address(operation.address + 0x03))
	{
		if(has_address(operation.address))
			remove_address(operation.address);
		
		if(has_address(operation.address + 0x01))
			remove_address(operation.address + 0x01);
		
		if(has_address(operation.address + 0x02))
			remove_address(operation.address + 0x02);
		
		if(has_address(operation.address + 0x03))
			remove_address(operation.address + 0x03);

		add_register(rt);
		
		WRITE_OP(write_op_move_to_address(rt, operation.address));
	}
}

static void sdl(const Operation& operation)
{
	uint32_t rt = RT(operation);
	uint32_t num_bytes = get_num_bytes(operation);

	if(rt == 0)
		return;
	
	bool trace = false;
	for(uint32_t i = 0; i < num_bytes; i++)
	{
		if(has_address(operation.address + i))
		{
			remove_address(operation.address + i);
			trace = true;
		}
	}

	if(trace)
	{
		add_register(rt);
		
		WRITE_OP(write_op_move_to_address(rt, operation.address));
	}
}

static void sdr(const Operation& operation)
{
	uint32_t rt = RT(operation);
	uint32_t num_bytes = get_num_bytes(operation);

	if(rt == 0)
		return;
	
	bool trace = false;
	for(uint32_t i = 0; i < num_bytes; i++)
	{
		if(has_address(operation.address - i))
		{
			remove_address(operation.address - i);
			trace = true;
		}
	}

	if(trace)
	{
		add_register(rt);	
		
		WRITE_OP(write_op_move_to_address(rt, operation.address));
	}
}

static void swr(const Operation& operation)
{
	uint32_t rt = RT(operation);
	uint32_t num_bytes = get_num_bytes(operation);

	if(rt == 0)
		return;
	
	bool trace = false;
	for(uint32_t i = 0; i < num_bytes; i++)
	{
		if(has_address(operation.address - i))
		{
			remove_address(operation.address - i);
			trace = true;
		}
	}

	if(trace)
	{
		add_register(rt);
		
		WRITE_OP(write_op_move_to_address(rt, operation.address));
	}
}

static void cache(const Operation& operation)
{
}

static void lwc1(const Operation& operation)
{	
	uint32_t rt = COP1(RT(operation));

	if(has_register(rt))
	{		
		remove_register(rt);
		
		add_address(operation.address);
		add_address(operation.address + 0x01);
		add_address(operation.address + 0x02);
		add_address(operation.address + 0x03);

		WRITE_OP(write_op_move_from_address(rt, operation.address));
	}
}

static void ldc1(const Operation& operation)
{
	uint32_t rt = COP1(RT(operation));

	if(has_register(rt))
	{
		remove_register(rt);

		add_address(operation.address);
		add_address(operation.address + 0x01);
		add_address(operation.address + 0x02);
		add_address(operation.address + 0x03);
		add_address(operation.address + 0x04);
		add_address(operation.address + 0x05);
		add_address(operation.address + 0x06);
		add_address(operation.address + 0x07);
		
		WRITE_OP(write_op_move_from_address(rt, operation.address));
	}
}

static void ld(const Operation& operation)
{
	uint32_t rt = RT(operation);

	if(has_register(rt))
	{
		remove_register(rt);

		add_address(operation.address);
		add_address(operation.address + 0x01);
		add_address(operation.address + 0x02);
		add_address(operation.address + 0x03);
		add_address(operation.address + 0x04);
		add_address(operation.address + 0x05);
		add_address(operation.address + 0x06);
		add_address(operation.address + 0x07);
		
		WRITE_OP(write_op_move_from_address(rt, operation.address));
	}
}

static void swc1(const Operation& operation)
{
	uint32_t rt = COP1(RT(operation));

	if(has_address(operation.address) || 
		has_address(operation.address + 0x01) || 
		has_address(operation.address + 0x02) || 
		has_address(operation.address + 0x03))
	{
		if(has_address(operation.address))
			remove_address(operation.address);
		
		if(has_address(operation.address + 0x01))
			remove_address(operation.address + 0x01);
		
		if(has_address(operation.address + 0x02))
			remove_address(operation.address + 0x02);
		
		if(has_address(operation.address + 0x03))
			remove_address(operation.address + 0x03);

		add_register(rt);
		
		WRITE_OP(write_op_move_to_address(rt, operation.address));
	}
}

static void sdc1(const Operation& operation)
{
	uint32_t rt = COP1(RT(operation));

	if(has_address(operation.address) || 
		has_address(operation.address + 0x01) || 
		has_address(operation.address + 0x02) || 
		has_address(operation.address + 0x03) || 
		has_address(operation.address + 0x04) || 
		has_address(operation.address + 0x05) || 
		has_address(operation.address + 0x06) || 
		has_address(operation.address + 0x07))
	{
		if(has_address(operation.address))
			remove_address(operation.address);
		
		if(has_address(operation.address + 0x01))
			remove_address(operation.address + 0x01);
		
		if(has_address(operation.address + 0x02))
			remove_address(operation.address + 0x02);
		
		if(has_address(operation.address + 0x03))
			remove_address(operation.address + 0x03);
		
		if(has_address(operation.address + 0x04))
			remove_address(operation.address + 0x04);
		
		if(has_address(operation.address + 0x05))
			remove_address(operation.address + 0x05);
		
		if(has_address(operation.address + 0x06))
			remove_address(operation.address + 0x06);
		
		if(has_address(operation.address + 0x07))
			remove_address(operation.address + 0x07);

		add_register(rt);
		
		WRITE_OP(write_op_move_to_address(rt, operation.address));
	}
}

static void sd(const Operation& operation)
{
	uint32_t rt = RT(operation);

	if(has_address(operation.address) || 
		has_address(operation.address + 0x01) || 
		has_address(operation.address + 0x02) || 
		has_address(operation.address + 0x03) || 
		has_address(operation.address + 0x04) || 
		has_address(operation.address + 0x05) || 
		has_address(operation.address + 0x06) || 
		has_address(operation.address + 0x07))
	{
		if(has_address(operation.address))
			remove_address(operation.address);
		
		if(has_address(operation.address + 0x01))
			remove_address(operation.address + 0x01);
		
		if(has_address(operation.address + 0x02))
			remove_address(operation.address + 0x02);
		
		if(has_address(operation.address + 0x03))
			remove_address(operation.address + 0x03);
		
		if(has_address(operation.address + 0x04))
			remove_address(operation.address + 0x04);
		
		if(has_address(operation.address + 0x05))
			remove_address(operation.address + 0x05);
		
		if(has_address(operation.address + 0x06))
			remove_address(operation.address + 0x06);
		
		if(has_address(operation.address + 0x07))
			remove_address(operation.address + 0x07);

		add_register(rt);
		
		WRITE_OP(write_op_move_to_address(rt, operation.address));
	}
}

static void check_ignore_list()
{
	for(const auto& ignoreItem : l_ignoreList)
	{
		for(uint32_t i = 0; i < ignoreItem.size; i++)
		{
			const uint32_t address = ignoreItem.address + i;

			if(l_addresses.find(address) != l_addresses.cend())
				l_addresses.erase(address);
		}
	}
}

static void analyze()
{
	unsigned int current_operation = 0;
	unsigned int prev_ops_found = 0;
	
	start_timer();

	for(auto it = l_tempOperations.rbegin(); it != l_tempOperations.rend(); it++)
	{
		check_ignore_list();

		uint32_t opcode = OPCODE((*it));

		switch(opcode)
		{
		case OP_SPECIAL: special(*it); break;
		case OP_REGIMM: regimm(*it); break;
		case OP_COP0: cop0(*it); break;
		case OP_COP1: cop1(*it); break;
		case OP_J: j(*it); break;
		case OP_JAL: jal(*it); break;
		case OP_BEQ: beq(*it); break;
		case OP_BNE: bne(*it); break;
		case OP_BLEZ: blez(*it); break;
		case OP_BGTZ: bgtz(*it); break;
		case OP_ADDI: addi(*it); break;
		case OP_ADDIU: addiu(*it); break;
		case OP_SLTI: slti(*it); break;
		case OP_SLTIU: sltiu(*it); break;
		case OP_ANDI: andi(*it); break;
		case OP_ORI: ori(*it); break;
		case OP_XORI: xori(*it); break;
		case OP_LUI: lui(*it); break;
		case OP_BEQL: beql(*it); break;
		case OP_BNEL: bnel(*it); break;
		case OP_BLEZL: blezl(*it); break;
		case OP_BGTZL: bgtzl(*it); break;
		case OP_DADDI: daddi(*it); break;
		case OP_DADDIU: daddiu(*it); break;
		case OP_LDL: ldl(*it); break;
		case OP_LDR: ldr(*it); break;
		case OP_LB: lb(*it); break;
		case OP_LH: lh(*it); break;
		case OP_LWL: lwl(*it); break;
		case OP_LW: lw(*it); break;
		case OP_LBU: lbu(*it); break;
		case OP_LHU: lhu(*it); break;
		case OP_LWR: lwr(*it); break;
		case OP_LWU: lwu(*it); break;
		case OP_SB: sb(*it); break;
		case OP_SH: sh(*it); break;
		case OP_SWL: swl(*it); break;
		case OP_SW: sw(*it); break;
		case OP_SDL: sdl(*it); break;
		case OP_SDR: sdr(*it); break;
		case OP_SWR: swr(*it); break;
		case OP_CACHE: cache(*it); break;
		//case OP_LL: ll(*it); break; //Kinda complicated
		case OP_LWC1: lwc1(*it); break;
		//case OP_LLD: lld(*it); break; //Not implemented
		case OP_LDC1: ldc1(*it); break;
		case OP_LD: ld(*it); break;
		//case OP_SC: sc(*it); break;  //Kinda complicated
		case OP_SWC1: swc1(*it); break;
		//case OP_SCD: scd(*it); break; //Not implemented
		case OP_SDC1: sdc1(*it); break;
		case OP_SD: sd(*it); break;
		}

#ifdef CREATE_VERIFICATION_FILE
		write_verification(*it);
#endif
		current_operation++;

		// Did we find an operation?
		const unsigned int curr_ops_found = l_tempFoundOperations.size();

		if(prev_ops_found < curr_ops_found)
		{
			prev_ops_found = curr_ops_found;
			l_tempIndices.insert(l_tempOperations.size() - current_operation);
		}

		if(check_timer(30))
		{
			update_progress_bar(current_operation, l_tempOperations.size());
			update_status_message(current_operation, l_tempOperations.size());
		}

		if(check_cancel_clicked())
		{
			set_analyzer_state(AnalyzerState::CANCELLED);
			return;
		}
	}

	return;
}

static void set_progress_bar_range(uint32_t first, uint32_t last)
{
	SetWindowLong(l_progressBar, GWL_STYLE, WS_CHILD | WS_VISIBLE | PBS_SMOOTH);
	SendMessage(l_progressBar, PBM_SETRANGE32, first, last);
}

static bool wait_for_state(AnalyzerState state)
{
	bool stop = false;
	bool reachedState = false;

	while(!stop)
	{
		l_analyzerStateMutex.lock();

		if(l_analyserState == state)
		{
			reachedState = true;
			stop = true;
		}
		else if(check_cancel_clicked())
		{
			l_analyserState = AnalyzerState::CANCELLED;
			stop = true;
		}

		l_analyzerStateMutex.unlock();

		Sleep(30);
	}

	return reachedState;
}

static void send_finished_message()
{
	SendMessage(l_window, WM_FINISHED_ANALYSIS, NULL, NULL);
}

static void send_abort_message()
{
	SendMessage(l_window, WM_ABORT, NULL, NULL);
}

static bool set_write_breakpoint(uint32_t address, uint32_t size)
{
	m64p_breakpoint bkp;
	bkp.address = address;
	bkp.endaddr = address + size - 1;
	bkp.flags = M64P_BKP_FLAG_WRITE | M64P_BKP_FLAG_ENABLED;

	if(DebugBreakpointCommand(M64P_BKP_CMD_ADD_STRUCT, 0, &bkp) == -1)
	{
		DebugMessage(M64MSG_ERROR, "failed to add write breakpoint at address 0x%x", bkp.address);

		return false;
	}

	return true;
}

static void remove_write_breakpoint()
{
	DebugBreakpointCommand(M64P_BKP_CMD_REMOVE_IDX, 0, NULL);
}

static bool set_execute_breakpoint(uint32_t address)
{
	if(DebugBreakpointCommand(M64P_BKP_CMD_ADD_ADDR, address, nullptr) == -1)
	{
		DebugMessage(M64MSG_ERROR, "failed to add execute breakpoint at address 0x%x", address);
		
		return false;
	}

	return true;
}

static void remove_execute_breakpoint(uint32_t address)
{
	DebugBreakpointCommand(M64P_BKP_CMD_REMOVE_ADDR, address, nullptr);
}

static void run_and_continue()
{
	DebugSetRunState(M64P_DBG_RUNSTATE_RUNNING);
	DebugContinue();
}

static void create_watch_list()
{
	l_watchListMutex.lock();
	
	l_operations = l_tempOperations;
	l_results = l_tempResults;
	l_foundIndices = l_tempIndices;
	l_foundOperations.assign(l_tempFoundOperations.crbegin(), l_tempFoundOperations.crend());

	l_watchList.clear();
	for(const auto& operation : l_foundOperations)
	{
		if(	!is_valid_address(operation.address) ||
			is_temporary_address(operation.address))
			continue;

		l_watchList.insert(Watch(operation.address, get_num_bytes(operation)));
	}

	l_watchListMutex.unlock();
}

static void abort_analysis()
{
	// std::vector thread-safe for concurrent read operations (i think)
	set_analyzer_state(l_foundOperations.empty() ? AnalyzerState::IDLE : AnalyzerState::DEBUGGING);

	l_analyzerResultMutex.lock();
	l_analyzerResult = (l_analyzerResult == AnalyzerResult::NONE) ? AnalyzerResult::USER_CANCELLED : l_analyzerResult;
	l_analyzerResultMutex.unlock();

	send_abort_message();
}

static void finish_analysis()
{
	create_watch_list();
	set_analyzer_state(AnalyzerState::DEBUGGING);
	set_analyzer_result(AnalyzerResult::SUCCESS);	
	send_finished_message();
}

static void start_analyzing()
{
	reset();

	set_analyzer_state(AnalyzerState::WAITING_FOR_VI_CALLBACK);
	set_status_message("Waiting for vertical interrupt...");

	bool reachedState = wait_for_state(AnalyzerState::ANALYZING);

	run_and_continue();

	if(!reachedState)
	{
		abort_analysis();
		return;
	}	
	
	set_progress_bar_range(0, l_tempOperations.size());
	update_status_message(0, l_tempOperations.size());

#ifdef CREATE_RESULT_FILE
	l_resultFile.open("result.txt", std::ios_base::trunc);

	if(!l_resultFile.is_open())
	{
		return;
	}
#endif

#ifdef CREATE_VERIFICATION_FILE
	l_verificationFile.open("verification.txt", std::ios_base::trunc);

	if(!l_verificationFile.is_open())
	{
		return;
	}
#endif

	for(uint32_t i = 0; i < l_size; i++)
		add_address(l_address + i);

	analyze();

	if(get_analyzer_state() == AnalyzerState::CANCELLED)
		abort_analysis();
	else
		finish_analysis();
	
#ifdef CREATE_RESULT_FILE
	l_resultFile.close();
#endif

#ifdef CREATE_VERIFICATION_FILE
	l_verificationFile.close();
#endif
}

static void analyzer_thread_start()
{
	while(true)
	{
		switch(get_analyzer_state())
		{
		case AnalyzerState::STARTED: start_analyzing();	break;
		case AnalyzerState::TERMINATED: return;
		}

		Sleep(30);
	}
}

/*********************************************************************************************************
 *	Analyzer window
 */
#define MAX_CHARACTERS 10

#define DEFAULT_BUTTON_SIZE_X 100
#define DEFAULT_BUTTON_SIZE_Y 25
#define DEFAULT_TAB_SIZE_Y 20
#define DEFAULT_COMBO_SIZE_X 125
#define DEFAULT_COMBO_SIZE_Y 15
#define DEFAULT_RADIO_SIZE_X 150
#define DEFAULT_RADIO_SIZE_Y 25

#define WINDOW_SIZE_X 1000
#define WINDOW_SIZE_Y 600
#define WINDOW_POSITION_X(screen_size_x) (screen_size_x - (screen_size_x / 3) - (WINDOW_SIZE_X / 2))
#define WINDOW_POSITION_Y(screen_size_y) ((screen_size_y / 2) - (WINDOW_SIZE_Y / 2))
#define WINDOW_STYLE WS_OVERLAPPEDWINDOW

#define EDIT_POSITION_X 20
#define EDIT_POSITION_Y 20
#define EDIT_SIZE_X 80
#define EDIT_SIZE_Y 25

#define DROPLIST_SIZE_POSITION_X 110
#define DROPLIST_SIZE_POSITION_Y 20
#define DROPLIST_SIZE_SIZE_X DEFAULT_COMBO_SIZE_X
#define DROPLIST_SIZE_SIZE_Y DEFAULT_COMBO_SIZE_Y

#define TABCONTROL_POSITION_X 20
#define TABCONTROL_POSITION_Y 60
#define TABCONTROL_DELTA_X 10
#define TABCONTROL_DELTA_Y 60
#define TABCONTROL_SIZE_X(window_size_x) (int)((window_size_x - TABCONTROL_POSITION_X - TABCONTROL_DELTA_X) * 0.75)
#define TABCONTROL_SIZE_Y(window_size_y) (window_size_y - TABCONTROL_DELTA_Y - TABCONTROL_POSITION_Y)

#define LISTVIEW_POSITION_X TABCONTROL_POSITION_X
#define LISTVIEW_POSITION_Y TABCONTROL_POSITION_Y + DEFAULT_TAB_SIZE_Y
#define LISTVIEW_SIZE_X(window_size_x) TABCONTROL_SIZE_X(window_size_x)
#define LISTVIEW_SIZE_Y(window_size_y) TABCONTROL_SIZE_Y(window_size_y) - DEFAULT_TAB_SIZE_Y

#define WATCHLIST_DELTA_X 20
#define WATCHLIST_DELTA_Y TABCONTROL_DELTA_Y
#define WATCHLIST_POSITION_X(window_size_x) (TABCONTROL_POSITION_X + TABCONTROL_SIZE_X(window_size_x) + TABCONTROL_DELTA_X)
#define WATCHLIST_POSITION_Y TABCONTROL_POSITION_Y + DEFAULT_TAB_SIZE_Y
#define WATCHLIST_SIZE_X(window_size_x) (window_size_x - WATCHLIST_DELTA_X - WATCHLIST_POSITION_X(window_size_x))
#define WATCHLIST_SIZE_Y(window_size_y) TABCONTROL_SIZE_Y(window_size_y) - DEFAULT_TAB_SIZE_Y

#define DROPLIST_FORMAT_POSITION_X(window_size_x) WATCHLIST_POSITION_X(window_size_x)
#define DROPLIST_FORMAT_POSITION_Y DROPLIST_SIZE_POSITION_Y
#define DROPLIST_FORMAT_SIZE_X DEFAULT_COMBO_SIZE_X
#define DROPLIST_FORMAT_SIZE_Y DEFAULT_COMBO_SIZE_Y

#define ANALYZEBUTTON_SIZE_X DEFAULT_BUTTON_SIZE_X
#define ANALYZEBUTTON_SIZE_Y DEFAULT_BUTTON_SIZE_Y
#define ANALYZEBUTTON_DELTA_Y 30
#define ANALYZEBUTTON_POSITION_X(window_size_x) ((window_size_x / 2) - (ANALYZEBUTTON_SIZE_X / 2))
#define ANALYZEBUTTON_POSITION_Y(window_size_y) (window_size_y - (ANALYZEBUTTON_SIZE_Y / 2) - ANALYZEBUTTON_DELTA_Y)

#define REFRESHBUTTON_SIZE_X 25
#define REFRESHBUTTON_SIZE_Y 25
#define REFRESHBUTTON_POSITION_X(window_size_x) TABCONTROL_POSITION_X + TABCONTROL_SIZE_X(window_size_x) - REFRESHBUTTON_SIZE_X
#define REFRESHBUTTON_POSITION_Y(window_size_y) 19

#define TOGGLEBUTTON_DELTA_X 10
#define TOGGLEBUTTON_SIZE_X DEFAULT_BUTTON_SIZE_X
#define TOGGLEBUTTON_SIZE_Y DEFAULT_BUTTON_SIZE_Y
#define TOGGLEBUTTON_POSITION_X 20
#define TOGGLEBUTTON_POSITION_Y(window_size_y) ANALYZEBUTTON_POSITION_Y(window_size_y)

#define STEPBUTTON_DELTA_X 10
#define STEPBUTTON_SIZE_X DEFAULT_BUTTON_SIZE_X
#define STEPBUTTON_SIZE_Y DEFAULT_BUTTON_SIZE_Y
#define STEPBUTTON_POSITION_X TOGGLEBUTTON_POSITION_X + TOGGLEBUTTON_SIZE_X + TOGGLEBUTTON_DELTA_X
#define STEPBUTTON_POSITION_Y(window_size_y) ANALYZEBUTTON_POSITION_Y(window_size_y)

#define CONTINUEBUTTON_SIZE_X DEFAULT_BUTTON_SIZE_X
#define CONTINUEBUTTON_SIZE_Y DEFAULT_BUTTON_SIZE_Y
#define CONTINUEBUTTON_POSITION_X STEPBUTTON_POSITION_X + STEPBUTTON_SIZE_X + STEPBUTTON_DELTA_X
#define CONTINUEBUTTON_POSITION_Y(window_size_y) ANALYZEBUTTON_POSITION_Y(window_size_y)

#define SEARCHWINDOWBUTTON_SIZE_X 25
#define SEARCHWINDOWBUTTON_SIZE_Y 25
#define SEARCHWINDOWBUTTON_POSITION_X(window_size_x) TABCONTROL_POSITION_X + TABCONTROL_SIZE_X(window_size_x) - SEARCHWINDOWBUTTON_SIZE_X
#define SEARCHWINDOWBUTTON_POSITION_Y 19

#define IGNOREWINDOWBUTTON_SIZE_X 25
#define IGNOREWINDOWBUTTON_SIZE_Y 25
#define IGNOREWINDOWBUTTON_POSITION_X(window_size_x) SEARCHWINDOWBUTTON_POSITION_X(window_size_x) - IGNOREWINDOWBUTTON_SIZE_X - 5
#define IGNOREWINDOWBUTTON_POSITION_Y SEARCHWINDOWBUTTON_POSITION_Y

#define ANALYZER_STATUS_DELTA_X WATCHLIST_DELTA_X
#define ANALYZER_STATUS_DELTA_Y 10
#define ANALYZER_STATUS_SIZE_X 400
#define ANALYZER_STATUS_SIZE_Y 25
#define ANALYZER_STATUS_POSITION_X(window_size_x) window_size_x - ANALYZER_STATUS_SIZE_X - ANALYZER_STATUS_DELTA_X
#define ANALYZER_STATUS_POSITION_Y(window_size_y) window_size_y - ANALYZER_STATUS_SIZE_Y - ANALYZER_STATUS_DELTA_Y

#define STATUS_POSITION_X 10
#define STATUS_POSITION_Y 10
#define STATUS_SIZE_X PROGRESS_WINDOW_SIZE_X
#define STATUS_SIZE_Y 25

#define PROGRESS_DELTA_X 10
#define PROGRESS_DELTA_Y 10
#define PROGRESS_SIZE_X(window_size_x) (window_size_x - (PROGRESS_DELTA_X * 2))
#define PROGRESS_SIZE_Y 30
#define PROGRESS_POSITION_X PROGRESS_DELTA_X
#define PROGRESS_POSITION_Y (STATUS_POSITION_Y + STATUS_SIZE_Y)

#define CANCEL_SIZE_X 100
#define CANCEL_SIZE_Y 25
#define CANCEL_DELTA_Y 10
#define CANCEL_POSITION_X(window_size_x) ((window_size_x / 2) - (CANCEL_SIZE_X / 2))
#define CANCEL_POSITION_Y (PROGRESS_POSITION_Y + PROGRESS_SIZE_Y + PROGRESS_DELTA_Y)

#define PROGRESS_WINDOW_STYLE WS_CAPTION
#define PROGRESS_WINDOW_SIZE_X 225
#define PROGRESS_WINDOW_SIZE_Y (CANCEL_POSITION_Y + CANCEL_SIZE_Y + CANCEL_DELTA_Y)
#define PROGRESS_WINDOW_POSITION_X(parent_position_x, parent_size_x) (parent_position_x + (parent_size_x / 2) - (PROGRESS_WINDOW_SIZE_X / 2))
#define PROGRESS_WINDOW_POSITION_Y(parent_position_y, parent_size_y) (parent_position_y + (parent_size_y / 2) - (PROGRESS_WINDOW_SIZE_Y / 2))

#define SEARCH_WINDOW_STYLE WS_OVERLAPPEDWINDOW
#define SEARCH_WINDOW_SIZE_X 600
#define SEARCH_WINDOW_SIZE_Y 500
#define SEARCH_WINDOW_POSITION_X(parent_position_x, parent_size_x) (parent_position_x + (parent_size_x / 2) - (SEARCH_WINDOW_SIZE_X / 2))
#define SEARCH_WINDOW_POSITION_Y(parent_position_y, parent_size_y) (parent_position_y + (parent_size_y / 2) - (SEARCH_WINDOW_SIZE_Y / 2))

#define SEARCH_STATUS_TEXT_SIZE_X 400
#define SEARCH_STATUS_TEXT_SIZE_Y 15
#define SEARCH_STATUS_TEXT_POSITION_X 10
#define SEARCH_STATUS_TEXT_POSITION_Y(window_size_y) (window_size_y - SEARCH_STATUS_TEXT_SIZE_Y - 10)

#define SEARCH_EQUALTO_RADIO_POSITION_X(window_size_x) SEARCH_SIZE_COMBO_POSITION_X(window_size_x)
#define SEARCH_EQUALTO_RADIO_POSITION_Y SEARCH_SPECIFICVALUE_EDIT_POSITION_Y + SEARCH_SPECIFICVALUE_EDIT_SIZE_Y + 20
#define SEARCH_EQUALTO_RADIO_SIZE_X DEFAULT_RADIO_SIZE_X
#define SEARCH_EQUALTO_RADIO_SIZE_Y DEFAULT_RADIO_SIZE_Y

#define SEARCH_NOTEQUALTO_RADIO_POSITION_X(window_size_x) SEARCH_EQUALTO_RADIO_POSITION_X(window_size_x)
#define SEARCH_NOTEQUALTO_RADIO_POSITION_Y SEARCH_EQUALTO_RADIO_POSITION_Y + DEFAULT_RADIO_SIZE_Y
#define SEARCH_NOTEQUALTO_RADIO_SIZE_X DEFAULT_RADIO_SIZE_X
#define SEARCH_NOTEQUALTO_RADIO_SIZE_Y DEFAULT_RADIO_SIZE_Y

#define SEARCH_LESSTHAN_RADIO_POSITION_X(window_size_x) SEARCH_EQUALTO_RADIO_POSITION_X(window_size_x)
#define SEARCH_LESSTHAN_RADIO_POSITION_Y SEARCH_NOTEQUALTO_RADIO_POSITION_Y + DEFAULT_RADIO_SIZE_Y
#define SEARCH_LESSTHAN_RADIO_SIZE_X DEFAULT_RADIO_SIZE_X
#define SEARCH_LESSTHAN_RADIO_SIZE_Y DEFAULT_RADIO_SIZE_Y

#define SEARCH_GREATERTHAN_RADIO_POSITION_X(window_size_x) SEARCH_EQUALTO_RADIO_POSITION_X(window_size_x)
#define SEARCH_GREATERTHAN_RADIO_POSITION_Y SEARCH_LESSTHAN_RADIO_POSITION_Y + DEFAULT_RADIO_SIZE_Y
#define SEARCH_GREATERTHAN_RADIO_SIZE_X DEFAULT_RADIO_SIZE_X
#define SEARCH_GREATERTHAN_RADIO_SIZE_Y DEFAULT_RADIO_SIZE_Y

#define SEARCH_LESSTHANOREQUALTO_RADIO_POSITION_X(window_size_x) SEARCH_EQUALTO_RADIO_POSITION_X(window_size_x)
#define SEARCH_LESSTHANOREQUALTO_RADIO_POSITION_Y SEARCH_GREATERTHAN_RADIO_POSITION_Y + DEFAULT_RADIO_SIZE_Y
#define SEARCH_LESSTHANOREQUALTO_RADIO_SIZE_X DEFAULT_RADIO_SIZE_X
#define SEARCH_LESSTHANOREQUALTO_RADIO_SIZE_Y DEFAULT_RADIO_SIZE_Y

#define SEARCH_GREATERTHANOREQUALTO_RADIO_POSITION_X(window_size_x) SEARCH_EQUALTO_RADIO_POSITION_X(window_size_x)
#define SEARCH_GREATERTHANOREQUALTO_RADIO_POSITION_Y SEARCH_LESSTHANOREQUALTO_RADIO_POSITION_Y + DEFAULT_RADIO_SIZE_Y
#define SEARCH_GREATERTHANOREQUALTO_RADIO_SIZE_X DEFAULT_RADIO_SIZE_X
#define SEARCH_GREATERTHANOREQUALTO_RADIO_SIZE_Y DEFAULT_RADIO_SIZE_Y

#define SEARCH_RANGE_RADIO_POSITION_X(window_size_x) SEARCH_EQUALTO_RADIO_POSITION_X(window_size_x)
#define SEARCH_RANGE_RADIO_POSITION_Y SEARCH_GREATERTHANOREQUALTO_RADIO_POSITION_Y + DEFAULT_RADIO_SIZE_Y
#define SEARCH_RANGE_RADIO_SIZE_X DEFAULT_RADIO_SIZE_X
#define SEARCH_RANGE_RADIO_SIZE_Y DEFAULT_RADIO_SIZE_Y

#define SEARCH_RANGE_START_EDIT_POSITION_X(window_size_x) SEARCH_EQUALTO_RADIO_POSITION_X(window_size_x) + 10
#define SEARCH_RANGE_START_EDIT_POSITION_Y SEARCH_RANGE_RADIO_POSITION_Y + SEARCH_RANGE_RADIO_SIZE_Y
#define SEARCH_RANGE_START_EDIT_SIZE_X 100
#define SEARCH_RANGE_START_EDIT_SIZE_Y 25
#define SEARCH_RANGE_START_EDIT_DELTA_X 10

#define SEARCH_RANGE_END_EDIT_POSITION_X(window_size_x) SEARCH_RANGE_START_EDIT_POSITION_X(window_size_x) + SEARCH_RANGE_START_EDIT_SIZE_X + SEARCH_RANGE_START_EDIT_DELTA_X
#define SEARCH_RANGE_END_EDIT_POSITION_Y SEARCH_RANGE_START_EDIT_POSITION_Y
#define SEARCH_RANGE_END_EDIT_SIZE_X 100
#define SEARCH_RANGE_END_EDIT_SIZE_Y 25

#define SEARCH_PREVIOUSVALUE_RADIO_POSITION_X(window_size_x) SEARCH_EQUALTO_RADIO_POSITION_X(window_size_x)
#define SEARCH_PREVIOUSVALUE_RADIO_POSITION_Y 20
#define SEARCH_PREVIOUSVALUE_RADIO_SIZE_X DEFAULT_RADIO_SIZE_X
#define SEARCH_PREVIOUSVALUE_RADIO_SIZE_Y DEFAULT_RADIO_SIZE_Y

#define SEARCH_SPECIFICVALUE_RADIO_POSITION_X(window_size_x) SEARCH_EQUALTO_RADIO_POSITION_X(window_size_x)
#define SEARCH_SPECIFICVALUE_RADIO_POSITION_Y SEARCH_PREVIOUSVALUE_RADIO_POSITION_Y + DEFAULT_RADIO_SIZE_Y
#define SEARCH_SPECIFICVALUE_RADIO_SIZE_X DEFAULT_RADIO_SIZE_X
#define SEARCH_SPECIFICVALUE_RADIO_SIZE_Y DEFAULT_RADIO_SIZE_Y

#define SEARCH_SPECIFICVALUE_EDIT_POSITION_X(window_size_x) SEARCH_SPECIFICVALUE_RADIO_POSITION_X(window_size_x) + 10
#define SEARCH_SPECIFICVALUE_EDIT_POSITION_Y SEARCH_SPECIFICVALUE_RADIO_POSITION_Y + SEARCH_SPECIFICVALUE_RADIO_SIZE_Y
#define SEARCH_SPECIFICVALUE_EDIT_SIZE_X 100
#define SEARCH_SPECIFICVALUE_EDIT_SIZE_Y 25

#define SEARCH_SIZE_COMBO_DELTA_X 10
#define SEARCH_SIZE_COMBO_SIZE_X 75
#define SEARCH_SIZE_COMBO_SIZE_Y DEFAULT_COMBO_SIZE_Y
#define SEARCH_SIZE_COMBO_POSITION_X(window_size_x) (window_size_x - 250)
#define SEARCH_SIZE_COMBO_POSITION_Y SEARCH_RANGE_START_EDIT_POSITION_Y + SEARCH_RANGE_START_EDIT_SIZE_Y + 20

#define SEARCH_FORMAT_COMBO_POSITION_X(window_size_x) SEARCH_SIZE_COMBO_POSITION_X(window_size_x) + SEARCH_SIZE_COMBO_SIZE_X + SEARCH_SIZE_COMBO_DELTA_X
#define SEARCH_FORMAT_COMBO_POSITION_Y SEARCH_SIZE_COMBO_POSITION_Y
#define SEARCH_FORMAT_COMBO_SIZE_X 100
#define SEARCH_FORMAT_COMBO_SIZE_Y DEFAULT_COMBO_SIZE_Y

#define SEARCH_SEARCHBUTTON_POSITION_X(window_size_x) SEARCH_EQUALTO_RADIO_POSITION_X(window_size_x)
#define SEARCH_SEARCHBUTTON_POSITION_Y SEARCH_SIZE_COMBO_POSITION_Y + SEARCH_FORMAT_COMBO_SIZE_Y + 20
#define SEARCH_SEARCHBUTTON_SIZE_X DEFAULT_BUTTON_SIZE_X
#define SEARCH_SEARCHBUTTON_SIZE_Y DEFAULT_BUTTON_SIZE_Y

#define SEARCH_RESETBUTTON_POSITION_X(window_size_x) SEARCH_SEARCHBUTTON_POSITION_X(window_size_x) + SEARCH_SEARCHBUTTON_SIZE_X + 10
#define SEARCH_RESETBUTTON_POSITION_Y SEARCH_SEARCHBUTTON_POSITION_Y
#define SEARCH_RESETBUTTON_SIZE_X DEFAULT_BUTTON_SIZE_X
#define SEARCH_RESETBUTTON_SIZE_Y DEFAULT_BUTTON_SIZE_Y 
 
#define SEARCH_LISTVIEW_DELTA_X 10
#define SEARCH_LISTVIEW_DELTA_Y 10
#define SEARCH_LISTVIEW_POSITION_X 10
#define SEARCH_LISTVIEW_POSITION_Y 10
#define SEARCH_LISTVIEW_SIZE_X(window_size_x) (SEARCH_EQUALTO_RADIO_POSITION_X(window_size_x) - SEARCH_LISTVIEW_DELTA_X - SEARCH_LISTVIEW_POSITION_X)
#define SEARCH_LISTVIEW_SIZE_Y(window_size_y) (SEARCH_STATUS_TEXT_POSITION_Y(window_size_y) - SEARCH_LISTVIEW_DELTA_Y - SEARCH_LISTVIEW_POSITION_Y)

#define IGNORE_WINDOW_STYLE WS_OVERLAPPEDWINDOW
#define IGNORE_WINDOW_SIZE_X 300
#define IGNORE_WINDOW_SIZE_Y 500
#define IGNORE_WINDOW_POSITION_X(parent_position_x, parent_size_x) (parent_position_x + (parent_size_x / 2) - (IGNORE_WINDOW_SIZE_X / 2))
#define IGNORE_WINDOW_POSITION_Y(parent_position_y, parent_size_y) (parent_position_y + (parent_size_y / 2) - (IGNORE_WINDOW_SIZE_Y / 2))

#define IGNORE_ADDRESS_EDIT_POSITION_X 10
#define IGNORE_ADDRESS_EDIT_POSITION_Y 10
#define IGNORE_ADDRESS_EDIT_SIZE_X 80
#define IGNORE_ADDRESS_EDIT_SIZE_Y 25

#define IGNORE_SIZE_COMBO_POSITION_X IGNORE_ADDRESS_EDIT_POSITION_X + IGNORE_ADDRESS_EDIT_SIZE_X + 10
#define IGNORE_SIZE_COMBO_POSITION_Y IGNORE_ADDRESS_EDIT_POSITION_Y
#define IGNORE_SIZE_COMBO_SIZE_X 80
#define IGNORE_SIZE_COMBO_SIZE_Y 25

#define IGNORE_ADD_BUTTON_POSITION_X IGNORE_SIZE_COMBO_POSITION_X + IGNORE_SIZE_COMBO_SIZE_X + 10
#define IGNORE_ADD_BUTTON_POSITION_Y IGNORE_ADDRESS_EDIT_POSITION_Y - 1
#define IGNORE_ADD_BUTTON_SIZE_X DEFAULT_BUTTON_SIZE_X
#define IGNORE_ADD_BUTTON_SIZE_Y DEFAULT_BUTTON_SIZE_Y

#define IGNORE_LISTVIEW_POSITION_X 10
#define IGNORE_LISTVIEW_POSITION_Y IGNORE_ADDRESS_EDIT_POSITION_Y + IGNORE_ADDRESS_EDIT_SIZE_Y + 10
#define IGNORE_LISTVIEW_SIZE_X(window_size_x) (window_size_x - IGNORE_LISTVIEW_POSITION_X - 10)
#define IGNORE_LISTVIEW_SIZE_Y(window_size_y) (window_size_y - IGNORE_LISTVIEW_POSITION_Y - 10)

static const std::string analyzer_window_class = "analyzer_window_class";
static const std::string search_window_class = "search_window_class";
static const std::string ignore_window_class = "ignore_window_class";
static const std::string progress_window_class = "progress_window_class";

static std::string get_value_string(const uint64_t value, const uint32_t num_bytes, const uint32_t format, const uint32_t hex_width = 0)
{
	if(num_bytes == 0)
		return "N/A";

	std::stringstream stream;

	switch(format)
	{
	case 0: 
	{
		uint64_t mask = ((num_bytes >= 1) ? 0x00000000000000FF : 0) |
						((num_bytes >= 2) ? 0x000000000000FF00 : 0) |
						((num_bytes >= 3) ? 0x0000000000FF0000 : 0) |
						((num_bytes >= 4) ? 0x00000000FF000000 : 0) |
						((num_bytes >= 5) ? 0x000000FF00000000 : 0) |
						((num_bytes >= 6) ? 0x0000FF0000000000 : 0) |
						((num_bytes >= 7) ? 0x00FF000000000000 : 0) |
						((num_bytes >= 8) ? 0xFF00000000000000 : 0);

		stream << std::uppercase << std::hex << "0x";
			
		if(hex_width > 0)
			stream << std::setfill('0') << std::setw(hex_width);
			
		stream << (value & mask) << std::dec;

		break;
	}
	case 1:
		stream << value;
		break;
	case 2:
		if(num_bytes >= 8)
			stream << (int64_t)value;
		else if(num_bytes >= 4)
			stream << (int32_t)value;
		else if(num_bytes >= 2)
			stream << (int16_t)value;
		else
			stream << static_cast<int16_t>((int8_t)value); //Avoid treating numerical value as char
		break;
	case 3:
		if(num_bytes >= 8)
			stream << *((double*)&value);
		else 
			stream << *((float*)&value);
		break;
	}
	

	return stream.str();
}

static unsigned int display_context_menu(HWND window, uint16_t x, uint16_t y, std::vector<std::string> items)
{
	HMENU context_menu = CreatePopupMenu();

	for(uint32_t i = 0; i < items.size(); i++)
	{
		AppendMenu(context_menu, MF_STRING | MF_ENABLED, i + 1, items[i].c_str());
	}
			
	BOOL result = TrackPopupMenu(context_menu, TPM_TOPALIGN | TPM_LEFTALIGN | TPM_RETURNCMD, x, y, 0, window, NULL);
			
	DestroyMenu(context_menu);

	return result;
}

static void add_to_ignore_list(const IgnoreItem& ignoreItem)
{	
	if(std::binary_search(l_ignoreList.begin(), l_ignoreList.end(), ignoreItem, [](const IgnoreItem& item1, const IgnoreItem& item2){ return item1.address < item2.address; }))
		return;

	l_ignoreList.push_back(ignoreItem);
		
	std::sort(l_ignoreList.begin(), l_ignoreList.end(), [](const IgnoreItem& item1, const IgnoreItem& item2){ return item1.address < item2.address; });

	if(l_ignoreWnd != NULL)
		ListView_SetItemCountEx(l_ignoreListView, l_ignoreList.size(), NULL);
}

static void remove_from_ignore_list(const IgnoreItem& ignoreItem)
{
	auto it = std::find(l_ignoreList.cbegin(), l_ignoreList.cend(), ignoreItem);

	if(it == l_ignoreList.cend())
		return;

	l_ignoreList.erase(it);
	
	if(l_ignoreWnd != NULL)
		ListView_SetItemCountEx(l_ignoreListView, l_ignoreList.size(), NULL);
}

static void add_traced_list_view_items()
{
	std::array<CHAR, 100> temp;

	ListView_DeleteAllItems(l_tracedListView);

	for(uint32_t i = 0; i < l_listItems.size(); i++)
	{
		const ListItem& listItem = l_listItems[l_listItems.size() - i - 1];

		strcpy(temp.data(), listItem.pc.c_str());

		LVITEM item;
		item.pszText = temp.data();
		item.mask = LVIF_TEXT | LVIF_DI_SETITEM;
		item.iItem = i;
		item.iSubItem = 0;
		ListView_InsertItem(l_tracedListView, &item);
		
		strcpy(temp.data(), listItem.func.c_str());

		item.iSubItem = 1;
		item.pszText = temp.data();
		ListView_SetItem(l_tracedListView, &item);

		strcpy(temp.data(), listItem.decoded.c_str());
			
		item.iSubItem = 2;
		item.pszText = temp.data();
		ListView_SetItem(l_tracedListView, &item);
		
		item.iSubItem = 3;
		item.pszText = "N/A";
		ListView_SetItem(l_tracedListView, &item);
		
		item.iSubItem = 4;
		item.pszText = "N/A";
		ListView_SetItem(l_tracedListView, &item);
	}
}

static void update_traced_list_view_items_realtime()
{
	l_watchListMutex.lock();
	{
		if(!l_watchList.empty())
		{
			std::array<CHAR, 100> temp;	

			for(uint32_t i = 0; i < l_foundOperations.size(); i++)
			{
				const Operation& operation = l_foundOperations[i];

				if( !is_valid_address(operation.address) ||
					is_temporary_address(operation.address))
					continue;

				const auto it = l_watchList.find(Watch(operation.address, 0));

				assert(it != l_watchList.end());

				std::string str = get_value_string(it->value, it->size, l_format);

				strcpy(temp.data(), str.c_str());
		
				ListView_SetItemText(l_tracedListView, i, 4, temp.data());
			}
		}
	}
	l_watchListMutex.unlock();
}

static void on_update_results()
{
	l_watchListMutex.lock();
	{
		if(!l_results.empty())
		{
			std::array<CHAR, 100> temp;

			unsigned int i = 0;

			for(auto index : l_foundIndices)
			{
				const Result& result = l_results[index];

				std::string str = get_value_string(result.value, result.size, l_format);

				strcpy(temp.data(), str.c_str());
		
				ListView_SetItemText(l_tracedListView, i, 3, temp.data());

				i++;
			}
		}		
	}
	l_watchListMutex.unlock();
}

static void add_watch_list_items()
{
	std::array<CHAR, 100> temp;

	uint32_t i = 0;
	
	ListView_DeleteAllItems(l_watchListView);

	for(const auto& watch : l_watchList)
	{
		const std::string address_str = write_hex(watch.address, 8);

		strcpy(temp.data(), address_str.c_str());

		LVITEM item;
		item.pszText = temp.data();
		item.mask = LVIF_TEXT | LVIF_DI_SETITEM;
		item.iItem = i;
		item.iSubItem = 0;
		ListView_InsertItem(l_watchListView, &item);
		
		const std::string str = get_value_string(watch.value, watch.size, l_format);
		
		strcpy(temp.data(), str.c_str());

		item.iSubItem = 1;
		item.pszText = temp.data();
		ListView_SetItem(l_watchListView, &item);

		i++;
	}
}

static void update_watch_list_items()
{
	l_watchListMutex.lock();
	{
		if(!l_watchList.empty())
		{
			std::array<CHAR, 100> temp;
	
			uint32_t i = 0;

			for(const auto& watch : l_watchList)
			{
				const std::string str = get_value_string(watch.value, watch.size, l_format, watch.size * 2);

				strcpy(temp.data(), str.c_str());
		
				ListView_SetItemText(l_watchListView, i, 1, temp.data());

				i++;
			}
		}
	}
	l_watchListMutex.unlock();
}

static void add_logged_list_view_items()
{	
	ListView_SetItemCountEx(l_loggedListView, l_operations.size(), NULL);
}
//	std::array<CHAR, 100> temp;
//
//	ListView_DeleteAllItems(l_loggedListView);
//	
//	auto prev_it = l_foundIndices.cbegin();
//	auto curr_it = prev_it;
//	auto next_it = prev_it;
//
//	using IteratorType = Indices::const_iterator;
//	using IteratorRange = std::pair<IteratorType, IteratorType>;
//	using Ranges = std::vector<IteratorRange>;
//
//	Ranges ranges;
//
//	while(++next_it != l_foundIndices.cend())
//	{
//		if((*next_it) - (*curr_it) >= 100)
//		{
//			ranges.push_back(std::make_pair(prev_it, curr_it));
//
//			prev_it = next_it;
//		}
//
//		curr_it++;
//	}
//	ranges.push_back(std::make_pair(prev_it, curr_it));
//	
//	unsigned int curr_index = 0;
//
//	for(const auto& range : ranges)
//	{
//		for(unsigned int i = (*range.first); i < (*range.second); i++)
//		{
//			const auto& operation = l_operations[i];
//
//			std::string pc = write_hex(operation.pc);
//			std::string op = decode_op(operation);
//
//			strcpy(temp.data(), pc.c_str());
//
//			LVITEM item;
//			item.pszText = temp.data();
//			item.mask = LVIF_TEXT | LVIF_DI_SETITEM;
//			item.iItem = curr_index;
//			item.iSubItem = 0;
//			ListView_InsertItem(l_loggedListView, &item);
//		
//			strcpy(temp.data(), op.c_str());
//
//			item.iSubItem = 1;
//			item.pszText = temp.data();
//			ListView_SetItem(l_loggedListView, &item);
//
//			curr_index++;
//		}
//
//		curr_index++;
//	}
//}

static void on_update_status_text()
{
	std::stringstream ss;

	switch(l_analyzerResult)
	{
		case AnalyzerResult::USER_CANCELLED:
		{
			ss << "Analysis was aborted by the user.";
			break;
		}

		case AnalyzerResult::ADDRESS_WAS_NEVER_WRITTEN:
		{
			ss << "The specified address range was not written to during this frame.";
			break;
		}

		case AnalyzerResult::SUCCESS:
		{			
			ss << "Traced " << l_foundOperations.size() << " out of " << l_operations.size() << " operations.";
			break;
		}
	}		

	SetWindowText(l_statusText, ss.str().c_str());
}

static void on_size(uint32_t cx, uint32_t cy)
{
	SetWindowPos(l_analyzeButton, NULL, ANALYZEBUTTON_POSITION_X(cx), ANALYZEBUTTON_POSITION_Y(cy), NULL, NULL, SWP_NOSIZE | SWP_NOZORDER);
	SetWindowPos(l_toggleButton, NULL, TOGGLEBUTTON_POSITION_X, TOGGLEBUTTON_POSITION_Y(cy), NULL, NULL, SWP_NOSIZE | SWP_NOZORDER);
	SetWindowPos(l_stepButton, NULL, STEPBUTTON_POSITION_X, STEPBUTTON_POSITION_Y(cy), NULL, NULL, SWP_NOSIZE | SWP_NOZORDER);
	SetWindowPos(l_continueButton, NULL, CONTINUEBUTTON_POSITION_X, CONTINUEBUTTON_POSITION_Y(cy), NULL, NULL, SWP_NOSIZE | SWP_NOZORDER);
	SetWindowPos(l_searchWndButton, NULL, SEARCHWINDOWBUTTON_POSITION_X(cx), SEARCHWINDOWBUTTON_POSITION_Y, NULL, NULL, SWP_NOSIZE | SWP_NOZORDER);
	SetWindowPos(l_ignoreWndButton, NULL, IGNOREWINDOWBUTTON_POSITION_X(cx), IGNOREWINDOWBUTTON_POSITION_Y, NULL, NULL, SWP_NOSIZE | SWP_NOZORDER);
	SetWindowPos(l_tabControl, NULL, NULL, NULL, TABCONTROL_SIZE_X(cx), TABCONTROL_SIZE_Y(cy), SWP_NOMOVE | SWP_NOZORDER);
	SetWindowPos(l_tracedListView, NULL, NULL, NULL, LISTVIEW_SIZE_X(cx), LISTVIEW_SIZE_Y(cy), SWP_NOMOVE | SWP_NOZORDER);
	SetWindowPos(l_loggedListView, NULL, NULL, NULL, LISTVIEW_SIZE_X(cx), LISTVIEW_SIZE_Y(cy), SWP_NOMOVE | SWP_NOZORDER);
	SetWindowPos(l_watchListView, NULL, WATCHLIST_POSITION_X(cx), WATCHLIST_POSITION_Y, WATCHLIST_SIZE_X(cx), WATCHLIST_SIZE_Y(cy), SWP_NOZORDER);
	SetWindowPos(l_formatCombo, NULL, DROPLIST_FORMAT_POSITION_X(cx), DROPLIST_FORMAT_POSITION_Y, NULL, NULL, SWP_NOSIZE | SWP_NOZORDER);
	SetWindowPos(l_statusText, NULL, ANALYZER_STATUS_POSITION_X(cx), ANALYZER_STATUS_POSITION_Y(cy), NULL, NULL, SWP_NOSIZE | SWP_NOZORDER);
}

static void pad_with_zeroes(char* text)
{
	size_t length = strlen(text);

	for(size_t i = length; i < MAX_CHARACTERS; i++)
		text[i] = '0';

	text[MAX_CHARACTERS] = '\0';
					
	SendMessage(l_edit,	WM_SETTEXT,	sizeof(text), (LPARAM)text);
}

static void create_progress_window(const std::string&);

static void remove_breakpoints()
{
	for(unsigned int i = 0; i < l_foundOperations.size(); i++)
	{
		const Operation& operation = l_foundOperations[i];

		remove_execute_breakpoint(operation.pc);

		ListView_SetItemState(l_tracedListView, i, INDEXTOSTATEIMAGEMASK(0), LVIS_STATEIMAGEMASK);
	}

	l_isStepping = false;
	l_expected_pc = 0;
	l_currentIndex = -1;
	l_breakpoints.clear();
}

static void on_analyze_clicked()
{	
	//Make sure we don't hit any breakpoints while resetting variables.
	set_analyzer_state(AnalyzerState::IDLE); //Potential deadlock?

	char text[MAX_CHARACTERS + 1];

	SendMessage(l_edit,	WM_GETTEXT,	sizeof(text), (LPARAM)text);

	pad_with_zeroes(text);

	l_address = strtoul(text, NULL, 16);

	uint32_t index = SendMessage(l_sizeCombo, CB_GETCURSEL, NULL, NULL);

	l_size = 1 << index;
	l_canToggle = false;
	
	EnableWindow(l_toggleButton, FALSE);
	EnableWindow(l_continueButton, FALSE);
	EnableWindow(l_stepButton, FALSE);
	EnableWindow(l_window, FALSE);
	EnableWindow(l_searchWnd, FALSE);
	EnableWindow(l_ignoreWnd, FALSE);

	remove_breakpoints();
	run_and_continue(); //In case we were already in a breakpoint when we pressed Analyze.

	create_progress_window("Analysis in progress...");

	set_analyzer_state(AnalyzerState::STARTED);
}

static unsigned int pc_to_index(unsigned int pc)
{
	auto it = l_foundOperations.cbegin();
	for(; it != l_foundOperations.cend(); it++)
	{
		if(it->pc == pc)
			break;
	}

	assert(it != l_foundOperations.cend());

	return std::distance(l_foundOperations.cbegin(), it);
}

static unsigned int index_to_pc(unsigned int index)
{
	return l_foundOperations[index].pc;
}

static bool is_current_index(int index)
{
	return (index == l_currentIndex);
}

static bool has_breakpoint(unsigned int pc)
{
	return (l_breakpoints.find(pc) != l_breakpoints.cend());
}

static void on_toggle_clicked()
{
	int selectedIndex = ListView_GetNextItem(l_tracedListView, -1, LVNI_SELECTED);

	if(selectedIndex == -1)
		return;

	unsigned int pc = index_to_pc(selectedIndex);

	if(has_breakpoint(pc))
	{
		ListView_SetItemState(l_tracedListView, selectedIndex, is_current_index(selectedIndex) ? INDEXTOSTATEIMAGEMASK(2) : INDEXTOSTATEIMAGEMASK(0), LVIS_STATEIMAGEMASK);
		remove_execute_breakpoint(pc);
		l_breakpoints.erase(pc);
	}
	else
	{
		ListView_SetItemState(l_tracedListView, selectedIndex, is_current_index(selectedIndex) ? INDEXTOSTATEIMAGEMASK(3) : INDEXTOSTATEIMAGEMASK(1), LVIS_STATEIMAGEMASK);
		set_execute_breakpoint(pc);
		l_breakpoints.insert(pc);
	}
}

static void on_continue_clicked()
{
	unsigned int pc = index_to_pc(l_currentIndex);
	ListView_SetItemState(l_tracedListView, l_currentIndex, has_breakpoint(pc) ? INDEXTOSTATEIMAGEMASK(1) : INDEXTOSTATEIMAGEMASK(0), LVIS_STATEIMAGEMASK);
	
	EnableWindow(l_stepButton, FALSE);
	EnableWindow(l_continueButton, FALSE);

	l_currentIndex = -1;

	run_and_continue();
}

static void on_step_clicked()
{
	unsigned int nextIndex = l_currentIndex + 1;

	if(nextIndex < l_foundOperations.size())
	{
		l_isStepping = true;
		l_expected_pc = index_to_pc(nextIndex);

		if(!has_breakpoint(l_expected_pc))
		{
			set_execute_breakpoint(l_expected_pc);
		}
	}

	on_continue_clicked();
}

static void on_edit_address()
{
	char text[MAX_CHARACTERS + 1];

	SendMessage(l_edit,	WM_GETTEXT,	sizeof(text), (LPARAM)text);

	size_t length = max(strlen(text), 3);

	const char* prefix = "0x8";

	for(int i = 0; i < 3; i++)
	{
		text[i] = prefix[i];
	}

	if(length <= 3)
		text[3] = '\0';

	SendMessage(l_edit, WM_SETTEXT, NULL, (LPARAM)text);
	SendMessage(l_edit, EM_SETSEL, length, length);
}

static void on_abort()
{
	on_update_status_text();
	
	EnableWindow(l_window, TRUE);
	EnableWindow(l_searchWnd, TRUE);
	EnableWindow(l_ignoreWnd, TRUE);
	DestroyWindow(l_progressWnd);
	l_canToggle = true;

}

static void on_finished_analysis()
{
	add_traced_list_view_items();
	add_logged_list_view_items();
	add_watch_list_items();

	on_update_results();
	on_update_status_text();
	
	EnableWindow(l_window, TRUE);
	EnableWindow(l_searchWnd, TRUE);
	EnableWindow(l_ignoreWnd, TRUE);
	DestroyWindow(l_progressWnd);
	l_canToggle = true;
}

static void on_update_realtime()
{
	update_traced_list_view_items_realtime();
	update_watch_list_items();
}

static void on_breakpoint_hit(unsigned int pc)
{
	unsigned int index = pc_to_index(pc);

	if(l_isStepping && (pc != l_expected_pc))
	{
		remove_execute_breakpoint(l_expected_pc);
		l_isStepping = false;
	}

	if(l_isStepping && !has_breakpoint(pc))
	{
		ListView_SetItemState(l_tracedListView, index, INDEXTOSTATEIMAGEMASK(2), LVIS_STATEIMAGEMASK);
		remove_execute_breakpoint(pc);
	}
	else
	{
		ListView_SetItemState(l_tracedListView, index, INDEXTOSTATEIMAGEMASK(3), LVIS_STATEIMAGEMASK);
	}

	ListView_EnsureVisible(l_tracedListView, index, FALSE);

	update_watch_values();
	on_update_realtime();

	EnableWindow(l_stepButton, TRUE);
	EnableWindow(l_continueButton, TRUE);
	
	l_currentIndex = index;
	l_expected_pc = 0;
	l_isStepping = false;
}

static void on_format_changed()
{
	l_format = SendMessage(l_formatCombo, CB_GETCURSEL, NULL, NULL);

	on_update_realtime();
	on_update_results();

	InvalidateRect(l_loggedListView, NULL, FALSE);
}

static LRESULT on_logged_list_view_custom_draw(LPNMLVCUSTOMDRAW lvCustomDraw)
{
	switch(lvCustomDraw->nmcd.dwDrawStage) 
    {
		case CDDS_PREPAINT:
            return CDRF_NOTIFYITEMDRAW; // request notifications for individual listview items
            
        case CDDS_ITEMPREPAINT:
			const unsigned int index = lvCustomDraw->nmcd.dwItemSpec;

			bool is_found_index = false;

			l_watchListMutex.lock();
			is_found_index = l_foundIndices.find(index) != l_foundIndices.cend();
			l_watchListMutex.unlock();

			if(is_found_index)
			{
                lvCustomDraw->clrText = RGB(255, 0, 0);
                return CDRF_NEWFONT;
			}

            break;
	}

	return CDRF_DODEFAULT;
}

static void on_get_disp_info(NMLVDISPINFO* lvDispInfo)
{	
	LVITEM& item = lvDispInfo->item;

	const unsigned int index = item.iItem;
	const unsigned int subIndex = item.iSubItem;

	if((item.mask & LVIF_TEXT) != LVIF_TEXT)
		return;
	
	l_watchListMutex.lock();
	const Operation& operation = l_operations[index];
	const Result& result = l_results[index];
	l_watchListMutex.unlock();

	std::string str;

	switch(subIndex)
	{
	case 0:
		str = write_hex(operation.pc);
		break;
	case 1:
		str = decode_op(operation);
		break;
	case 2:
		str = get_value_string(result.value, result.size, l_format);
		break;
	}

	if((unsigned int)item.cchTextMax <= str.size())
		return;

	std::strcpy(item.pszText, str.data());
}

static void on_watch_context_menu(LPARAM lp)
{	
	const uint16_t x = LOWORD(lp);
	const uint16_t y = HIWORD(lp);

	LVHITTESTINFO hitTestInfo;

	hitTestInfo.pt.x = x;
	hitTestInfo.pt.y = y;

	ScreenToClient(l_watchListView, &hitTestInfo.pt);
	
	const int item = ListView_HitTest(l_watchListView, &hitTestInfo);

	if(item == -1)
		return;
	
	const uint32_t result = display_context_menu(l_window, x, y, {"Trace", "Watch", "Ignore"});
	
	if(result == 0)
		return;

	Watch watch;

	l_watchListMutex.lock();
	{
		auto it = l_watchList.cbegin();

		std::advance(it, item);

		watch = *it;
	}
	l_watchListMutex.unlock();

	switch(result)
	{
		case 1:
		{
			std::string str = write_hex(watch.address, 8);

			SetWindowText(l_edit, str.c_str());

			break;
		}

		case 2:
		{
			break;
		}

		case 3:
		{
			add_to_ignore_list(IgnoreItem(watch.address, watch.size, "N/A"));

			break;
		}
	}
}

static void create_search_window();
static void create_ignore_window();

static long __stdcall winproc( HWND hwnd, unsigned int msg, WPARAM wp, LPARAM lp )
{
    switch(msg)
    {
        case WM_DESTROY:
		{
            PostQuitMessage(0);
			return 0L;
		}		
		case WM_ABORT:
		{
			on_abort();
			return 0L;
		}
		case WM_FINISHED_ANALYSIS:
		{
			on_finished_analysis();
			return 0L;
		}
		case WM_UPDATE_REALTIME:
		{
			on_update_realtime();
			return 0L;
		}
		case WM_BREAKPOINT_HIT:
		{
			on_breakpoint_hit(lp);
			return 0L;
		}
		case WM_GETMINMAXINFO:
		{
			RECT client;
			client.left = 0;
			client.top = 0;
			client.right = WINDOW_SIZE_X;
			client.bottom = WINDOW_SIZE_Y;

			AdjustWindowRect(&client, WINDOW_STYLE, FALSE);

			MINMAXINFO* minMaxInfo = (MINMAXINFO*)lp;
			minMaxInfo->ptMinTrackSize.x = (client.right - client.left);
			minMaxInfo->ptMinTrackSize.y = (client.bottom - client.top);

			return 0L;
		}
		case WM_SIZE:
		{
			on_size(LOWORD(lp), HIWORD(lp)); 
			break;
		}
        case WM_COMMAND:
		{
			switch(HIWORD(wp))
			{
			case BN_CLICKED:
				if((HWND)lp == l_analyzeButton) on_analyze_clicked();
				else if((HWND)lp == l_toggleButton) on_toggle_clicked();
				else if((HWND)lp == l_stepButton) on_step_clicked();
				else if((HWND)lp == l_continueButton) on_continue_clicked();
				else if((HWND)lp == l_searchWndButton) create_search_window();
				else if((HWND)lp == l_ignoreWndButton) create_ignore_window();
				break;
			case EN_CHANGE:
				if((HWND)lp == l_edit) on_edit_address();
				break;
			case CBN_SELCHANGE:
				if((HWND)lp == l_formatCombo) on_format_changed();
				break;
			}
			break;
		}
		case WM_NOTIFY:
		{
			LPNMHDR notMsgHeader = (LPNMHDR)lp;

			switch(notMsgHeader->code)
			{
				case NM_CUSTOMDRAW:
				{
					if(notMsgHeader->hwndFrom == l_loggedListView)
						return on_logged_list_view_custom_draw((LPNMLVCUSTOMDRAW)lp);

					break;
				}
				case TCN_SELCHANGING:
				{
					const unsigned int selectedTab = TabCtrl_GetCurSel(l_tabControl);

					if(selectedTab == Tabs::LOGGED)
					{
						// Go to 'Traced' tab
						ShowWindow(l_tracedListView, TRUE);
						ShowWindow(l_loggedListView, FALSE);
					}
					else 
					{
						// Go to 'Logged' tab
						ShowWindow(l_tracedListView, FALSE);
						ShowWindow(l_loggedListView, TRUE);
					}

					break;
				}
				case LVN_ITEMCHANGED:
				{
					LPNMLISTVIEW stateChangeMsg = (LPNMLISTVIEW)lp;

					if(notMsgHeader->hwndFrom == l_tracedListView)
					{
						// Enable/Disable toggle button
						if(((stateChangeMsg->uNewState ^ stateChangeMsg->uOldState) & LVIS_SELECTED) == LVIS_SELECTED)
						{
							unsigned int num_selected = ListView_GetSelectedCount(l_tracedListView);

							if(num_selected > 0 && l_canToggle)
								EnableWindow(l_toggleButton, TRUE);
							else
								EnableWindow(l_toggleButton, FALSE);
						}
					}

					break;
				}
				case LVN_GETDISPINFO:
				{
					if(notMsgHeader->hwndFrom == l_loggedListView)
						on_get_disp_info((NMLVDISPINFO*)lp);

					break;
				}
			}
		}

		case WM_CTLCOLORSTATIC:
		{
			if((HWND)lp == l_statusText)
			{
				 HDC hdc = (HDC)wp;

				 if(get_analyzer_result() == AnalyzerResult::SUCCESS)
					SetTextColor(hdc, RGB(0, 164, 0));
				 else
					SetTextColor(hdc, RGB(200, 0, 0));

				 SetBkMode (hdc, TRANSPARENT);				 

				 return (INT_PTR)GetSysColorBrush(COLOR_MENU);
			}

			break;
		}

		case WM_CONTEXTMENU:
		{
			if((HWND)wp == l_watchListView)
			{
				on_watch_context_menu(lp);
			}

			break;
		}
    }

	return DefWindowProc( hwnd, msg, wp, lp );
}

static long __stdcall progress_winproc( HWND hwnd, unsigned int msg, WPARAM wp, LPARAM lp )
{ 
	switch(msg)
    {       
	case WM_COMMAND:
		switch(HIWORD(wp))
		{
			case BN_CLICKED:
			{
				if((HWND)lp == l_cancelBtn)
				{
					EnableWindow(l_cancelBtn, FALSE);
					set_cancel_clicked(true);
				}
				break;
			}
		}
	}

	return DefWindowProc( hwnd, msg, wp, lp );
}

static void on_search_size(uint32_t cx, uint32_t cy)
{
	SetWindowPos(l_searchListView, NULL, NULL, NULL, SEARCH_LISTVIEW_SIZE_X(cx), SEARCH_LISTVIEW_SIZE_Y(cy), SWP_NOMOVE | SWP_NOZORDER);
	SetWindowPos(l_searchSizeCombo, NULL, SEARCH_SIZE_COMBO_POSITION_X(cx), SEARCH_SIZE_COMBO_POSITION_Y, NULL, NULL, SWP_NOSIZE | SWP_NOZORDER);
	SetWindowPos(l_searchFormatCombo, NULL, SEARCH_FORMAT_COMBO_POSITION_X(cx), SEARCH_FORMAT_COMBO_POSITION_Y, NULL, NULL, SWP_NOSIZE | SWP_NOZORDER);
	SetWindowPos(l_searchEqualToRadio, NULL, SEARCH_EQUALTO_RADIO_POSITION_X(cx), SEARCH_EQUALTO_RADIO_POSITION_Y, NULL, NULL, SWP_NOSIZE | SWP_NOZORDER);
	SetWindowPos(l_searchNotEqualToRadio, NULL, SEARCH_NOTEQUALTO_RADIO_POSITION_X(cx), SEARCH_NOTEQUALTO_RADIO_POSITION_Y, NULL, NULL, SWP_NOSIZE | SWP_NOZORDER);
	SetWindowPos(l_searchLessThanRadio, NULL, SEARCH_LESSTHAN_RADIO_POSITION_X(cx), SEARCH_LESSTHAN_RADIO_POSITION_Y, NULL, NULL, SWP_NOSIZE | SWP_NOZORDER);
	SetWindowPos(l_searchGreaterThanRadio, NULL, SEARCH_GREATERTHAN_RADIO_POSITION_X(cx), SEARCH_GREATERTHAN_RADIO_POSITION_Y, NULL, NULL, SWP_NOSIZE | SWP_NOZORDER);
	SetWindowPos(l_searchLessThanOrEqualToRadio, NULL, SEARCH_LESSTHANOREQUALTO_RADIO_POSITION_X(cx), SEARCH_LESSTHANOREQUALTO_RADIO_POSITION_Y, NULL, NULL, SWP_NOSIZE | SWP_NOZORDER);
	SetWindowPos(l_searchGreaterThanOrEqualToRadio, NULL, SEARCH_GREATERTHANOREQUALTO_RADIO_POSITION_X(cx), SEARCH_GREATERTHANOREQUALTO_RADIO_POSITION_Y, NULL, NULL, SWP_NOSIZE | SWP_NOZORDER);
	SetWindowPos(l_searchRangeRadio, NULL, SEARCH_RANGE_RADIO_POSITION_X(cx), SEARCH_RANGE_RADIO_POSITION_Y, NULL, NULL, SWP_NOSIZE | SWP_NOZORDER);	
	SetWindowPos(l_searchRangeStartEdit, NULL, SEARCH_RANGE_START_EDIT_POSITION_X(cx), SEARCH_RANGE_START_EDIT_POSITION_Y, NULL, NULL, SWP_NOSIZE | SWP_NOZORDER);
	SetWindowPos(l_searchRangeEndEdit, NULL, SEARCH_RANGE_END_EDIT_POSITION_X(cx), SEARCH_RANGE_END_EDIT_POSITION_Y, NULL, NULL, SWP_NOSIZE | SWP_NOZORDER);
	SetWindowPos(l_searchPreviousValueRadio, NULL, SEARCH_PREVIOUSVALUE_RADIO_POSITION_X(cx), SEARCH_PREVIOUSVALUE_RADIO_POSITION_Y, NULL, NULL, SWP_NOSIZE | SWP_NOZORDER);
	SetWindowPos(l_searchSpecificValueRadio, NULL, SEARCH_SPECIFICVALUE_RADIO_POSITION_X(cx), SEARCH_SPECIFICVALUE_RADIO_POSITION_Y, NULL, NULL, SWP_NOSIZE | SWP_NOZORDER);
	SetWindowPos(l_searchSpecificValueEdit, NULL, SEARCH_SPECIFICVALUE_EDIT_POSITION_X(cx), SEARCH_SPECIFICVALUE_EDIT_POSITION_Y, NULL, NULL, SWP_NOSIZE | SWP_NOZORDER);
	SetWindowPos(l_searchSearchButton, NULL, SEARCH_SEARCHBUTTON_POSITION_X(cx), SEARCH_SEARCHBUTTON_POSITION_Y, NULL, NULL, SWP_NOSIZE | SWP_NOZORDER);
	SetWindowPos(l_searchResetButton, NULL, SEARCH_RESETBUTTON_POSITION_X(cx), SEARCH_RESETBUTTON_POSITION_Y, NULL, NULL, SWP_NOSIZE | SWP_NOZORDER);
	SetWindowPos(l_searchStatusText, NULL, SEARCH_STATUS_TEXT_POSITION_X, SEARCH_STATUS_TEXT_POSITION_Y(cy), NULL, NULL, SWP_NOSIZE | SWP_NOZORDER);

	InvalidateRect(l_searchWnd, NULL, FALSE);
}

static bool is_checked(HWND hwnd)
{
	return (SendMessage(hwnd, BM_GETCHECK, NULL, NULL) == BST_CHECKED);
}

static void on_search_size_changed()
{
	unsigned int size = 1 << SendMessage(l_searchSizeCombo, CB_GETCURSEL, NULL, NULL);
	unsigned int numItems = SendMessage(l_searchFormatCombo, CB_GETCOUNT, NULL, NULL);

	if(size == 4)
	{
		if(numItems >= 4)
			return;

		SendMessage(l_searchFormatCombo, CB_ADDSTRING, NULL, (LPARAM)"Float");
	}
	else
	{
		if(l_searchFormat == 3)
		{
			l_searchFormat = 0;

			SendMessage(l_searchFormatCombo, CB_SETCURSEL, (WPARAM)0, NULL);

			InvalidateRect(l_searchListView, NULL, FALSE);
		}

		SendMessage(l_searchFormatCombo, CB_DELETESTRING, (WPARAM)3, NULL);
	}
}

static void on_search_format_changed()
{
	l_searchFormat = SendMessage(l_searchFormatCombo, CB_GETCURSEL, NULL, NULL);

	InvalidateRect(l_searchListView, NULL, FALSE);
}

static void on_search_get_disp_info(NMLVDISPINFO* lvDispInfo)
{
	LVITEM& item = lvDispInfo->item;

	const unsigned int index = item.iItem;
	const unsigned int subIndex = item.iSubItem;

	if((item.mask & LVIF_TEXT) != LVIF_TEXT)
		return;

	std::string str;
	
	unsigned int address = 0x80000000;

	if(l_searchResults.empty())
	{
		address += index << 2;
	}
	else
	{
		address += l_searchResults[index].address;
	}

	switch(subIndex)
	{
		case 0:
		{
			str = write_hex(address, 8);

			break;
		}

		case 1:
		{
			const auto value = get_address_value(address, l_searchSize);

			str = get_value_string(value, l_searchSize, l_searchFormat, l_searchSize * 2);

			break;
		}

		case 2:
		{
			if(l_searchResults.empty())
			{
				str = "N/A";
			}
			else
			{
				const auto value = l_searchResults[index].value;

				str = get_value_string(value, l_searchSize, l_searchFormat, l_searchSize * 2);
			}

			break;
		}
	}

	if((unsigned int)item.cchTextMax <= str.size())
		return;

	std::strcpy(item.pszText, str.data());
}

static SearchOperator get_search_operator()
{
	if(is_checked(l_searchEqualToRadio))
		return SearchOperator::EQUAL_TO;
	else if(is_checked(l_searchNotEqualToRadio))
		return SearchOperator::NOT_EQUAL_TO;
	else if(is_checked(l_searchLessThanRadio))
		return SearchOperator::LESS_THAN;
	else if(is_checked(l_searchGreaterThanRadio))
		return SearchOperator::GREATER_THAN;
	else if(is_checked(l_searchLessThanOrEqualToRadio))
		return SearchOperator::LESS_THAN_OR_EQUAL_TO;
	else if(is_checked(l_searchGreaterThanOrEqualToRadio))
		return SearchOperator::GREATER_THAN_OR_EQUAL_TO;

	return SearchOperator::IN_RANGE;
}

static SearchComparator get_search_comparator()
{
	if(is_checked(l_searchPreviousValueRadio))
		return SearchComparator::PREVIOUS_VALUE;

	return SearchComparator::SPECIFIC_VALUE;
}

static bool get_edit_value(HWND edit_box, uint32_t format, uint32_t& value)
{
	CHAR buffer[100];

	SendMessage(edit_box, WM_GETTEXT, sizeof(buffer), (LPARAM)buffer);

	std::string str(buffer);

	size_t index = 0;

	try
	{
		switch(format)
		{
			case 0:
			{
				auto temp = std::stoul(str, &index, 16);

				value = *(uint32_t*)&temp;

				break;
			}

			case 1:
			{
				auto temp = std::stoul(str, &index);

				value =  *(uint32_t*)&temp;

				break;
			}

			case 2:
			{
				auto temp = std::stoi(str, &index);
		
				value = *(uint32_t*)&temp;
		
				break;
			}

			case 3:
			{
				auto temp = std::stof(str, &index);
		
				value = *(uint32_t*)&temp;
		
				break;
			}
		}
	}
	catch(std::invalid_argument e)
	{
		return false;
	}

	if(index != str.length())
	{
		return false;
	}

	return true;
}

static void on_search_clicked()
{
	// Make sure we don't hit any breakpoints while resetting variables
	set_analyzer_state(AnalyzerState::IDLE); //Potential deadlock?

	l_tempSearchResults.clear();

	l_searchSize = 1 << SendMessage(l_searchSizeCombo, CB_GETCURSEL, NULL, NULL);
	l_searchOperator = get_search_operator();
	l_searchComparator = get_search_comparator();

	if(!get_edit_value(l_searchSpecificValueEdit, l_searchFormat, l_searchSpecificValue))
	{
		MessageBox(l_searchWnd, "The specified value has an invalid format.", "Error", MB_OK | MB_ICONERROR);

		return;
	}

	if(l_searchOperator == SearchOperator::IN_RANGE)
	{
		if(!get_edit_value(l_searchRangeStartEdit, l_searchFormat, l_searchRangeStart) ||
		   !get_edit_value(l_searchRangeEndEdit, l_searchFormat, l_searchRangeEnd))
		{
			MessageBox(l_searchWnd, "The specified range has an invalid format.", "Error", MB_OK | MB_ICONERROR);

			return;
		}
	}

	create_progress_window("Searching...");

	EnableWindow(l_toggleButton, FALSE);
	EnableWindow(l_continueButton, FALSE);
	EnableWindow(l_stepButton, FALSE);
	EnableWindow(l_searchWnd, FALSE);
	EnableWindow(l_ignoreWnd, FALSE);
	EnableWindow(l_window, FALSE);
	
	remove_breakpoints();
	run_and_continue(); //In case we were already in a breakpoint when we pressed Analyze.

	start_timer();

	if(l_searchResults.empty())
		set_progress_bar_range(0, 0x800000);
	else
		set_progress_bar_range(0, l_searchResults.size());

	set_status_message("Waiting for vertical interrupt...");
	set_cancel_clicked(false);
	set_analyzer_state(AnalyzerState::SEARCHING);
}

static void on_reset_clicked()
{
	l_searchSize = 1 << SendMessage(l_searchSizeCombo, CB_GETCURSEL, NULL, NULL);
	l_searchResults.clear();
	
	ListView_SetItemCountEx(l_searchListView, 0x200000, NULL);

	InvalidateRect(l_searchListView, NULL, FALSE);
	EnableWindow(l_searchPreviousValueRadio, FALSE);
	
	std::stringstream stream;

	stream << "Search has been reset. " << 0x200000 << " results remaining.";

	SetWindowText(l_searchStatusText, stream.str().c_str());
}

static void on_search_completed()
{
	EnableWindow(l_searchWnd, TRUE);
	EnableWindow(l_ignoreWnd, TRUE);
	EnableWindow(l_window, TRUE);
	DestroyWindow(l_progressWnd);

	if(l_tempSearchResults.empty())
	{
		SetWindowText(l_searchStatusText, "No addresses found. Keeping previous results.");
	}
	else
	{
		const uint32_t prevResults = l_searchResults.empty() ? 0x200000 : l_searchResults.size();
		const uint32_t currResults = l_tempSearchResults.size();
		
		l_searchResults = l_tempSearchResults;

		std::stringstream stream;

		stream << prevResults - currResults << " addresses removed. " << currResults << " results remaining.";

		SetWindowText(l_searchStatusText, stream.str().c_str());

		ListView_SetItemCountEx(l_searchListView, l_searchResults.size(), NULL);

		InvalidateRect(l_searchListView, NULL, FALSE);
	}
	
	EnableWindow(l_searchPreviousValueRadio, TRUE);

	l_analyserState = AnalyzerState::IDLE;
}

static void on_search_aborted()
{
	EnableWindow(l_searchWnd, TRUE);
	EnableWindow(l_ignoreWnd, TRUE);
	EnableWindow(l_window, TRUE);
	DestroyWindow(l_progressWnd);
	
	SetWindowText(l_searchStatusText, "Search was aborted by the user.");
	
	l_analyserState = AnalyzerState::IDLE;
}

static LRESULT on_search_list_view_custom_draw(LPNMLVCUSTOMDRAW lvCustomDraw)
{
	switch(lvCustomDraw->nmcd.dwDrawStage) 
    {
		case CDDS_PREPAINT:
            return CDRF_NOTIFYITEMDRAW; // request notifications for individual listview items
            
        case CDDS_ITEMPREPAINT:
			const unsigned int index = lvCustomDraw->nmcd.dwItemSpec;

			if(l_searchResults.empty())
				return CDRF_DODEFAULT;

			const auto& searchResult = l_searchResults[index];
			const auto value = get_address_value(0x80000000 + searchResult.address, l_searchSize);

			if(value == searchResult.value)
				return CDRF_DODEFAULT;

            lvCustomDraw->clrTextBk = RGB(255, 200, 200);
				return CDRF_NEWFONT;
	}

	return CDRF_DODEFAULT;
}

static void on_search_context_menu(LPARAM lp)
{	
	const uint16_t x = LOWORD(lp);
	const uint16_t y = HIWORD(lp);

	LVHITTESTINFO hitTestInfo;

	hitTestInfo.pt.x = x;
	hitTestInfo.pt.y = y;

	ScreenToClient(l_searchListView, &hitTestInfo.pt);
	
	const int item = ListView_HitTest(l_searchListView, &hitTestInfo);

	if(item == -1)
		return;
	
	const uint32_t result = display_context_menu(l_searchWnd, x, y, {"Trace", "Watch", "Ignore"});

	if(result == 0)
		return;	

	unsigned int address = 0x80000000;

	if(l_searchResults.empty())
	{
		address += item << 2;
	}
	else
	{
		address += l_searchResults[item].address;
	}

	switch(result)
	{
		case 1:
		{
			std::string str = write_hex(address, 8);

			SetWindowText(l_edit, str.c_str());

			break;
		}

		case 2:
		{
			break;
		}

		case 3:
		{
			add_to_ignore_list(IgnoreItem(address, l_searchSize, "N/A"));	

			break;
		}
	}
}

static long __stdcall search_winproc(HWND hwnd, unsigned int msg, WPARAM wp, LPARAM lp)
{ 
	switch(msg)
    {
		case WM_SEARCH_COMPLETED:
		{
			on_search_completed();
			break;
		}

		case WM_ABORT:
		{
			on_search_aborted();
			break;
		}

		case WM_DESTROY:
		{
			l_searchWnd = NULL;
			break;
		}

		case WM_GETMINMAXINFO:
		{
			RECT client;
			client.left = 0;
			client.top = 0;
			client.right = SEARCH_WINDOW_SIZE_X;
			client.bottom = SEARCH_WINDOW_SIZE_Y;

			AdjustWindowRect(&client, SEARCH_WINDOW_STYLE, FALSE);

			MINMAXINFO* minMaxInfo = (MINMAXINFO*)lp;
			minMaxInfo->ptMinTrackSize.x = (client.right - client.left);
			minMaxInfo->ptMinTrackSize.y = (client.bottom - client.top);

			return 0L;
		}

		case WM_SIZE:
		{
			on_search_size(LOWORD(lp), HIWORD(lp)); 
			break;
		}

		case WM_COMMAND:
		{
			switch(HIWORD(wp))
			{
				case BN_CLICKED:
				{
					if((HWND)lp == l_searchPreviousValueRadio && is_checked(l_searchPreviousValueRadio))
					{
						EnableWindow(l_searchSpecificValueEdit, FALSE);
					}
					else if((HWND)lp == l_searchSpecificValueRadio && is_checked(l_searchSpecificValueRadio))
					{
						EnableWindow(l_searchSpecificValueEdit, TRUE);
					}
					else if((HWND)lp == l_searchRangeRadio && is_checked(l_searchRangeRadio))
					{
						EnableWindow(l_searchRangeStartEdit, TRUE);
						EnableWindow(l_searchRangeEndEdit, TRUE);
					}
					else if(((HWND)lp == l_searchEqualToRadio && is_checked(l_searchEqualToRadio)) ||
							((HWND)lp == l_searchNotEqualToRadio && is_checked(l_searchNotEqualToRadio)) ||
							((HWND)lp == l_searchLessThanRadio && is_checked(l_searchLessThanRadio)) ||
							((HWND)lp == l_searchGreaterThanRadio && is_checked(l_searchGreaterThanRadio)) ||
							((HWND)lp == l_searchLessThanOrEqualToRadio && is_checked(l_searchLessThanOrEqualToRadio)) ||
							((HWND)lp == l_searchGreaterThanOrEqualToRadio && is_checked(l_searchGreaterThanOrEqualToRadio)))
					{
						EnableWindow(l_searchRangeStartEdit, FALSE);
						EnableWindow(l_searchRangeEndEdit, FALSE);
					}
					else if((HWND)lp == l_searchSearchButton)
					{
						on_search_clicked();
					}
					else if((HWND)lp == l_searchResetButton)
					{
						on_reset_clicked();
					}

					break;
				}

				case CBN_SELCHANGE:
				{
					if((HWND)lp == l_searchSizeCombo) on_search_size_changed();
					else if((HWND)lp == l_searchFormatCombo) on_search_format_changed();

					break;
				}
			}

			break;
		}

		case WM_NOTIFY:
		{
			LPNMHDR notMsgHeader = (LPNMHDR)lp;

			switch(notMsgHeader->code)
			{
				case NM_CUSTOMDRAW:
				{
					if(notMsgHeader->hwndFrom == l_searchListView)
						return on_search_list_view_custom_draw((LPNMLVCUSTOMDRAW)lp);

					break;
				}

				case LVN_GETDISPINFO:
				{
					if(notMsgHeader->hwndFrom == l_searchListView)
						on_search_get_disp_info((NMLVDISPINFO*)lp);

					break;
				}
			}

			break;
		}

		case WM_CONTEXTMENU:
		{
			if((HWND)wp == l_searchListView)
			{
				on_search_context_menu(lp);
			}

			break;
		}
	}

	return DefWindowProc(hwnd, msg, wp, lp);
}

static void on_ignore_size(uint32_t cx, uint32_t cy)
{
	SetWindowPos(l_ignoreListView, NULL, NULL, NULL, IGNORE_LISTVIEW_SIZE_X(cx), IGNORE_LISTVIEW_SIZE_Y(cy), SWP_NOMOVE | SWP_NOZORDER);
}

static void on_ignore_get_disp_info(NMLVDISPINFO* lvDispInfo)
{
	LVITEM& item = lvDispInfo->item;

	const unsigned int index = item.iItem;
	const unsigned int subIndex = item.iSubItem;

	if((item.mask & LVIF_TEXT) != LVIF_TEXT)
		return;

	const auto& ignoreItem = l_ignoreList[index];

	std::string str;

	switch(subIndex)
	{
		case 0:
		{
			str = write_hex(ignoreItem.address, 8);

			break;
		}

		case 1:
		{
			str = write_size(ignoreItem.size);

			break;
		}

		case 2:
		{
			str = ignoreItem.description;

			break;
		}
	}

	if((unsigned int)item.cchTextMax <= str.size())
		return;

	std::strcpy(item.pszText, str.data());
}

static void on_ignore_context_menu(LPARAM lp)
{	
	const uint16_t x = LOWORD(lp);
	const uint16_t y = HIWORD(lp);

	LVHITTESTINFO hitTestInfo;

	hitTestInfo.pt.x = x;
	hitTestInfo.pt.y = y;

	ScreenToClient(l_ignoreListView, &hitTestInfo.pt);
	
	const int item = ListView_HitTest(l_ignoreListView, &hitTestInfo);

	if(item == -1)
		return;
	
	const uint32_t result = display_context_menu(l_ignoreWnd, x, y, {"Delete"});

	if(result == 0)
		return;	

	remove_from_ignore_list(l_ignoreList[item]);
}

static void on_ignore_add()
{
	uint32_t address = 0;

	if(!get_edit_value(l_ignoreAddressEdit, 0, address))
	{
		MessageBox(l_ignoreWnd, "Address has an invalid format.", "Error", MB_OK | MB_ICONERROR);

		return;
	}

	if((address < 0x80000000) ||
		(address > 0x8007FFFFC))
	{
		MessageBox(l_ignoreWnd, "Address is outside the valid range.", "Error", MB_OK | MB_ICONERROR);

		return;
	}

	uint32_t size = 1 << SendMessage(l_ignoreSizeCombo, CB_GETCURSEL, NULL, NULL);

	add_to_ignore_list(IgnoreItem(address, size, "N/A"));
}

static long __stdcall ignore_winproc(HWND hwnd, unsigned int msg, WPARAM wp, LPARAM lp)
{ 
	switch(msg)
    {
		case WM_DESTROY:
		{
			l_ignoreWnd = NULL;
			break;
		}

		case WM_GETMINMAXINFO:
		{
			RECT client;
			client.left = 0;
			client.top = 0;
			client.right = IGNORE_WINDOW_SIZE_X;
			client.bottom = IGNORE_WINDOW_SIZE_Y;

			AdjustWindowRect(&client, IGNORE_WINDOW_STYLE, FALSE);

			MINMAXINFO* minMaxInfo = (MINMAXINFO*)lp;
			minMaxInfo->ptMinTrackSize.x = (client.right - client.left);
			minMaxInfo->ptMinTrackSize.y = (client.bottom - client.top);

			return 0L;
		}

		case WM_SIZE:
		{
			on_ignore_size(LOWORD(lp), HIWORD(lp)); 
			break;
		}

		case WM_COMMAND:
		{
			switch(HIWORD(wp))
			{
				case BN_CLICKED:
				{
					if((HWND)lp == l_ignoreAddButton)
						on_ignore_add();

					break;
				}
			}

			break;
		}

		case WM_NOTIFY:
		{
			LPNMHDR notMsgHeader = (LPNMHDR)lp;

			switch(notMsgHeader->code)
			{
				case LVN_GETDISPINFO:
				{
					if(notMsgHeader->hwndFrom == l_ignoreListView)
						on_ignore_get_disp_info((NMLVDISPINFO*)lp);

					break;
				}
			}

			break;
		}

		case WM_CONTEXTMENU:
		{
			if((HWND)wp == l_ignoreListView)
			{
				on_ignore_context_menu(lp);
			}

			break;
		}
	}

	return DefWindowProc(hwnd, msg, wp, lp);
}

static HWND create_list_view(	HINSTANCE instance, HWND parent, 
								int x, int y, int size_x, int size_y,
								const std::vector<LPSTR>& column_titles,
								const std::vector<LONG>& column_sizes,
								bool is_virtual = false,
								const std::string& image_list = "",
								const unsigned int image_width = 0)
{
	assert(column_titles.size() == column_sizes.size());

	DWORD listViewStyle = LVS_REPORT | LVS_SHOWSELALWAYS;

	if(is_virtual)
		listViewStyle |= LVS_OWNERDATA;

	HWND listView = CreateWindow(WC_LISTVIEW, "", 
					 WS_VISIBLE | WS_BORDER | WS_CHILD | listViewStyle,
					 x, y, size_x, size_y, parent, NULL, instance, 0);

	ListView_SetExtendedListViewStyle(listView, LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES | LVS_EX_DOUBLEBUFFER);

	int numColumns = column_titles.size();

	for(int i = (numColumns - 1); i >= 0; i--)
	{
		LVCOLUMN column;

		column.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM;
		column.pszText = column_titles[i];
		column.cx = column_sizes[i];
		column.fmt = LVCFMT_LEFT;
		column.iSubItem = i;
				
		ListView_InsertColumn(listView, 0, &column);
	}

	if(!image_list.empty())
	{
		HIMAGELIST imageList = ImageList_LoadImage(NULL, image_list.c_str(), image_width, 1, CLR_DEFAULT, IMAGE_BITMAP, LR_CREATEDIBSECTION | LR_LOADFROMFILE);

		if(imageList == NULL)
		{
			MessageBox(NULL, "Failed to load image list", "Image list error", MB_OK | MB_ICONERROR);
		}

		ListView_SetImageList(listView, imageList, LVSIL_STATE);
	}

	return listView;
}

static HWND create_button(HINSTANCE instance, HWND parent,  DWORD button_style, const std::wstring& text, int x, int y, int size_x, int size_y, bool enabled = true)
{
	HGDIOBJ font = GetStockObject(DEFAULT_GUI_FONT);

	HWND button = CreateWindowW( L"BUTTON", text.c_str(),	WS_TABSTOP | WS_VISIBLE | WS_CHILD | button_style,
										x, y, size_x, size_y,
										parent,	NULL, instance,	NULL);

	SendMessage(button, WM_SETFONT, (WPARAM)font, NULL);

	if(!enabled)
		EnableWindow(button, FALSE);

	return button;
}

static HWND create_bitmap_button(HINSTANCE instance, HWND parent,  int x, int y, int size_x, int size_y, const std::string& image_path, bool enabled = true)
{	
	HWND button = create_button(instance, parent, BS_BITMAP, L"", x,y, size_x, size_y, enabled);	
	HBITMAP bitmap = (HBITMAP)LoadImage(NULL, image_path.c_str(), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);

	SendMessage(button, BM_SETIMAGE, IMAGE_BITMAP, (LPARAM)bitmap);

	return button;
}

static HWND create_radio_button(HINSTANCE instance, HWND parent,  const std::wstring& text, int x, int y, int size_x, int size_y, bool new_group = false)
{
	DWORD button_style = BS_AUTORADIOBUTTON;

	if(new_group)
		button_style |= WS_GROUP;

	return create_button(instance, parent, button_style, text, x, y, size_x, size_y);
}

static HWND create_drop_down_list(HINSTANCE instance, HWND parent, int x, int y, int size_x, int size_y, const std::vector<std::string>& items, int default = 0)
{
	HGDIOBJ font = GetStockObject(DEFAULT_GUI_FONT);

	HWND dropDownList = CreateWindow(	WC_COMBOBOX, TEXT(""), CBS_DROPDOWNLIST | CBS_HASSTRINGS | WS_CHILD | WS_VISIBLE,
										x, y, size_x, size_y, 
										parent, NULL, instance, NULL);			

	for(const auto& item : items)
	{
		SendMessage(dropDownList, CB_ADDSTRING, NULL, (LPARAM)item.c_str());
	}
	
	SendMessage(dropDownList, CB_SETCURSEL, (WPARAM)default, NULL);
	SendMessage(dropDownList, WM_SETFONT, (WPARAM)font, NULL);

	return dropDownList;
}

static HWND create_tab_control(HINSTANCE instance, HWND parent, int x, int y, int size_x, int size_y, const std::vector<std::string>& items)
{
	HGDIOBJ font = GetStockObject(DEFAULT_GUI_FONT);

	HWND tabControl = CreateWindowEx(	NULL, WC_TABCONTROL, TEXT(""), TCS_FIXEDWIDTH | WS_CHILD | WS_VISIBLE,
										x, y, size_x, size_y, 
										l_window, NULL, instance, NULL);
	
	std::array<CHAR, 100> temp;

	for(unsigned int i = 0; i < items.size(); i++)
	{
		strcpy(temp.data(), items[i].c_str());
		
		TCITEM item = TCITEM();
		item.mask = TCIF_TEXT;
		item.pszText = temp.data();

		TabCtrl_InsertItem(tabControl, i, &item);
	}
	
	SendMessage(tabControl, WM_SETFONT, (WPARAM)font, NULL);

	return tabControl;
}

static HWND create_edit_box(HINSTANCE instance, HWND parent, int x, int y, int size_x, int size_y, int char_limit, const std::wstring& text = L"")
{
	HGDIOBJ font = GetStockObject(DEFAULT_GUI_FONT);

	HWND editBox = CreateWindowExW(WS_EX_CLIENTEDGE, L"EDIT", text.c_str(),
                      WS_VISIBLE | WS_CHILD | WS_BORDER | ES_LEFT,
                      x, y, size_x, size_y,
                      parent,
                      NULL, instance, NULL);
			
	SendMessage(editBox, EM_LIMITTEXT, (WPARAM)char_limit, NULL);
	SendMessage(editBox, WM_SETFONT, (WPARAM)font, NULL);

	return editBox;
}

static void set_window_icon(HWND window, const std::string& path)
{
	HICON icon = (HICON)LoadImage(0, path.c_str(), IMAGE_ICON, 0, 0, LR_LOADFROMFILE);

	if(icon == NULL)
		return;

	SendMessage(window, WM_SETICON, ICON_SMALL, (LPARAM)icon);
	SendMessage(window, WM_SETICON, ICON_BIG, (LPARAM)icon);

	SendMessage(GetWindow(window, GW_OWNER), WM_SETICON, ICON_SMALL, (LPARAM)icon);
	SendMessage(GetWindow(window, GW_OWNER), WM_SETICON, ICON_BIG, (LPARAM)icon);
}

static void create_progress_window(const std::string& title)
{
	RECT client;
	client.left = 0;
	client.top = 0;
	client.right = PROGRESS_WINDOW_SIZE_X;
	client.bottom = PROGRESS_WINDOW_SIZE_Y;

	AdjustWindowRect(&client, PROGRESS_WINDOW_STYLE, FALSE);

	LONG progress_size_x = client.right - client.left;
	LONG progress_size_y = client.bottom - client.top;

	RECT parent;
	GetWindowRect(l_window, &parent);

	LONG parent_size_x = parent.right - parent.left;
	LONG parent_size_y = parent.bottom - parent.top;

	l_progressWnd = CreateWindowEx(NULL, progress_window_class.c_str(), title.c_str(), PROGRESS_WINDOW_STYLE,
		PROGRESS_WINDOW_POSITION_X(parent.left, parent_size_x), PROGRESS_WINDOW_POSITION_Y(parent.top, parent_size_y), 
		progress_size_x, progress_size_y, l_window, NULL, GetModuleHandle(0), NULL);

	if(l_progressWnd)
	{
		HINSTANCE instance = (HINSTANCE)GetWindowLong(l_window, GWL_HINSTANCE);
		HGDIOBJ font = GetStockObject(DEFAULT_GUI_FONT);

		GetClientRect(l_progressWnd, &client);

		LONG size_x = client.right - client.left;
		LONG size_y = client.bottom - client.top;

		// Status text
		l_statusMsg = CreateWindowEx(NULL, "STATIC", "", WS_CHILD | WS_VISIBLE, STATUS_POSITION_X, STATUS_POSITION_Y,
			STATUS_SIZE_X, STATUS_SIZE_Y, l_progressWnd, NULL, instance, NULL);
					
		SendMessage(l_statusMsg, WM_SETFONT, (WPARAM)font, NULL);
					
		// Progress bar
		l_progressBar = CreateWindowEx(NULL, PROGRESS_CLASS, NULL, WS_CHILD | WS_VISIBLE | PBS_MARQUEE | PBS_SMOOTH, 
			PROGRESS_POSITION_X, PROGRESS_POSITION_Y, PROGRESS_SIZE_X(size_x), PROGRESS_SIZE_Y,
			l_progressWnd, NULL, instance, NULL);
			
		SendMessage(l_progressBar, PBM_SETMARQUEE, TRUE, 30);

		//Cancel button
		l_cancelBtn = CreateWindowEx(NULL, "BUTTON", "Cancel", WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON, 
						CANCEL_POSITION_X(size_x), CANCEL_POSITION_Y, CANCEL_SIZE_X, CANCEL_SIZE_Y,
						l_progressWnd, NULL, instance, NULL);
		
		SendMessage(l_cancelBtn, WM_SETFONT, (WPARAM)font, NULL);
					
		ShowWindow(l_progressWnd, SW_SHOWDEFAULT);
	}
}

static void create_search_window()
{
	if(l_searchWnd != NULL) 
	{
		SetForegroundWindow(l_searchWnd);

		return;
	}

	RECT client;
	client.left = 0;
	client.top = 0;
	client.right = SEARCH_WINDOW_SIZE_X;
	client.bottom = SEARCH_WINDOW_SIZE_Y;

	AdjustWindowRect(&client, SEARCH_WINDOW_STYLE, FALSE);

	LONG search_size_x = client.right - client.left;
	LONG search_size_y = client.bottom - client.top;

	RECT parent;
	GetWindowRect(l_window, &parent);

	LONG parent_size_x = parent.right - parent.left;
	LONG parent_size_y = parent.bottom - parent.top;

	l_searchWnd = CreateWindowEx(NULL, search_window_class.c_str(), "Search", SEARCH_WINDOW_STYLE,
		SEARCH_WINDOW_POSITION_X(parent.left, parent_size_x), SEARCH_WINDOW_POSITION_Y(parent.top, parent_size_y), 
		search_size_x, search_size_y, l_window, NULL, GetModuleHandle(0), NULL);

	if(l_searchWnd)
	{
		HINSTANCE instance = (HINSTANCE)GetWindowLong(l_window, GWL_HINSTANCE);
		HGDIOBJ font = GetStockObject(DEFAULT_GUI_FONT);
		
		GetClientRect(l_searchWnd, &client);

		LONG size_x = client.right - client.left;
		LONG size_y = client.bottom - client.top;

		// Search list view
		l_searchListView = create_list_view(instance, l_searchWnd,
											SEARCH_LISTVIEW_POSITION_X, SEARCH_LISTVIEW_POSITION_Y, 
											SEARCH_LISTVIEW_SIZE_X(size_x), SEARCH_LISTVIEW_SIZE_Y(size_y),
											{"Address", "Current value", "Previous value"},
											{100, 125, 125}, true);
		
		if(l_searchResults.empty())
			ListView_SetItemCountEx(l_searchListView, 0x200000, NULL);
		else
			ListView_SetItemCountEx(l_searchListView, l_searchResults.size(), NULL);

		// Size combo
		l_searchSizeCombo = create_drop_down_list(	instance, l_searchWnd, 
													SEARCH_SIZE_COMBO_POSITION_X(size_x), SEARCH_SIZE_COMBO_POSITION_Y, 
													SEARCH_SIZE_COMBO_SIZE_X, SEARCH_SIZE_COMBO_SIZE_Y,
													{"1 Byte", "2 Byte", "4 Byte"}, 2);

		// Format combo
		l_searchFormatCombo = create_drop_down_list(instance, l_searchWnd, 
													SEARCH_FORMAT_COMBO_POSITION_X(size_x), SEARCH_FORMAT_COMBO_POSITION_Y, 
													SEARCH_FORMAT_COMBO_SIZE_X, SEARCH_FORMAT_COMBO_SIZE_Y,
													{"Hex", "Unsigned", "Signed", "Float"});
		// Equal to Radio
		l_searchEqualToRadio = create_radio_button(instance, l_searchWnd, L"Equal To", 
			SEARCH_EQUALTO_RADIO_POSITION_X(size_x), SEARCH_EQUALTO_RADIO_POSITION_Y, 
			SEARCH_EQUALTO_RADIO_SIZE_X, SEARCH_EQUALTO_RADIO_SIZE_Y, true);

		// Not Equal To Radio
		l_searchNotEqualToRadio = create_radio_button(instance, l_searchWnd, L"Not Equal To", 
			SEARCH_NOTEQUALTO_RADIO_POSITION_X(size_x), SEARCH_NOTEQUALTO_RADIO_POSITION_Y, 
			SEARCH_NOTEQUALTO_RADIO_SIZE_X, SEARCH_NOTEQUALTO_RADIO_SIZE_Y);

		// Less Than Radio
		l_searchLessThanRadio = create_radio_button(instance, l_searchWnd, L"Less Than", 
			SEARCH_LESSTHAN_RADIO_POSITION_X(size_x), SEARCH_LESSTHAN_RADIO_POSITION_Y, 
			SEARCH_LESSTHAN_RADIO_SIZE_X, SEARCH_LESSTHAN_RADIO_SIZE_Y);

		// Greater Than Radio
		l_searchGreaterThanRadio = create_radio_button(instance, l_searchWnd, L"Greater Than", 
			SEARCH_GREATERTHAN_RADIO_POSITION_X(size_x), SEARCH_GREATERTHAN_RADIO_POSITION_Y, 
			SEARCH_GREATERTHAN_RADIO_SIZE_X, SEARCH_GREATERTHAN_RADIO_SIZE_Y);
		
		// Less Than Or Equal Radio
		l_searchLessThanOrEqualToRadio = create_radio_button(instance, l_searchWnd, L"Less Than Or Equal To", 
			SEARCH_LESSTHANOREQUALTO_RADIO_POSITION_X(size_x), SEARCH_LESSTHANOREQUALTO_RADIO_POSITION_Y, 
			SEARCH_LESSTHANOREQUALTO_RADIO_SIZE_X, SEARCH_LESSTHANOREQUALTO_RADIO_SIZE_Y);
		
		// Greater Than Or Equal Radio
		l_searchGreaterThanOrEqualToRadio = create_radio_button(instance, l_searchWnd, L"Greater Than Or Equal To", 
			SEARCH_GREATERTHANOREQUALTO_RADIO_POSITION_X(size_x), SEARCH_GREATERTHANOREQUALTO_RADIO_POSITION_Y, 
			SEARCH_GREATERTHANOREQUALTO_RADIO_SIZE_X, SEARCH_GREATERTHANOREQUALTO_RADIO_SIZE_Y);

		// Range Radio
		l_searchRangeRadio = create_radio_button(instance, l_searchWnd, L"In Range",
			SEARCH_RANGE_RADIO_POSITION_X(size_x), SEARCH_RANGE_RADIO_POSITION_Y,
			SEARCH_RANGE_RADIO_SIZE_X, SEARCH_RANGE_RADIO_SIZE_Y);

		SendMessage(l_searchEqualToRadio, BM_SETCHECK, (WPARAM)BST_CHECKED, NULL);

		
		// Range Start Edit Box
		l_searchRangeStartEdit = create_edit_box(instance, l_searchWnd,
			SEARCH_RANGE_START_EDIT_POSITION_X(size_x), SEARCH_RANGE_START_EDIT_POSITION_Y,
			SEARCH_RANGE_START_EDIT_SIZE_X, SEARCH_RANGE_START_EDIT_SIZE_Y, MAX_CHARACTERS, L"0");
		
		// Range End Edit Box
		l_searchRangeEndEdit = create_edit_box(instance, l_searchWnd,
			SEARCH_RANGE_END_EDIT_POSITION_X(size_x), SEARCH_RANGE_END_EDIT_POSITION_Y,
			SEARCH_RANGE_END_EDIT_SIZE_X, SEARCH_RANGE_END_EDIT_SIZE_Y, MAX_CHARACTERS, L"0");
		
		EnableWindow(l_searchRangeStartEdit, FALSE);
		EnableWindow(l_searchRangeEndEdit, FALSE);

		// Previous Value Radio
		l_searchPreviousValueRadio = create_radio_button(instance, l_searchWnd, L"Previous Value", 
			SEARCH_PREVIOUSVALUE_RADIO_POSITION_X(size_x), SEARCH_PREVIOUSVALUE_RADIO_POSITION_Y, 
			SEARCH_PREVIOUSVALUE_RADIO_SIZE_X, SEARCH_PREVIOUSVALUE_RADIO_SIZE_Y, true);

		// Specific Value Radio
		l_searchSpecificValueRadio = create_radio_button(instance, l_searchWnd, L"Specific Value:", 
			SEARCH_SPECIFICVALUE_RADIO_POSITION_X(size_x), SEARCH_SPECIFICVALUE_RADIO_POSITION_Y, 
			SEARCH_SPECIFICVALUE_RADIO_SIZE_X, SEARCH_SPECIFICVALUE_RADIO_SIZE_Y);
		
		SendMessage(l_searchSpecificValueRadio, BM_SETCHECK, (WPARAM)BST_CHECKED, NULL);
		EnableWindow(l_searchPreviousValueRadio, FALSE);

		// Specific Value Edit Box
		l_searchSpecificValueEdit = create_edit_box(instance, l_searchWnd,
			SEARCH_SPECIFICVALUE_EDIT_POSITION_X(size_x), SEARCH_SPECIFICVALUE_EDIT_POSITION_Y,
			SEARCH_SPECIFICVALUE_EDIT_SIZE_X, SEARCH_SPECIFICVALUE_EDIT_SIZE_Y, MAX_CHARACTERS, L"0");

		// Search button		
		l_searchSearchButton = create_bitmap_button(instance, l_searchWnd, 
			SEARCH_SEARCHBUTTON_POSITION_X(size_x), SEARCH_SEARCHBUTTON_POSITION_Y,
			SEARCH_SEARCHBUTTON_SIZE_X, SEARCH_SEARCHBUTTON_SIZE_Y, 
			"Resources/search_16x16.bmp");

		// Reset button
		l_searchResetButton = create_bitmap_button(instance, l_searchWnd, 
			SEARCH_RESETBUTTON_POSITION_X(size_x), SEARCH_RESETBUTTON_POSITION_Y,
			SEARCH_RESETBUTTON_SIZE_X, SEARCH_RESETBUTTON_SIZE_Y, 
			"Resources/reset_16x16.bmp");

		// Status text
		l_searchStatusText = CreateWindowEx(NULL, "STATIC", "", WS_CHILD | WS_VISIBLE | SS_LEFT, SEARCH_STATUS_TEXT_POSITION_X, SEARCH_STATUS_TEXT_POSITION_Y(size_y),
											SEARCH_STATUS_TEXT_SIZE_X, SEARCH_STATUS_TEXT_SIZE_Y, l_searchWnd, NULL, instance, NULL);

		SendMessage(l_searchStatusText, WM_SETFONT, (WPARAM)font, NULL);

		ShowWindow(l_searchWnd, SW_SHOWDEFAULT);
	}
}

static void create_ignore_window()
{
	if(l_ignoreWnd != NULL) 
	{
		SetForegroundWindow(l_ignoreWnd);

		return;
	}

	RECT client;
	client.left = 0;
	client.top = 0;
	client.right = IGNORE_WINDOW_SIZE_X;
	client.bottom = IGNORE_WINDOW_SIZE_Y;

	AdjustWindowRect(&client, IGNORE_WINDOW_STYLE, FALSE);

	LONG ignore_size_x = client.right - client.left;
	LONG ignore_size_y = client.bottom - client.top;

	RECT parent;
	GetWindowRect(l_window, &parent);

	LONG parent_size_x = parent.right - parent.left;
	LONG parent_size_y = parent.bottom - parent.top;

	l_ignoreWnd = CreateWindowEx(NULL, ignore_window_class.c_str(), "Ignore list", IGNORE_WINDOW_STYLE,
		IGNORE_WINDOW_POSITION_X(parent.left, parent_size_x), IGNORE_WINDOW_POSITION_Y(parent.top, parent_size_y), 
		ignore_size_x, ignore_size_y, l_window, NULL, GetModuleHandle(0), NULL);

	if(l_ignoreWnd)
	{
		HINSTANCE instance = (HINSTANCE)GetWindowLong(l_window, GWL_HINSTANCE);
		HGDIOBJ font = GetStockObject(DEFAULT_GUI_FONT);
		
		GetClientRect(l_ignoreWnd, &client);

		LONG size_x = client.right - client.left;
		LONG size_y = client.bottom - client.top;

		// Ignore list view
		l_ignoreListView = create_list_view(instance, l_ignoreWnd,
											IGNORE_LISTVIEW_POSITION_X, IGNORE_LISTVIEW_POSITION_Y, 
											IGNORE_LISTVIEW_SIZE_X(size_x), IGNORE_LISTVIEW_SIZE_Y(size_y),
											{"Address", "Size", "Description"},
											{100, 50, 125}, true);

		// Ignore address edit box
		l_ignoreAddressEdit = create_edit_box(instance, l_ignoreWnd,
												IGNORE_ADDRESS_EDIT_POSITION_X, IGNORE_ADDRESS_EDIT_POSITION_Y,
												IGNORE_ADDRESS_EDIT_SIZE_X, IGNORE_ADDRESS_EDIT_SIZE_Y, MAX_CHARACTERS,
												L"0x80000000");

		// Ignore size combo
		l_ignoreSizeCombo = create_drop_down_list(instance, l_ignoreWnd,
													IGNORE_SIZE_COMBO_POSITION_X, IGNORE_SIZE_COMBO_POSITION_Y,
													IGNORE_SIZE_COMBO_SIZE_X, IGNORE_SIZE_COMBO_SIZE_Y,
													{"Byte", "Half-Word", "Word", "Double-Word"});

		// Ignore add button
		l_ignoreAddButton = create_button(instance, l_ignoreWnd, BS_DEFPUSHBUTTON, L"Add", 
											IGNORE_ADD_BUTTON_POSITION_X, IGNORE_ADD_BUTTON_POSITION_Y,
											IGNORE_ADD_BUTTON_SIZE_X, IGNORE_ADD_BUTTON_SIZE_Y);
		
		if(!l_ignoreList.empty())
			ListView_SetItemCountEx(l_ignoreListView, l_ignoreList.size(), NULL);

		ShowWindow(l_ignoreWnd, SW_SHOWDEFAULT);
	}
}

static void create_analyzer_window()
{
	InitCommonControls();

	WNDCLASSEX progress_class = { sizeof(WNDCLASSEX), CS_DBLCLKS, progress_winproc,
                    0, 0, GetModuleHandle(0), LoadIcon(0,IDI_APPLICATION),
                    LoadCursor(0,IDC_ARROW), HBRUSH(COLOR_WINDOW),
                    0, progress_window_class.c_str(), LoadIcon(0,IDI_APPLICATION) };

	WNDCLASSEX search_class = { sizeof(WNDCLASSEX), CS_DBLCLKS, search_winproc,
                    0, 0, GetModuleHandle(0), LoadIcon(0,IDI_APPLICATION),
                    LoadCursor(0,IDC_ARROW), HBRUSH(COLOR_WINDOW),
                    0, search_window_class.c_str(), LoadIcon(0,IDI_APPLICATION) };

	WNDCLASSEX ignore_class = { sizeof(WNDCLASSEX), CS_DBLCLKS, ignore_winproc,
                    0, 0, GetModuleHandle(0), LoadIcon(0,IDI_APPLICATION),
                    LoadCursor(0,IDC_ARROW), HBRUSH(COLOR_WINDOW),
                    0, ignore_window_class.c_str(), LoadIcon(0,IDI_APPLICATION) };

	WNDCLASSEX analyzer_class = { sizeof(WNDCLASSEX), CS_DBLCLKS, winproc,
                            0, 0, GetModuleHandle(0), LoadIcon(0,IDI_APPLICATION),
                            LoadCursor(0,IDC_ARROW), HBRUSH(COLOR_WINDOW),
                            0, analyzer_window_class.c_str(), LoadIcon(0,IDI_APPLICATION) };

    if(RegisterClassEx(&analyzer_class) && 
		RegisterClassEx(&progress_class) &&
		RegisterClassEx(&search_class) &&
		RegisterClassEx(&ignore_class))
    {
		RECT client;
		client.left = 0;
		client.top = 0;
		client.right = WINDOW_SIZE_X;
		client.bottom = WINDOW_SIZE_Y;

		AdjustWindowRect(&client, WINDOW_STYLE, FALSE);

		LONG size_x = client.right - client.left;
		LONG size_y = client.bottom - client.top;

		int screen_size_x = GetSystemMetrics(SM_CXSCREEN);
		int screen_size_y = GetSystemMetrics(SM_CYSCREEN);

        l_window =	CreateWindowEx(NULL, analyzer_window_class.c_str(), "N64 Analyzer v1.0",
						WINDOW_STYLE, WINDOW_POSITION_X(screen_size_x), WINDOW_POSITION_Y(screen_size_y),
						size_x, size_y, 0, 0, GetModuleHandle(0), 0);

        if(l_window)
        {
			HINSTANCE instance = (HINSTANCE)GetWindowLong(l_window, GWL_HINSTANCE);
			HGDIOBJ font = GetStockObject(DEFAULT_GUI_FONT);

			set_window_icon(l_window, "Resources/n64_32x32.ico");

			GetClientRect(l_window, &client);

			size_x = client.right - client.left;
			size_y = client.bottom - client.top;
			
			// Address field
			l_edit = create_edit_box(instance, l_window, EDIT_POSITION_X, EDIT_POSITION_Y,
										EDIT_SIZE_X, EDIT_SIZE_Y, MAX_CHARACTERS, L"0x80000000");

			// Value-size drop down list
			l_sizeCombo = create_drop_down_list(	instance, l_window, DROPLIST_SIZE_POSITION_X, DROPLIST_SIZE_POSITION_Y, DROPLIST_SIZE_SIZE_X, DROPLIST_SIZE_SIZE_Y,
													{"Byte", "Half-Word", "Word", "Double-Word"}, 2);

			// Analyze button
			l_analyzeButton = create_button(instance, l_window, BS_DEFPUSHBUTTON, L"Analyze", ANALYZEBUTTON_POSITION_X(size_x), ANALYZEBUTTON_POSITION_Y(size_y), ANALYZEBUTTON_SIZE_X, ANALYZEBUTTON_SIZE_Y);

			// Toggle breakpoint button
			l_toggleButton = create_button(instance, l_window, BS_PUSHBUTTON, L"Toggle breakpoint", TOGGLEBUTTON_POSITION_X, TOGGLEBUTTON_POSITION_Y(size_y), TOGGLEBUTTON_SIZE_X, TOGGLEBUTTON_SIZE_Y, false);

			// Step button
			l_stepButton = create_button(instance, l_window, BS_PUSHBUTTON, L"Step", STEPBUTTON_POSITION_X, STEPBUTTON_POSITION_Y(size_y), STEPBUTTON_SIZE_X, STEPBUTTON_SIZE_Y, false);

			// Continue button
			l_continueButton = create_button(instance, l_window, BS_PUSHBUTTON, L"Continue", CONTINUEBUTTON_POSITION_X, CONTINUEBUTTON_POSITION_Y(size_y), CONTINUEBUTTON_SIZE_X, CONTINUEBUTTON_SIZE_Y, false);

			// Tab control
			l_tabControl = create_tab_control(	instance, l_window, 
												TABCONTROL_POSITION_X, TABCONTROL_POSITION_Y, 
												TABCONTROL_SIZE_X(size_x), TABCONTROL_SIZE_Y(size_y),
												{"Traced", "Logged"});

			// 'Traced' list view
			l_tracedListView = create_list_view(instance, l_window,
											LISTVIEW_POSITION_X, LISTVIEW_POSITION_Y,
											LISTVIEW_SIZE_X(size_x), LISTVIEW_SIZE_Y(size_y),
											{"PC", "Operation", "Disassembly", "Result", "Current value"},
											{100, 125, 125, 125, 125},
											false,
											"Resources/breakpoint_15x15.bmp", 15);

			// 'Logged' list view
			l_loggedListView = create_list_view(instance, l_window,
												LISTVIEW_POSITION_X, LISTVIEW_POSITION_Y,
												LISTVIEW_SIZE_X(size_x), LISTVIEW_SIZE_Y(size_y),
												{"PC", "Disassembly", "Result"},
												{100, 125, 125},
												true);

			ShowWindow(l_loggedListView, FALSE);

			// Watch list
			l_watchListView = create_list_view(	instance, l_window,
												WATCHLIST_POSITION_X(size_x), WATCHLIST_POSITION_Y, 
												WATCHLIST_SIZE_X(size_x), WATCHLIST_SIZE_Y(size_y),
												{"Address", "Current value"},
												{75, 125});

			// Format combo
			l_formatCombo = create_drop_down_list(	instance, l_window, DROPLIST_FORMAT_POSITION_X(size_x), DROPLIST_FORMAT_POSITION_Y, DROPLIST_FORMAT_SIZE_X, DROPLIST_FORMAT_SIZE_Y,
													{"Hex", "Unsigned", "Signed", "Float"});

			// Status text
			l_statusText = CreateWindowEx(NULL, "STATIC", "", WS_CHILD | WS_VISIBLE | SS_RIGHT, ANALYZER_STATUS_POSITION_X(size_x), ANALYZER_STATUS_POSITION_Y(size_y),
											ANALYZER_STATUS_SIZE_X, ANALYZER_STATUS_SIZE_Y, l_window, NULL, instance, NULL);

			// Search window button
			l_searchWndButton = create_bitmap_button(instance, l_window, SEARCHWINDOWBUTTON_POSITION_X(size_x), SEARCHWINDOWBUTTON_POSITION_Y, 
				SEARCHWINDOWBUTTON_SIZE_X, SEARCHWINDOWBUTTON_SIZE_Y, "Resources/search_16x16.bmp");

			// Ignore window button
			l_ignoreWndButton = create_bitmap_button(instance, l_window, IGNOREWINDOWBUTTON_POSITION_X(size_x), IGNOREWINDOWBUTTON_POSITION_Y, 
				IGNOREWINDOWBUTTON_SIZE_X, IGNOREWINDOWBUTTON_SIZE_Y, "Resources/ignore_16x16.bmp");
					
			SendMessage(l_statusText, WM_SETFONT, (WPARAM)font, NULL);

            ShowWindow(l_window, SW_SHOWDEFAULT);

			MSG msg;
            while( GetMessage( &msg, NULL, 0, 0 ) )
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
        }
    }
}

static void analyzer_window_thread_start()
{
	create_analyzer_window();
}

/*********************************************************************************************************
 *  Configuration handling
 */

static m64p_error OpenConfigurationHandles(void)
{
    float fConfigParamsVersion;
    int bSaveConfig = 0;
    m64p_error rval;

    /* Open Configuration sections for core library and console User Interface */
    rval = (*ConfigOpenSection)("Core", &l_ConfigCore);
    if (rval != M64ERR_SUCCESS)
    {
        DebugMessage(M64MSG_ERROR, "failed to open 'Core' configuration section");
        return rval;
    }

    rval = (*ConfigOpenSection)("Video-General", &l_ConfigVideo);
    if (rval != M64ERR_SUCCESS)
    {
        DebugMessage(M64MSG_ERROR, "failed to open 'Video-General' configuration section");
        return rval;
    }

    rval = (*ConfigOpenSection)("UI-Console", &l_ConfigUI);
    if (rval != M64ERR_SUCCESS)
    {
        DebugMessage(M64MSG_ERROR, "failed to open 'UI-Console' configuration section");
        return rval;
    }

    if ((*ConfigGetParameter)(l_ConfigUI, "Version", M64TYPE_FLOAT, &fConfigParamsVersion, sizeof(float)) != M64ERR_SUCCESS)
    {
        DebugMessage(M64MSG_WARNING, "No version number in 'UI-Console' config section. Setting defaults.");
        (*ConfigDeleteSection)("UI-Console");
        (*ConfigOpenSection)("UI-Console", &l_ConfigUI);
        bSaveConfig = 1;
    }
    else if (((int) fConfigParamsVersion) != ((int) CONFIG_PARAM_VERSION))
    {
        DebugMessage(M64MSG_WARNING, "Incompatible version %.2f in 'UI-Console' config section: current is %.2f. Setting defaults.", fConfigParamsVersion, (float) CONFIG_PARAM_VERSION);
        (*ConfigDeleteSection)("UI-Console");
        (*ConfigOpenSection)("UI-Console", &l_ConfigUI);
        bSaveConfig = 1;
    }
    else if ((CONFIG_PARAM_VERSION - fConfigParamsVersion) >= 0.0001f)
    {
        /* handle upgrades */
        float fVersion = CONFIG_PARAM_VERSION;
        ConfigSetParameter(l_ConfigUI, "Version", M64TYPE_FLOAT, &fVersion);
        DebugMessage(M64MSG_INFO, "Updating parameter set version in 'UI-Console' config section to %.2f", fVersion);
        bSaveConfig = 1;
    }

    /* Set default values for my Config parameters */
    (*ConfigSetDefaultFloat)(l_ConfigUI, "Version", CONFIG_PARAM_VERSION,  "Mupen64Plus UI-Console config parameter set version number.  Please don't change this version number.");
    (*ConfigSetDefaultString)(l_ConfigUI, "PluginDir", OSAL_CURRENT_DIR, "Directory in which to search for plugins");
    (*ConfigSetDefaultString)(l_ConfigUI, "VideoPlugin", "mupen64plus-video-rice" OSAL_DLL_EXTENSION, "Filename of video plugin");
    (*ConfigSetDefaultString)(l_ConfigUI, "AudioPlugin", "mupen64plus-audio-sdl" OSAL_DLL_EXTENSION, "Filename of audio plugin");
    (*ConfigSetDefaultString)(l_ConfigUI, "InputPlugin", "mupen64plus-input-sdl" OSAL_DLL_EXTENSION, "Filename of input plugin");
    (*ConfigSetDefaultString)(l_ConfigUI, "RspPlugin", "mupen64plus-rsp-hle" OSAL_DLL_EXTENSION, "Filename of RSP plugin");

	/* Enable debugger */
	bool enableDebugger = true;
    ConfigSetParameter(l_ConfigCore, "EnableDebugger", M64TYPE_BOOL, &enableDebugger);

    if (bSaveConfig && ConfigSaveSection != NULL) /* ConfigSaveSection was added in Config API v2.1.0 */
        (*ConfigSaveSection)("UI-Console");

    return M64ERR_SUCCESS;
}

static m64p_error SaveConfigurationOptions(void)
{
    /* if shared data directory was given on the command line, write it into the config file */
    if (l_DataDirPath != NULL)
        (*ConfigSetParameter)(l_ConfigCore, "SharedDataPath", M64TYPE_STRING, l_DataDirPath);

    /* if any plugin filepaths were given on the command line, write them into the config file */
    if (g_PluginDir != NULL)
        (*ConfigSetParameter)(l_ConfigUI, "PluginDir", M64TYPE_STRING, g_PluginDir);
    if (g_GfxPlugin != NULL)
        (*ConfigSetParameter)(l_ConfigUI, "VideoPlugin", M64TYPE_STRING, g_GfxPlugin);
    if (g_AudioPlugin != NULL)
        (*ConfigSetParameter)(l_ConfigUI, "AudioPlugin", M64TYPE_STRING, g_AudioPlugin);
    if (g_InputPlugin != NULL)
        (*ConfigSetParameter)(l_ConfigUI, "InputPlugin", M64TYPE_STRING, g_InputPlugin);
    if (g_RspPlugin != NULL)
        (*ConfigSetParameter)(l_ConfigUI, "RspPlugin", M64TYPE_STRING, g_RspPlugin);

    return (*ConfigSaveFile)();
}

/*********************************************************************************************************
 *  Command-line parsing
 */

static void printUsage(const char *progname)
{
    printf("Usage: %s [parameters] [romfile]\n"
           "\n"
           "Parameters:\n"
           "    --noosd                : disable onscreen display\n"
           "    --osd                  : enable onscreen display\n"
           "    --fullscreen           : use fullscreen display mode\n"
           "    --windowed             : use windowed display mode\n"
           "    --resolution (res)     : display resolution (640x480, 800x600, 1024x768, etc)\n"
           "    --nospeedlimit         : disable core speed limiter (should be used with dummy audio plugin)\n"
           "    --cheats (cheat-spec)  : enable or list cheat codes for the given rom file\n"
           "    --corelib (filepath)   : use core library (filepath) (can be only filename or full path)\n"
           "    --configdir (dir)      : force configation directory to (dir); should contain mupen64plus.cfg\n"
           "    --datadir (dir)        : search for shared data files (.ini files, languages, etc) in (dir)\n"
           "    --plugindir (dir)      : search for plugins in (dir)\n"
           "    --sshotdir (dir)       : set screenshot directory to (dir)\n"
           "    --gfx (plugin-spec)    : use gfx plugin given by (plugin-spec)\n"
           "    --audio (plugin-spec)  : use audio plugin given by (plugin-spec)\n"
           "    --input (plugin-spec)  : use input plugin given by (plugin-spec)\n"
           "    --rsp (plugin-spec)    : use rsp plugin given by (plugin-spec)\n"
           "    --emumode (mode)       : set emu mode to: 0=Pure Interpreter 1=Interpreter 2=DynaRec\n"
           "    --savestate (filepath) : savestate loaded at startup\n"
           "    --testshots (list)     : take screenshots at frames given in comma-separated (list), then quit\n"
           "    --set (param-spec)     : set a configuration variable, format: ParamSection[ParamName]=Value\n"
           "    --core-compare-send    : use the Core Comparison debugging feature, in data sending mode\n"
           "    --core-compare-recv    : use the Core Comparison debugging feature, in data receiving mode\n"
           "    --nosaveoptions        : do not save the given command-line options in configuration file\n"
           "    --verbose              : print lots of information\n"
           "    --help                 : see this help message\n\n"
           "(plugin-spec):\n"
           "    (pluginname)           : filename (without path) of plugin to find in plugin directory\n"
           "    (pluginpath)           : full path and filename of plugin\n"
           "    'dummy'                : use dummy plugin\n\n"
           "(cheat-spec):\n"
           "    'list'                 : show all of the available cheat codes\n"
           "    'all'                  : enable all of the available cheat codes\n"
           "    (codelist)             : a comma-separated list of cheat code numbers to enable,\n"
           "                             with dashes to use code variables (ex 1-2 to use cheat 1 option 2)\n"
           "\n", progname);

    return;
}

static int SetConfigParameter(const char *ParamSpec)
{
    char *ParsedString, *VarName, *VarValue=NULL;
    m64p_handle ConfigSection;
    m64p_type VarType;
    m64p_error rval;

    if (ParamSpec == NULL)
    {
        DebugMessage(M64MSG_ERROR, "ParamSpec is NULL in SetConfigParameter()");
        return 1;
    }

    /* make a copy of the input string */
    ParsedString = (char *) malloc(strlen(ParamSpec) + 1);
    if (ParsedString == NULL)
    {
        DebugMessage(M64MSG_ERROR, "SetConfigParameter() couldn't allocate memory for temporary string.");
        return 2;
    }
    strcpy(ParsedString, ParamSpec);

    /* parse it for the simple section[name]=value format */
    VarName = strchr(ParsedString, '[');
    if (VarName != NULL)
    {
        *VarName++ = 0;
        VarValue = strchr(VarName, ']');
        if (VarValue != NULL)
        {
            *VarValue++ = 0;
        }
    }
    if (VarName == NULL || VarValue == NULL || *VarValue != '=')
    {
        DebugMessage(M64MSG_ERROR, "invalid (param-spec) '%s'", ParamSpec);
        free(ParsedString);
        return 3;
    }
    VarValue++;

    /* then set the value */
    rval = (*ConfigOpenSection)(ParsedString, &ConfigSection);
    if (rval != M64ERR_SUCCESS)
    {
        DebugMessage(M64MSG_ERROR, "SetConfigParameter failed to open config section '%s'", ParsedString);
        free(ParsedString);
        return 4;
    }
    if ((*ConfigGetParameterType)(ConfigSection, VarName, &VarType) == M64ERR_SUCCESS)
    {
        switch(VarType)
        {
            int ValueInt;
            float ValueFloat;
            case M64TYPE_INT:
                ValueInt = atoi(VarValue);
                ConfigSetParameter(ConfigSection, VarName, M64TYPE_INT, &ValueInt);
                break;
            case M64TYPE_FLOAT:
                ValueFloat = (float) atof(VarValue);
                ConfigSetParameter(ConfigSection, VarName, M64TYPE_FLOAT, &ValueFloat);
                break;
            case M64TYPE_BOOL:
                ValueInt = (int) (osal_insensitive_strcmp(VarValue, "true") == 0);
                ConfigSetParameter(ConfigSection, VarName, M64TYPE_BOOL, &ValueInt);
                break;
            case M64TYPE_STRING:
                ConfigSetParameter(ConfigSection, VarName, M64TYPE_STRING, VarValue);
                break;
            default:
                DebugMessage(M64MSG_ERROR, "invalid VarType in SetConfigParameter()");
                return 5;
        }
    }
    else
    {
        ConfigSetParameter(ConfigSection, VarName, M64TYPE_STRING, VarValue);
    }

    free(ParsedString);
    return 0;
}

static int *ParseNumberList(const char *InputString, int *ValuesFound)
{
    const char *str;
    int *OutputList;

    /* count the number of integers in the list */
    int values = 1;
    str = InputString;
    while ((str = strchr(str, ',')) != NULL)
    {
        str++;
        values++;
    }

    /* create a list and populate it with the frame counter values at which to take screenshots */
    if ((OutputList = (int *) malloc(sizeof(int) * (values + 1))) != NULL)
    {
        int idx = 0;
        str = InputString;
        while (str != NULL)
        {
            OutputList[idx++] = atoi(str);
            str = strchr(str, ',');
            if (str != NULL) str++;
        }
        OutputList[idx] = 0;
    }

    if (ValuesFound != NULL)
        *ValuesFound = values;
    return OutputList;
}

static int ParseCommandLineInitial(int argc, const char **argv)
{
    int i;

    /* look through commandline options */
    for (i = 1; i < argc; i++)
    {
        int ArgsLeft = argc - i - 1;

        if (strcmp(argv[i], "--corelib") == 0 && ArgsLeft >= 1)
        {
            l_CoreLibPath = argv[i+1];
            i++;
        }
        else if (strcmp(argv[i], "--configdir") == 0 && ArgsLeft >= 1)
        {
            l_ConfigDirPath = argv[i+1];
            i++;
        }
        else if (strcmp(argv[i], "--datadir") == 0 && ArgsLeft >= 1)
        {
            l_DataDirPath = argv[i+1];
            i++;
        }
        else if (strcmp(argv[i], "--help") == 0 || strcmp(argv[i], "-h") == 0)
        {
            printUsage(argv[0]);
            return 1;
        }
    }

    return 0;
}

static m64p_error ParseCommandLineFinal(int argc, const char **argv)
{
    int i;

    /* parse commandline options */
    for (i = 1; i < argc; i++)
    {
        int ArgsLeft = argc - i - 1;
        if (strcmp(argv[i], "--noosd") == 0)
        {
            int Osd = 0;
            (*ConfigSetParameter)(l_ConfigCore, "OnScreenDisplay", M64TYPE_BOOL, &Osd);
        }
        else if (strcmp(argv[i], "--osd") == 0)
        {
            int Osd = 1;
            (*ConfigSetParameter)(l_ConfigCore, "OnScreenDisplay", M64TYPE_BOOL, &Osd);
        }
        else if (strcmp(argv[i], "--fullscreen") == 0)
        {
            int Fullscreen = 1;
            (*ConfigSetParameter)(l_ConfigVideo, "Fullscreen", M64TYPE_BOOL, &Fullscreen);
        }
        else if (strcmp(argv[i], "--windowed") == 0)
        {
            int Fullscreen = 0;
            (*ConfigSetParameter)(l_ConfigVideo, "Fullscreen", M64TYPE_BOOL, &Fullscreen);
        }
        else if (strcmp(argv[i], "--nospeedlimit") == 0)
        {
            int EnableSpeedLimit = 0;
            if (g_CoreAPIVersion < 0x020001)
                DebugMessage(M64MSG_WARNING, "core library doesn't support --nospeedlimit");
            else
            {
                if ((*CoreDoCommand)(M64CMD_CORE_STATE_SET, M64CORE_SPEED_LIMITER, &EnableSpeedLimit) != M64ERR_SUCCESS)
                    DebugMessage(M64MSG_ERROR, "core gave error while setting --nospeedlimit option");
            }
        }
        else if ((strcmp(argv[i], "--corelib") == 0 || strcmp(argv[i], "--configdir") == 0 ||
                  strcmp(argv[i], "--datadir") == 0) && ArgsLeft >= 1)
        {   /* these are handled in ParseCommandLineInitial */
            i++;
        }
        else if (strcmp(argv[i], "--resolution") == 0 && ArgsLeft >= 1)
        {
            const char *res = argv[i+1];
            int xres, yres;
            i++;
            if (sscanf(res, "%ix%i", &xres, &yres) != 2)
                DebugMessage(M64MSG_WARNING, "couldn't parse resolution '%s'", res);
            else
            {
                (*ConfigSetParameter)(l_ConfigVideo, "ScreenWidth", M64TYPE_INT, &xres);
                (*ConfigSetParameter)(l_ConfigVideo, "ScreenHeight", M64TYPE_INT, &yres);
            }
        }
        else if (strcmp(argv[i], "--cheats") == 0 && ArgsLeft >= 1)
        {
            if (strcmp(argv[i+1], "all") == 0)
                l_CheatMode = CHEAT_ALL;
            else if (strcmp(argv[i+1], "list") == 0)
                l_CheatMode = CHEAT_SHOW_LIST;
            else
            {
                l_CheatMode = CHEAT_LIST;
                l_CheatNumList = (char*) argv[i+1];
            }
            i++;
        }
        else if (strcmp(argv[i], "--plugindir") == 0 && ArgsLeft >= 1)
        {
            g_PluginDir = argv[i+1];
            i++;
        }
        else if (strcmp(argv[i], "--sshotdir") == 0 && ArgsLeft >= 1)
        {
            (*ConfigSetParameter)(l_ConfigCore, "ScreenshotPath", M64TYPE_STRING, argv[i+1]);
            i++;
        }
        else if (strcmp(argv[i], "--gfx") == 0 && ArgsLeft >= 1)
        {
            g_GfxPlugin = argv[i+1];
            i++;
        }
        else if (strcmp(argv[i], "--audio") == 0 && ArgsLeft >= 1)
        {
            g_AudioPlugin = argv[i+1];
            i++;
        }
        else if (strcmp(argv[i], "--input") == 0 && ArgsLeft >= 1)
        {
            g_InputPlugin = argv[i+1];
            i++;
        }
        else if (strcmp(argv[i], "--rsp") == 0 && ArgsLeft >= 1)
        {
            g_RspPlugin = argv[i+1];
            i++;
        }
        else if (strcmp(argv[i], "--emumode") == 0 && ArgsLeft >= 1)
		{
			i++;
		}
        else if (strcmp(argv[i], "--savestate") == 0 && ArgsLeft >= 1)
        {
            l_SaveStatePath = argv[i+1];
            i++;
        }
        else if (strcmp(argv[i], "--testshots") == 0 && ArgsLeft >= 1)
        {
            l_TestShotList = ParseNumberList(argv[i+1], NULL);
            i++;
        }
        else if (strcmp(argv[i], "--set") == 0 && ArgsLeft >= 1)
        {
            if (SetConfigParameter(argv[i+1]) != 0)
                return M64ERR_INPUT_INVALID;
            i++;
        }
        else if (strcmp(argv[i], "--core-compare-send") == 0)
        {
            l_CoreCompareMode = 1;
        }
        else if (strcmp(argv[i], "--core-compare-recv") == 0)
        {
            l_CoreCompareMode = 2;
        }
        else if (strcmp(argv[i], "--nosaveoptions") == 0)
        {
            l_SaveOptions = 0;
        }
        else if (ArgsLeft == 0)
        {
            /* this is the last arg, it should be a ROM filename */
            l_ROMFilepath = argv[i];
            return M64ERR_SUCCESS;
        }
        else if (strcmp(argv[i], "--verbose") == 0)
        {
            g_Verbose = 1;
        }
        else
        {
            DebugMessage(M64MSG_WARNING, "unrecognized command-line parameter '%s'", argv[i]);
        }
        /* continue argv loop */
    }

    /* missing ROM filepath */
    DebugMessage(M64MSG_ERROR, "no ROM filepath given");
    return M64ERR_INPUT_INVALID;
}

/*********************************************************************************************************
* main function
*/


/* Allow state callback in external module to be specified via build flags (header and function name) */
#ifdef CALLBACK_HEADER
#define xstr(s) str(s)
#define str(s) #s
#include xstr(CALLBACK_HEADER)
#endif

#ifndef CALLBACK_FUNC
#define CALLBACK_FUNC NULL
#endif

#ifndef WIN32
/* Allow external modules to call the main function as a library method.  This is useful for user
 * interfaces that simply layer on top of (rather than re-implement) UI-Console (e.g. mupen64plus-ae).
 */
__attribute__ ((visibility("default")))
#endif
int main(int argc, char *argv[])
{
	DisableProcessWindowsGhosting();

    int i;

    printf(" __  __                         __   _  _   ____  _             \n");  
    printf("|  \\/  |_   _ _ __   ___ _ __  / /_ | || | |  _ \\| |_   _ ___ \n");
    printf("| |\\/| | | | | '_ \\ / _ \\ '_ \\| '_ \\| || |_| |_) | | | | / __|  \n");
    printf("| |  | | |_| | |_) |  __/ | | | (_) |__   _|  __/| | |_| \\__ \\  \n");
    printf("|_|  |_|\\__,_| .__/ \\___|_| |_|\\___/   |_| |_|   |_|\\__,_|___/  \n");
    printf("             |_|         http://code.google.com/p/mupen64plus/  \n");
    printf("%s Version %i.%i.%i\n\n", CONSOLE_UI_NAME, VERSION_PRINTF_SPLIT(CONSOLE_UI_VERSION));

    /* bootstrap some special parameters from the command line */
    if (ParseCommandLineInitial(argc, (const char **) argv) != 0)
        return 1;

    /* load the Mupen64Plus core library */
    if (AttachCoreLib(l_CoreLibPath) != M64ERR_SUCCESS)
        return 2;

    /* start the Mupen64Plus core library, load the configuration file */
    m64p_error rval = (*CoreStartup)(CORE_API_VERSION, l_ConfigDirPath, l_DataDirPath, "Core", DebugCallback, NULL, CALLBACK_FUNC);
    if (rval != M64ERR_SUCCESS)
    {
        DebugMessage(M64MSG_ERROR, "couldn't start Mupen64Plus core library.");
        DetachCoreLib();
        return 3;
    }

    /* Open configuration sections */
    rval = OpenConfigurationHandles();
    if (rval != M64ERR_SUCCESS)
    {
        (*CoreShutdown)();
        DetachCoreLib();
        return 4;
    }

	/* set default graphics plugin to glide64mk2*/
    g_GfxPlugin = "mupen64plus-video-glide64mk2";

    /* parse command-line options */
    rval = ParseCommandLineFinal(argc, (const char **) argv);
    if (rval != M64ERR_SUCCESS)
    {
        (*CoreShutdown)();
        DetachCoreLib();
        return 5;
    }

	/* set emumode to always 0 (pure interpreter) */
    int emumode = 0;

    (*ConfigSetParameter)(l_ConfigCore, "R4300Emulator", M64TYPE_INT, &emumode);

    /* Handle the core comparison feature */
    if (l_CoreCompareMode != 0 && !(g_CoreCapabilities & M64CAPS_CORE_COMPARE))
    {
        DebugMessage(M64MSG_ERROR, "can't use --core-compare feature with this Mupen64Plus core library.");
        DetachCoreLib();
        return 6;
    }
    compare_core_init(l_CoreCompareMode);

    /* save the given command-line options in configuration file if requested */
    if (l_SaveOptions)
        SaveConfigurationOptions();

    /* load ROM image */
    FILE *fPtr = fopen(l_ROMFilepath, "rb");
    if (fPtr == NULL)
    {
        DebugMessage(M64MSG_ERROR, "couldn't open ROM file '%s' for reading.", l_ROMFilepath);
        (*CoreShutdown)();
        DetachCoreLib();
        return 7;
    }

    /* get the length of the ROM, allocate memory buffer, load it from disk */
    long romlength = 0;
    fseek(fPtr, 0L, SEEK_END);
    romlength = ftell(fPtr);
    fseek(fPtr, 0L, SEEK_SET);
    unsigned char *ROM_buffer = (unsigned char *) malloc(romlength);
    if (ROM_buffer == NULL)
    {
        DebugMessage(M64MSG_ERROR, "couldn't allocate %li-byte buffer for ROM image file '%s'.", romlength, l_ROMFilepath);
        fclose(fPtr);
        (*CoreShutdown)();
        DetachCoreLib();
        return 8;
    }
    else if (fread(ROM_buffer, 1, romlength, fPtr) != romlength)
    {
        DebugMessage(M64MSG_ERROR, "couldn't read %li bytes from ROM image file '%s'.", romlength, l_ROMFilepath);
        free(ROM_buffer);
        fclose(fPtr);
        (*CoreShutdown)();
        DetachCoreLib();
        return 9;
    }
    fclose(fPtr);

    /* Try to load the ROM image into the core */
    if ((*CoreDoCommand)(M64CMD_ROM_OPEN, (int) romlength, ROM_buffer) != M64ERR_SUCCESS)
    {
        DebugMessage(M64MSG_ERROR, "core failed to open ROM image file '%s'.", l_ROMFilepath);
        free(ROM_buffer);
        (*CoreShutdown)();
        DetachCoreLib();
        return 10;
    }
    free(ROM_buffer); /* the core copies the ROM image, so we can release this buffer immediately */

    /* handle the cheat codes */
    CheatStart(l_CheatMode, l_CheatNumList);
    if (l_CheatMode == CHEAT_SHOW_LIST)
    {
        (*CoreDoCommand)(M64CMD_ROM_CLOSE, 0, NULL);
        (*CoreShutdown)();
        DetachCoreLib();
        return 11;
    }

	/* set debug callbacks */
	if(DebugSetCallbacks(DebugInitCallback, DebugUpdateCallback, DebugVICallback) != M64ERR_SUCCESS)
	{
		DebugMessage(M64MSG_ERROR, "failed to set debug callbacks");
	}

	/* set frame callback */
	if ((*CoreDoCommand)(M64CMD_SET_FRAME_CALLBACK, 0, FrameCallback) != M64ERR_SUCCESS)
    {
        DebugMessage(M64MSG_WARNING, "failed to set frame callback");
    }

    /* search for and load plugins */
    rval = PluginSearchLoad(l_ConfigUI);
    if (rval != M64ERR_SUCCESS)
    {
        (*CoreDoCommand)(M64CMD_ROM_CLOSE, 0, NULL);
        (*CoreShutdown)();
        DetachCoreLib();
        return 12;
    }

    /* attach plugins to core */
    for (i = 0; i < 4; i++)
    {
        if ((*CoreAttachPlugin)(g_PluginMap[i].type, g_PluginMap[i].handle) != M64ERR_SUCCESS)
        {
            DebugMessage(M64MSG_ERROR, "core error while attaching %s plugin.", g_PluginMap[i].name);
            (*CoreDoCommand)(M64CMD_ROM_CLOSE, 0, NULL);
            (*CoreShutdown)();
            DetachCoreLib();
            return 13;
        }
    }
	
	/* start analyzer threads */
	std::thread analyzerWindowThread(analyzer_window_thread_start);
	std::thread analyzerThread(analyzer_thread_start);

    /* run the game */
    (*CoreDoCommand)(M64CMD_EXECUTE, 0, NULL);

	/* join analyzer threads */
	SendMessage(l_window, WM_CLOSE, NULL, NULL);

	set_cancel_clicked(true);
	set_analyzer_state(AnalyzerState::TERMINATED);
	
	analyzerWindowThread.join();
	analyzerThread.join();

    /* detach plugins from core and unload them */
    for (i = 0; i < 4; i++)
        (*CoreDetachPlugin)(g_PluginMap[i].type);
    PluginUnload();

    /* close the ROM image */
    (*CoreDoCommand)(M64CMD_ROM_CLOSE, 0, NULL);

    /* save the configuration file again if --nosaveoptions was not specified, to keep any updated parameters from the core/plugins */
    if (l_SaveOptions)
        SaveConfigurationOptions();

    /* Shut down and release the Core library */
    (*CoreShutdown)();
    DetachCoreLib();

    /* free allocated memory */
    if (l_TestShotList != NULL)
        free(l_TestShotList);

    return 0;
}
