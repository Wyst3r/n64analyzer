/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *   Mupen64plus - debugger.c                                              *
 *   Mupen64Plus homepage: http://code.google.com/p/mupen64plus/           *
 *   Copyright (C) 2008 DarkJeztr                                          *
 *   Copyright (C) 2002 davFr                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.          *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include <SDL.h>

#include "api/debugger.h"
#include "dbg_breakpoints.h"
#include "dbg_debugger.h"
#include "dbg_memory.h"
#include "dbg_types.h"
#include "memory/memory.h"

#ifdef DBG

int g_DebuggerActive = 0;    // whether the debugger is enabled or not

m64p_dbg_runstate g_dbg_runstate;

static SDL_mutex *mut_cont;
static SDL_cond *cond_cont;

static unsigned int cont = 0;

uint32 previousPC;

//]=-=-=-=-=-=-=-=-=-=-=[ Initialisation du Debugger ]=-=-=-=-=-=-=-=-=-=-=-=[

void init_debugger()
{
    g_DebuggerActive = 1;
    g_dbg_runstate = M64P_DBG_RUNSTATE_PAUSED;

    DebuggerCallback(DEBUG_UI_INIT, 0, 0); /* call front-end to initialize user interface */

    init_host_disassembler();

    mut_cont = SDL_CreateMutex();
	cond_cont = SDL_CreateCond();
}

void destroy_debugger()
{
	SDL_DestroyMutex(mut_cont);
	SDL_DestroyCond(cond_cont);
    mut_cont = NULL;
	cond_cont = NULL;
    g_DebuggerActive = 0;
}

//]=-=-=-=-=-=-=-=-=-=-=-=-=[ Mise-a-Jour Debugger ]=-=-=-=-=-=-=-=-=-=-=-=-=[

void update_debugger(uint32 pc)
// Update debugger state and display.
// Should be called after each R4300 instruction
// Checks for breakpoint hits on PC
{
    int bpt;

    if (g_dbg_runstate != M64P_DBG_RUNSTATE_PAUSED) {
        bpt = check_breakpoints(pc);
        if (bpt != -1) {
            g_dbg_runstate = M64P_DBG_RUNSTATE_PAUSED;

            if (BPT_CHECK_FLAG(g_Breakpoints[bpt], M64P_BKP_FLAG_LOG))
                log_breakpoint(pc, M64P_BKP_FLAG_EXEC, 0);
        }
    }

    if (g_dbg_runstate != M64P_DBG_RUNSTATE_RUNNING) {
		SDL_LockMutex(mut_cont);
		{
			cont = 0;

			DebuggerCallback(DEBUG_UI_UPDATE, *fast_mem_access(pc), pc);  /* call front-end to notify user interface to update */
			
			if(g_dbg_runstate != M64P_DBG_RUNSTATE_RUNNING)
			{
				while(!cont)
				{
					SDL_CondWait(cond_cont, mut_cont);
				}
			}			
		}
		SDL_UnlockMutex(mut_cont);
    }

    previousPC = pc;
}

void debugger_continue()
{
	SDL_LockMutex(mut_cont);
	{
		cont = 1;
	}
	SDL_UnlockMutex(mut_cont);

	SDL_CondSignal(cond_cont);

}

#endif
